package com.example.developer.schoolmanagementsystem.Adatper;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.developer.schoolmanagementsystem.Activities.TeacherSyllabus;
import com.example.developer.schoolmanagementsystem.Activities.subject;
import com.example.developer.schoolmanagementsystem.Model.TeachersClasses;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 3/11/2018.
 */

public class TeacherStudentClassForSyllabusAdapter extends RecyclerView.Adapter<TeacherStudentClassForSyllabusAdapter.MyViewHolder> {

    private List<TeachersClasses> moviesList;

    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public Button classbutton;

        public MyViewHolder(View view) {
            super(view);
            classbutton = (Button) view.findViewById(R.id.studentclassbtn);


        }
    }

    public TeacherStudentClassForSyllabusAdapter(List<TeachersClasses> moviesList, Context context) {
        this.moviesList = moviesList;
        this.context = context;
    }

    @Override
    public TeacherStudentClassForSyllabusAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.studentclass_list, parent, false);

        return new TeacherStudentClassForSyllabusAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TeacherStudentClassForSyllabusAdapter.MyViewHolder holder, int position) {
        final TeachersClasses material = moviesList.get(position);
        holder.classbutton.setText(material.getName());
        holder.classbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String classid = material.getClassId();
                String teacher_id = material.getTeacherId();
                context.startActivity(new Intent(context,TeacherSyllabus.class).putExtra("class_id",classid));

            }
        });








    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
