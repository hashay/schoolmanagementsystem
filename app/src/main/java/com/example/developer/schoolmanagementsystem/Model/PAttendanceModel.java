package com.example.developer.schoolmanagementsystem.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Developer on 3/27/2018.
 */

public class PAttendanceModel {

    @SerializedName("attendance_id")
    @Expose
    private String attendanceId;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("class_id")
    @Expose
    private String classId;
    @SerializedName("section_id")
    @Expose
    private String sectionId;
    @SerializedName("student_id")
    @Expose
    private String studentId;
    @SerializedName("class_routine_id")
    @Expose
    private Object classRoutineId;
    @SerializedName("status")
    @Expose
    private Object status;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("student_name")
    @Expose
    private String studentName;
    @SerializedName("section_name")
    @Expose
    private String sectionName;
    @SerializedName("class_name")
    @Expose
    private String className;

    public String getAttendanceId() {
        return attendanceId;
    }

    public void setAttendanceId(String attendanceId) {
        this.attendanceId = attendanceId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public Object getClassRoutineId() {
        return classRoutineId;
    }

    public void setClassRoutineId(Object classRoutineId) {
        this.classRoutineId = classRoutineId;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

}
