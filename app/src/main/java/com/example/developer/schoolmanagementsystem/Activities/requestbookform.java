package com.example.developer.schoolmanagementsystem.Activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Model.RequestBookForm;
import com.example.developer.schoolmanagementsystem.Model.RequestBookFormResponse;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;
import com.gdacciaro.iOSDialog.iOSDialog;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;
import com.gdacciaro.iOSDialog.iOSDialogClickListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class requestbookform extends AppCompatActivity {
    EditText date, date2;
    private Call<RequestBookFormResponse> CallBackCall;
    private List<RequestBookForm> bookdetails = new ArrayList<>();
    DatePickerDialog datePickerDialog, datePickerDialog2;
    private String book_name, book_id, start_date, end_date, student_id;
    private Button bookrequest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requestbookform);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_library); // check it
        setSupportActionBar(toolbar);

        // adding back functionality

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        book_name = getIntent().getExtras().getString("book_name", "");
        book_id = getIntent().getExtras().getString("book_id", "");
        student_id = getIntent().getExtras().getString("student_id","");
        TextView txtinput = (TextView) findViewById(R.id.requestbookform_title);
        txtinput.setText(book_name);
        bookrequest = (Button) findViewById(R.id.bookrequest_btn);

        // initiate the dateEditText picker and a button
        date = (EditText) findViewById(R.id.startingdate);
        // perform click event on edit text
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current dateEditText , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current date
                // dateEditText picker dialog
                datePickerDialog = new DatePickerDialog(requestbookform.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set date of month , month and year value in the edit text
                                date.setText(dayOfMonth + "-"
                                        + (monthOfYear + 1) + "-" + year);
                                start_date=date.getText().toString();


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });



            date2 = (EditText) findViewById(R.id.endingdate);
        // perform click event on edit text
        date2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current dateEditText , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current date
                // dateEditText picker dialog
                datePickerDialog2 = new DatePickerDialog(requestbookform.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set date of month , month and year value in the edit text
                                date2.setText(dayOfMonth + "-"
                                        + (monthOfYear + 1) + "-" + year);
                                end_date=date.getText().toString();
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog2.show();
            }
        });


        bookrequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                start_date = dateEditText.getText().toString();
//                end_date = date2.getText().toString();
               Login(student_id,book_id,start_date,end_date);
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private  void Login(String student_id,String book_id,String issue_start_date, String issue_end_date) {
        API api = RestAdapter.createAPI();
        CallBackCall = api.RequestBookFormResponse(student_id,book_id,issue_start_date,issue_end_date);

        CallBackCall.enqueue(new Callback<RequestBookFormResponse>() {
            @Override
            public void onResponse(Call<RequestBookFormResponse> call, Response<RequestBookFormResponse> response) {

                if (response.body().getSuccess()) {

                    new iOSDialogBuilder(requestbookform.this)
                            .setTitle("Done")
                            .setSubtitle("Book Request is submitted.The book will be issued when the Librarian will accept your request.")
                            .setBoldPositiveLabel(true)
                            .setCancelable(false)
                            .setPositiveListener("okay",new iOSDialogClickListener() {
                                @Override
                                public void onClick(iOSDialog dialog) {
                                    Intent intent = new Intent(getApplicationContext(), requestbook.class);
                                    startActivity(intent);
                                    dialog.dismiss();

                                }
                            })

                            .build().show();


                } else {
                    Toast.makeText(requestbookform.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<RequestBookFormResponse> call, Throwable t) {

                Toast.makeText(requestbookform.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });

    }


}