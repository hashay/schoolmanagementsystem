package com.example.developer.schoolmanagementsystem.Adatper;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Activities.details;
import com.example.developer.schoolmanagementsystem.Model.Movie;
import com.example.developer.schoolmanagementsystem.Model.TeacherDetails;
import com.example.developer.schoolmanagementsystem.Model.data;
import com.example.developer.schoolmanagementsystem.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Developer on 2/14/2018.
 */

public class dataAdapter extends RecyclerView.Adapter<dataAdapter.MyViewHolder> {



    private List<TeacherDetails> moviesList;
    Context context;
    public CircleImageView iv_TeacherPicture;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;
        RelativeLayout rv_item_teacher;


        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.teacher_name);
            genre = (TextView) view.findViewById(R.id.teacher_description);
            year = (TextView) view.findViewById(R.id.teacher_description_date);
            rv_item_teacher = (RelativeLayout) view.findViewById(R.id.rv_item_teacher);
            iv_TeacherPicture = (CircleImageView) view.findViewById(R.id.teacher_description_iv);

        }
    }

    public dataAdapter(List<TeacherDetails> moviesList, Context context) {
        this.moviesList = moviesList;
        this.context = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.teacher_details, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(dataAdapter.MyViewHolder holder, int position) {
        final TeacherDetails data = moviesList.get(position);
        holder.title.setText(data.getName());
        holder.genre.setText(data.getDesignation());
        holder.year.setText(data.getBirthday());
        Picasso.get().load(data.getImageUrl()).resize(80,80).centerCrop().into(iv_TeacherPicture);
        holder.rv_item_teacher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(context, ""+data.getName(), Toast.LENGTH_SHORT).show();
//                Toast.makeText(context, ""+data.getTeacherId(), Toast.LENGTH_SHORT).show();
                String teacher_id = data.getTeacherId().toString();
                context.startActivity(new Intent(context,details.class).putExtra("teacher_id",teacher_id));
            }
        });



    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }




}
