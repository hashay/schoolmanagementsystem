package com.example.developer.schoolmanagementsystem.Model;

/**
 * Created by Developer on 2/19/2018.
 */

public class libreq {

    private String book, requested,startdate, enddate, status ;

    public libreq(String book,String requested, String startdate, String enddate, String status){
        this.book = book;
        this.requested = requested;
        this.startdate = startdate;
        this.enddate = enddate;
        this.status = status;



    }

    public String getBook(){
        return  book;

    }

    public void setBook(String name){
        this.book = name;

    }

    public String getRequested(){
        return requested;
    }

    public void  setRequested(String name){
        this.requested = name;
    }

    public String getStartdate(){
        return startdate;
    }

    public void setStartdate(String name){
        this.startdate = name;
    }

    public String getEnddate(){
        return enddate;
    }

    public void setEnddate(String name){
        this.enddate = name;
    }

    public String getStatus(){
        return status;
    }

    public void setStatus(String name){

        this.status = name;
    }



}
