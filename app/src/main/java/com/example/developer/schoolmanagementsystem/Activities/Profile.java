package com.example.developer.schoolmanagementsystem.Activities;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Model.ProfileDataUpdateResponse;
import com.example.developer.schoolmanagementsystem.Model.ProfileDataViewResponse;
import com.example.developer.schoolmanagementsystem.Model.data;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;
import com.gdacciaro.iOSDialog.iOSDialog;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;
import com.gdacciaro.iOSDialog.iOSDialogClickListener;
import com.imagepicker.FilePickUtils;
import com.imagepicker.LifeCycleCallBackManager;
import com.squareup.picasso.Picasso;

import java.io.File;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.imagepicker.FilePickUtils.CAMERA_PERMISSION;
import static com.imagepicker.FilePickUtils.STORAGE_PERMISSION_IMAGE;

public class Profile extends AppCompatActivity {
    EditText Name , Email;
    String user_id,user_type;
    private Call<ProfileDataViewResponse> callbackCall;
    private Call<ProfileDataUpdateResponse> ProfileUpdateCallBackCall;
    ImageView ProfilePicture;
    LifeCycleCallBackManager lifeCycleCallBackManager;
    FilePickUtils filePickUtils;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        TakingData();
        Toolbar toolbar = (Toolbar) findViewById(R.id.studymaterial_toolbar); // check it
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        initializations();
        FetchRecord(user_id,user_type);

        //picture uploading
        filePickUtils = new FilePickUtils(this, onFileChoose);;
        lifeCycleCallBackManager = filePickUtils.getCallBackManager();


    }

    //picture uploading start
    private FilePickUtils.OnFileChoose onFileChoose = new FilePickUtils.OnFileChoose() {
        @Override public void onFileChoose(String fileUri, int requestCode) {
            ProfilePicture.setImageBitmap(BitmapFactory.decodeFile(fileUri));

        }
    };
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (lifeCycleCallBackManager != null) {
            lifeCycleCallBackManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (lifeCycleCallBackManager != null) {
            lifeCycleCallBackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void Selectphoto(View v){


        new iOSDialogBuilder(Profile.this)
                .setTitle("Upload Image From")
                .setBoldPositiveLabel(true)
                .setCancelable(false)
                .setPositiveListener("Camera",new iOSDialogClickListener() {
                    @Override
                    public void onClick(iOSDialog dialog) {
                        filePickUtils.requestImageCamera(CAMERA_PERMISSION, true, true);
                        dialog.dismiss();

                    }
                })
                .setNegativeListener("Gallery",new iOSDialogClickListener() {
                    @Override
                    public void onClick(iOSDialog dialog) {
                        filePickUtils.requestImageGallery(STORAGE_PERMISSION_IMAGE, true, true);
                        dialog.dismiss();

                    }
                })

                .build().show();

    }
    //picture uploading end

    public void initializations(){
        Name = (EditText) findViewById(R.id.et_ProfileName);
        Email = (EditText) findViewById(R.id.et_ProfileEmail);
        ProfilePicture = (ImageView) findViewById(R.id.iv_profilePicture);

    }

    public void TakingData(){
        if(getIntent().getStringExtra("student_id") != null && getIntent().getStringExtra("user_type") !=null){
            user_id =  getIntent().getExtras().getString("student_id","");
            user_type =  getIntent().getExtras().getString("user_type","");
        }else if (getIntent().getStringExtra("teacher_id") != null && getIntent().getStringExtra("user_type") !=null){
            user_id = getIntent().getExtras().getString("teacher_id","");
        }else if (getIntent().getStringExtra("parent_id") != null && getIntent().getStringExtra("user_type") !=null) {
            user_id = getIntent().getExtras().getString("parent_id", "");
        }else if (getIntent().getStringExtra("librarian_id") != null && getIntent().getStringExtra("user_type") !=null) {
            user_id = getIntent().getExtras().getString("librarian_id", "");
        }
    }

    private void FetchRecord(String user_id,String user_type) {
        API api = RestAdapter.createAPI();
        callbackCall = api.PROFILE_DATA_VIEW_RESPONSE_CALL(user_id,user_type);

        callbackCall.enqueue(new Callback<ProfileDataViewResponse>() {
            @Override
            public void onResponse(Call<ProfileDataViewResponse> call, Response<ProfileDataViewResponse> response) {

                if (response.body().getSuccess()) {
                    Name.setText(response.body().getData().getName());
                    Email.setText(response.body().getData().getEmail());
                    try {
                        Picasso.get().load(response.body().getData().getPhoto()).resize(80, 80).centerCrop().into(ProfilePicture);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(Profile.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProfileDataViewResponse> call, Throwable t) {

                Toast.makeText(Profile.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private  void UpdateProfile(String user_id, String name, String email, String user_type, File file) {
        API api = RestAdapter.createAPI();
        ProfileUpdateCallBackCall = api.PROFILE_DATA_UPDATE_RESPONSE_CALL(user_id,name,email,user_type,file);

        ProfileUpdateCallBackCall.enqueue(new Callback<ProfileDataUpdateResponse>() {
            @Override
            public void onResponse(Call<ProfileDataUpdateResponse> call, Response<ProfileDataUpdateResponse> response) {

                if (response.body().getSuccess()) {

                    new iOSDialogBuilder(Profile.this)
                            .setTitle("Alert")
                            .setSubtitle("Your Profile is Updated.")
                            .setBoldPositiveLabel(true)
                            .setCancelable(false)
                            .setPositiveListener("Okay",new iOSDialogClickListener() {
                                @Override
                                public void onClick(iOSDialog dialog) {

                                    dialog.dismiss();

                                }
                            })

                            .build().show();


                } else {
                    Toast.makeText(Profile.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ProfileDataUpdateResponse> call, Throwable t) {

                Toast.makeText(Profile.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
