package com.example.developer.schoolmanagementsystem.Adatper;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.developer.schoolmanagementsystem.Model.tbook;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 2/21/2018.
 */

public class tbookadapter extends RecyclerView.Adapter<tbookadapter.MyViewHolder>{

    private List<tbook> moviesList;

    private tbookadapter.OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, tbook obj);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView book, author , description , price , classname;


        public MyViewHolder(View view) {
            super(view);
            book = (TextView) view.findViewById(R.id.tbooklist_title);
            author    = (TextView) view.findViewById(R.id.tbooklist_author);
            description = (TextView) view.findViewById(R.id.tbooklist_description);
            price = (TextView) view.findViewById(R.id.tbooklidt_price);
            classname = (TextView) view.findViewById(R.id.tbooklist_class);



        }
    }

    public tbookadapter(List<tbook> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public tbookadapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tbook_list, parent, false);

        return new tbookadapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(tbookadapter.MyViewHolder holder, int position) {
        final tbook material = moviesList.get(position);
        holder.book.setText(material.getBook());
        // holder.genre.setText(data.getGenre());
        // holder.year.setText(data.getYear());
        holder.author.setText(material.getAuthor());
        holder.description.setText(material.getDescription());
        holder.price.setText(material.getPrice());
        holder.classname.setText(material.getClassname());



    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}
