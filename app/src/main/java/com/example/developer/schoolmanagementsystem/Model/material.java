package com.example.developer.schoolmanagementsystem.Model;

/**
 * Created by Developer on 2/16/2018.
 */

public class material {

    private String title, date, description, classnumber, subject ;




    public material(String title, String date, String description, String classnumber, String subject) {
        this.title = title;
        this.date = date;
        this.description = description;
        this.classnumber = classnumber;
        this.subject = subject;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String name) {
        this.date = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String name) {
        this.description = name;
    }

    public String getClassnumber() {
        return classnumber;
    }

    public void setClassnumber(String name) {
        this.classnumber = name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String name) {
        this.subject = name;
    }



}
