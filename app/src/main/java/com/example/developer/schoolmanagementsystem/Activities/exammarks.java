package com.example.developer.schoolmanagementsystem.Activities;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.exammarksAdapter;
import com.example.developer.schoolmanagementsystem.Adatper.materialAdapter;
import com.example.developer.schoolmanagementsystem.Model.StudentMarksheet;
import com.example.developer.schoolmanagementsystem.Model.StudentMarksheetResponse;
import com.example.developer.schoolmanagementsystem.Model.marks;
import com.example.developer.schoolmanagementsystem.Model.material;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class exammarks extends AppCompatActivity {
    private Call<StudentMarksheetResponse> callbackCall;
    private List<StudentMarksheet> examList = new ArrayList<>();
    private RecyclerView recyclerView;
    private exammarksAdapter mnAdapter;
    public String student_id;
    public TextView totalMarks, AverageGradePoints;
    public Button marksheet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exammarks);
        student_id =  getIntent().getExtras().getString("student_id","");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_subject); // check it
        setSupportActionBar(toolbar);
        totalMarks = (TextView)findViewById(R.id.totalexam);
        AverageGradePoints = (TextView)findViewById(R.id.exam_gradess);
        marksheet = (Button)findViewById(R.id.btn_exammarks);
        marksheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {





            }
        });
        // adding back functionality

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.exammarks_recycler_view);



        mnAdapter = new exammarksAdapter(StudentMarkSheetById(student_id));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);

        getTotal(student_id);


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private List<StudentMarksheet> StudentMarkSheetById(String student_id) {
        API api = RestAdapter.createAPI();
        callbackCall = api.StudentMarkSheetResponse(student_id);


        callbackCall.enqueue(new Callback<StudentMarksheetResponse>() {
            @Override
            public void onResponse(Call<StudentMarksheetResponse> call, Response<StudentMarksheetResponse> response) {

                if (response.body().getSuccess()) {
//
                    examList = response.body().getData();
                    setMaterialList(response.body().getData());
                   // Toast.makeText(exammarks.this, "Date 1"+movieList.get(0).get(), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(exammarks.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<StudentMarksheetResponse> call, Throwable t) {

                Toast.makeText(exammarks.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });
        return examList;

    }

    private void setMaterialList(List<StudentMarksheet> materialList)
    {

        mnAdapter = new exammarksAdapter(materialList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);
    }



    private void getTotal(String student_id) {
        API api = RestAdapter.createAPI();
        callbackCall = api.StudentMarkSheetResponse(student_id);

        callbackCall.enqueue(new Callback<StudentMarksheetResponse>() {
            @Override
            public void onResponse(Call<StudentMarksheetResponse> call, final Response<StudentMarksheetResponse> response) {

                if (response.body().getSuccess()) {


                    //            name.setText(response.body().getData().get(0).getName().toString());
                    totalMarks.setText(String.valueOf(response.body().getTotalMarks()));
                    AverageGradePoints.setText(String.valueOf(response.body().getAvgGradePoint()));
                    marksheet.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View view) {
                            String URL = response.body().getMarkUrl();
                            String FileName = "Marksheet for year "+response.body().getYear();
                            if (URL.equals("")) {

                            }
                         else{
                            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(URL));
                            request.setDescription("Marksheet");
                            request.setTitle(FileName);
// in order for this if to run, you must use the android 3.2 to compile your app
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                request.allowScanningByMediaScanner();
                                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                            }
                            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, FileName);

// get download service and enqueue file
                            DownloadManager manager = (DownloadManager) getApplicationContext().getSystemService(Context.DOWNLOAD_SERVICE);
                            manager.enqueue(request);

                        }
                        }
                    });


                } else {
                    Toast.makeText(exammarks.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<StudentMarksheetResponse> call, Throwable t) {

                Toast.makeText(exammarks.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
