package com.example.developer.schoolmanagementsystem.Model;

/**
 * Created by Developer on 2/14/2018.
 */

public class data {

    private String title, genre, year;

    public data(String hashim, String s) {
    }

    public data(String title, String genre, String year) {
        this.title = title;
        this.genre = genre;
        this.year = year;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
