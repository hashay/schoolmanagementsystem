package com.example.developer.schoolmanagementsystem.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Developer on 2/25/2018.
 */

public class LoginResponse {

    @SerializedName("data")
    @Expose
    private UserDetails data;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("HTTP_response_code")
    @Expose
    private Integer hTTPResponseCode;

    public UserDetails getData() {
        return data;
    }

    public void setData(UserDetails data) {
        this.data = data;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getHTTPResponseCode() {
        return hTTPResponseCode;
    }

    public void setHTTPResponseCode(Integer hTTPResponseCode) {
        this.hTTPResponseCode = hTTPResponseCode;
    }

}