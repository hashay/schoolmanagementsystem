package com.example.developer.schoolmanagementsystem.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.libreqAdapter;
import com.example.developer.schoolmanagementsystem.Adatper.paymentAdapter;
import com.example.developer.schoolmanagementsystem.Model.Invoice;
import com.example.developer.schoolmanagementsystem.Model.InvoiceResponse;
import com.example.developer.schoolmanagementsystem.Model.libreq;
import com.example.developer.schoolmanagementsystem.Model.pay;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class payment extends AppCompatActivity {
    private Call<InvoiceResponse> callbackCall;
    private List<Invoice> paymentList = new ArrayList<>();
    private RecyclerView recyclerView;
    private paymentAdapter mnAdapter;
    public String student_id;
    public KProgressHUD hud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        student_id =  getIntent().getExtras().getString("student_id","");
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setDetailsLabel("Downloading data")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        hud.show();


        Toolbar toolbar = (Toolbar) findViewById(R.id.studymaterial_toolbar); // check it
        setSupportActionBar(toolbar);

        // adding back functionality

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        recyclerView = (RecyclerView) findViewById(R.id.payment_recycler_view);

        mnAdapter = new paymentAdapter(PaymentByStudentId(student_id));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);





    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private List<Invoice> PaymentByStudentId(String student_id) {
        API api = RestAdapter.createAPI();
        callbackCall = api.Invoice(student_id);


        callbackCall.enqueue(new Callback<InvoiceResponse>() {
            @Override
            public void onResponse(Call<InvoiceResponse> call, Response<InvoiceResponse> response) {

                if (response.body().getSuccess()) {
                    hud.dismiss();
                    paymentList = response.body().getData();
                    setMaterialList(response.body().getData());


                } else {
                    Toast.makeText(payment.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<InvoiceResponse> call, Throwable t) {

                Toast.makeText(payment.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });
        return paymentList;

    }

    private void setMaterialList(List<Invoice> paymentList)
    {

        mnAdapter = new paymentAdapter(paymentList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);
    }





}
