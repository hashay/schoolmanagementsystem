package com.example.developer.schoolmanagementsystem.Adatper;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Model.TeacherStudyMaterialModel;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 3/26/2018.
 */

public class TeacherStudyMaterialAdapter extends RecyclerView.Adapter<TeacherStudyMaterialAdapter.MyViewHolder> {

    private List<TeacherStudyMaterialModel> StudyMaterialList;
    Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, date, description, subjectclass, subjectname;
        Button download;


        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.studymaterial_title);
            //date    = (TextView) view.findViewById(R.id.studymaterial_date);
            description = (TextView) view.findViewById(R.id.studymaterial_description);
            subjectclass = (TextView) view.findViewById(R.id.studymaterial_class);
            subjectname = (TextView) view.findViewById(R.id.studymaterial_subject);
            download = (Button) view.findViewById(R.id.studymaterial_download);
            download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(context, "set up ", Toast.LENGTH_SHORT).show();




                }
            });

        }
    }

    public TeacherStudyMaterialAdapter(List<TeacherStudyMaterialModel> moviesList, Context context) {
        this.StudyMaterialList = moviesList;
        this.context = context;
    }

    @Override
    public TeacherStudyMaterialAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.studymaterial_list, parent, false);

        return new TeacherStudyMaterialAdapter.MyViewHolder(itemView);
    }




    @Override
    public void onBindViewHolder(TeacherStudyMaterialAdapter.MyViewHolder holder, int position) {
        final TeacherStudyMaterialModel material = StudyMaterialList.get(position);
        Spanned StrDescp = Html.fromHtml(material.getDescription());
        String StrDescription = StrDescp.toString().replace("\r\n","");
        holder.title.setText(material.getTitle());
        // holder.genre.setText(data.getGenre());
        // holder.year.setText(data.getYear());
        //holder.date.setText(material.getDate());
        holder.description.setText(StrDescription);
        holder.subjectclass.setText(material.getClassName());
        holder.subjectname.setText(material.getSubjectName());
        holder.download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,material.getFileUrl().toString(), Toast.LENGTH_SHORT).show();

                //    String url = material.getFileUrl().toString();
                String url = material.getFileUrl().toString();
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                request.setDescription(material.getDescription().toString());
                request.setTitle(material.getFileName().toString());
// in order for this if to run, you must use the android 3.2 to compile your app
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    request.allowScanningByMediaScanner();
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                }
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, material.getFileName().toString());

// get download service and enqueue file
                DownloadManager manager = (DownloadManager)context.getSystemService(Context.DOWNLOAD_SERVICE);
                manager.enqueue(request);

                //  startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("www.education.gov.yk.ca/pdf/pdf-test.pdf")));

            }
        });
//        holder.download.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (onItemClickListener != null) {
//                    onItemClickListener.onItemClick(v, material);
//
//                }
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return StudyMaterialList.size();
    }


}
