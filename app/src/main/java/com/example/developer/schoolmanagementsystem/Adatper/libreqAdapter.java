package com.example.developer.schoolmanagementsystem.Adatper;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.developer.schoolmanagementsystem.Model.RequestBook;
import com.example.developer.schoolmanagementsystem.Model.libreq;
import com.example.developer.schoolmanagementsystem.Model.marks;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 2/19/2018.
 */

public class libreqAdapter extends RecyclerView.Adapter<libreqAdapter.MyViewHolder>{

    private List<RequestBook> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView book, requested , startdate , enddate , status;

        public MyViewHolder(View view) {
            super(view);
            book = (TextView) view.findViewById(R.id.libreq_bookname);
            requested    = (TextView) view.findViewById(R.id.libreq_reqby);
            startdate = (TextView) view.findViewById(R.id.libreq_datestart);
            enddate = (TextView) view.findViewById(R.id.libreq_dateend);
            status = (TextView) view.findViewById(R.id.libreq_requeststatus);

        }
    }

    public libreqAdapter(List<RequestBook> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public libreqAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.requestbook_list, parent, false);

        return new libreqAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(libreqAdapter.MyViewHolder holder, int position) {
        RequestBook material = moviesList.get(position);
        holder.book.setText(material.getBookName());
        // holder.genre.setText(data.getGenre());
        // holder.year.setText(data.getYear());
        holder.requested.setText(material.getStudentName());
        holder.startdate.setText(material.getIssueStartDate());
        holder.enddate.setText(material.getIssueEndDate());
        holder.status.setText(material.getStatus());



    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}
