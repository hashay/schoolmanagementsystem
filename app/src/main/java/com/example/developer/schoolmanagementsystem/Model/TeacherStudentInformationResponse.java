package com.example.developer.schoolmanagementsystem.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Developer on 3/8/2018.
 */

public class TeacherStudentInformationResponse {

    @SerializedName("data")
    @Expose
    private List<TeachersStudentInformation> data = null;
    @SerializedName("marks_sheet")
    @Expose
    private List<TeacherStudentProfileExam> marksSheet = null;
    @SerializedName("total_marks")
    @Expose
    private Integer totalMarks;
    @SerializedName("avg_grade_point")
    @Expose
    private Integer avgGradePoint;
    @SerializedName("invoices")
    @Expose
    private List<TeacherStudentProfilePayment> invoices = null;
    @SerializedName("subjects")
    @Expose
    private List<TeacherStudentInformationSubject> subjects = null;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("HTTP_response_code")
    @Expose
    private Integer hTTPResponseCode;

    public List<TeachersStudentInformation> getData() {
        return data;
    }

    public void setData(List<TeachersStudentInformation> data) {
        this.data = data;
    }

    public List<TeacherStudentProfileExam> getMarksSheet() {
        return marksSheet;
    }

    public void setMarksSheet(List<TeacherStudentProfileExam> marksSheet) {
        this.marksSheet = marksSheet;
    }

    public Integer getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(Integer totalMarks) {
        this.totalMarks = totalMarks;
    }

    public Integer getAvgGradePoint() {
        return avgGradePoint;
    }

    public void setAvgGradePoint(Integer avgGradePoint) {
        this.avgGradePoint = avgGradePoint;
    }

    public List<TeacherStudentProfilePayment> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<TeacherStudentProfilePayment> invoices) {
        this.invoices = invoices;
    }

    public List<TeacherStudentInformationSubject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<TeacherStudentInformationSubject> subjects) {
        this.subjects = subjects;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getHTTPResponseCode() {
        return hTTPResponseCode;
    }

    public void setHTTPResponseCode(Integer hTTPResponseCode) {
        this.hTTPResponseCode = hTTPResponseCode;
    }

}
