package com.example.developer.schoolmanagementsystem.Model;

/**
 * Created by Developer on 2/19/2018.
 */

public class pay {

    private String title, description,amount, status, date ;

    public pay(String title,String description, String amount, String date, String status){
        this.title = title;
        this.description = description;
        this.amount = amount;
        this.date = date;
        this.status = status;



    }

    public String getTitle(){
        return  title;

    }

    public void setTitle(String name){
        this.title = name;

    }

    public String getDescription(){
        return description;
    }

    public void  setDescription(String name){
        this.description = name;
    }

    public String getAmount(){
        return amount;
    }

    public void setAmount(String name){
        this.amount = name;
    }

    public String getDate(){
        return date;
    }

    public void setDate(String name){
        this.date = name;
    }

    public String getStatus(){
        return status;
    }

    public void setStatus(String name){

        this.status = name;
    }

}
