package com.example.developer.schoolmanagementsystem.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Developer on 2/26/2018.
 */

public class TeacherDetails {


    @SerializedName("teacher_id")
    @Expose
    private String teacherId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("religion")
    @Expose
    private Object religion;
    @SerializedName("blood_group")
    @Expose
    private Object bloodGroup;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("authentication_key")
    @Expose
    private Object authenticationKey;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("social_links")
    @Expose
    private String socialLinks;
    @SerializedName("show_on_website")
    @Expose
    private String showOnWebsite;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Object getReligion() {
        return religion;
    }

    public void setReligion(Object religion) {
        this.religion = religion;
    }

    public Object getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(Object bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Object getAuthenticationKey() {
        return authenticationKey;
    }

    public void setAuthenticationKey(Object authenticationKey) {
        this.authenticationKey = authenticationKey;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getSocialLinks() {
        return socialLinks;
    }

    public void setSocialLinks(String socialLinks) {
        this.socialLinks = socialLinks;
    }

    public String getShowOnWebsite() {
        return showOnWebsite;
    }

    public void setShowOnWebsite(String showOnWebsite) {
        this.showOnWebsite = showOnWebsite;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}