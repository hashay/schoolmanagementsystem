package com.example.developer.schoolmanagementsystem.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Model.TeacherDetails;
import com.example.developer.schoolmanagementsystem.Model.TeacherDetailsResponse;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Adatper.dataAdapter;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class teachers extends AppCompatActivity {
    private Call<TeacherDetailsResponse> callbackCall;
    //private List<data> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private dataAdapter mnAdapter;
    List<TeacherDetails> teacherDetails = new ArrayList<>();

    public String student_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teachers);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_teacher);
        setSupportActionBar(toolbar);
        if(getIntent().getStringExtra("student_id") != null){
            student_id =  getIntent().getExtras().getString("student_id","");

        }else if (getIntent().getStringExtra("teacher_id") != null){
            student_id = getIntent().getExtras().getString("teacher_id","");
        }else if (getIntent().getStringExtra("parent_id") != null) {
            student_id = getIntent().getExtras().getString("parent_id", "");
        }
        // adding back functionality

       getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       getSupportActionBar().setDisplayShowHomeEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.teacher_recycler_view);

        LoadTeacherList();















    }

    private void LoadTeacherList() {
        mnAdapter = new dataAdapter(teacherDetailsByStId(student_id),this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private List<TeacherDetails> teacherDetailsByStId(String student_id) {
        API api = RestAdapter.createAPI();
        callbackCall = api.teacher(student_id);


        callbackCall.enqueue(new Callback<TeacherDetailsResponse>() {
            @Override
            public void onResponse(Call<TeacherDetailsResponse> call, Response<TeacherDetailsResponse> response) {

                if (response.body().getSuccess()) {
//

                    teacherDetails = response.body().getData();
                    setTeacherList(response.body().getData());


                } else {
                    Toast.makeText(teachers.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<TeacherDetailsResponse> call, Throwable t) {

                Toast.makeText(teachers.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });
        return teacherDetails;

    }

    private void setTeacherList(List<TeacherDetails> teacherList)
    {

        mnAdapter = new dataAdapter(teacherList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);
    }




}
