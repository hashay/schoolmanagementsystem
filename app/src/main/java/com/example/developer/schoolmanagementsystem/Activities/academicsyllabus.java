package com.example.developer.schoolmanagementsystem.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.materialAdapter;
import com.example.developer.schoolmanagementsystem.Adatper.syllabusAdapter;
import com.example.developer.schoolmanagementsystem.Model.AcademicSyllabus;
import com.example.developer.schoolmanagementsystem.Model.AcademicSyllabusResponse;
import com.example.developer.schoolmanagementsystem.Model.material;
import com.example.developer.schoolmanagementsystem.Model.syllabus;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class academicsyllabus extends AppCompatActivity {
    public String   student_id;
    private List<AcademicSyllabus> syllabusList = new ArrayList<>();
    private RecyclerView recyclerView;
    private syllabusAdapter mnAdapter;
    private Call<AcademicSyllabusResponse> callbackCall;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_academicsyllabus);
        student_id =  getIntent().getExtras().getString("student_id","");
        Toolbar toolbar = (Toolbar) findViewById(R.id.studymaterial_toolbar); // check it
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        recyclerView = (RecyclerView) findViewById(R.id.syllabus_recycler_view);



        mnAdapter = new syllabusAdapter(SyllabusById(student_id), this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);


        prepareTeacherData();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private List<AcademicSyllabus> SyllabusById(String student_id) {
        API api = RestAdapter.createAPI();
        callbackCall = api.AcademicSyllabus(student_id);


        callbackCall.enqueue(new Callback<AcademicSyllabusResponse>() {
            @Override
            public void onResponse(Call<AcademicSyllabusResponse> call, Response<AcademicSyllabusResponse> response) {

                if (response.body().getSuccess()) {
//
                    Toast.makeText(academicsyllabus.this,"success",Toast.LENGTH_SHORT).show();
                    syllabusList = response.body().getData();
                    setTeacherList(response.body().getData());

                } else {
                    Toast.makeText(academicsyllabus.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AcademicSyllabusResponse> call, Throwable t) {

                Toast.makeText(academicsyllabus.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });
        return syllabusList;

    }

    private void setTeacherList(List<AcademicSyllabus> teacherList)
    {

        mnAdapter = new syllabusAdapter(syllabusList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);
    }


    private void prepareTeacherData() {
//        syllabus material = new syllabus("A happy life","25/01/2015","hello i am hashim", "programming" , "Wong lee");
//        movieList.add(material);
//
//        material = new syllabus("A sad life","2/11/2017","hello i am hashim,hello i am hashim,hello i am hashim,hello i am hashimhello i am hashim", "programming" , "hawken");
//        movieList.add(material);
//
//
//        mnAdapter.notifyDataSetChanged();
    }
}
