package com.example.developer.schoolmanagementsystem.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.TeacherStudyMaterialAdapter;
import com.example.developer.schoolmanagementsystem.Adatper.dataAdapter;
import com.example.developer.schoolmanagementsystem.Adatper.materialAdapter;
import com.example.developer.schoolmanagementsystem.Model.StudentStudyMaterial;
import com.example.developer.schoolmanagementsystem.Model.StudentStudyMaterialResponse;
import com.example.developer.schoolmanagementsystem.Model.TeacherStudyMaterialModel;
import com.example.developer.schoolmanagementsystem.Model.TeacherStudyMaterialResponse;
import com.example.developer.schoolmanagementsystem.Model.data;
import com.example.developer.schoolmanagementsystem.Model.material;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class studymaterial extends AppCompatActivity {
    private Call<StudentStudyMaterialResponse> callbackCall;
    private Call<TeacherStudyMaterialResponse> TeacherStudyMaterialCallBackCall;
    private List<TeacherStudyMaterialModel> studyMaterialList = new ArrayList<>();
    private List<StudentStudyMaterial> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private materialAdapter mnAdapter;
    private TeacherStudyMaterialAdapter TStudyMaterialAdapter;
    public  String student_id, teacher_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studymaterial);

        Toolbar toolbar = (Toolbar) findViewById(R.id.studymaterial_toolbar); // check it
        setSupportActionBar(toolbar);
        if(getIntent().getStringExtra("student_id")!= null){
            student_id =  getIntent().getExtras().getString("student_id","");
            recyclerView = (RecyclerView) findViewById(R.id.studymaterial_recycler_view);
            LoadStudentStudyMaterial();
        } else if(getIntent().getStringExtra("teacher_id") != null ){
            teacher_id =  getIntent().getExtras().getString("teacher_id","");
            recyclerView = (RecyclerView) findViewById(R.id.studymaterial_recycler_view);
            LoadStudyMaterialByTeacherId();

        }

        // adding back functionality

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);









    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private List<StudentStudyMaterial> StudyMaterialsById(String student_id) {
        API api = RestAdapter.createAPI();
        callbackCall = api.StudentStudyMaterial(student_id);


        callbackCall.enqueue(new Callback<StudentStudyMaterialResponse>() {
            @Override
            public void onResponse(Call<StudentStudyMaterialResponse> call, Response<StudentStudyMaterialResponse> response) {

                if (response.body().getSuccess()) {
//

                    movieList = response.body().getData();
                    setMaterialList(response.body().getData());

                } else {
                    Toast.makeText(studymaterial.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<StudentStudyMaterialResponse> call, Throwable t) {

                Toast.makeText(studymaterial.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });
        return movieList;

    }

    private void setMaterialList(List<StudentStudyMaterial> materialList)
    {

        mnAdapter = new materialAdapter(materialList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);
    }


  public void LoadStudyMaterialByTeacherId(){
      TStudyMaterialAdapter = new TeacherStudyMaterialAdapter(TeacherStudyMaterial(teacher_id),this);

      RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
      recyclerView.setLayoutManager(mLayoutManager);

      recyclerView.setItemAnimator(new DefaultItemAnimator());
      recyclerView.setAdapter(TStudyMaterialAdapter);
  }

    private List<TeacherStudyMaterialModel> TeacherStudyMaterial(String teacher_id) {
        API api = RestAdapter.createAPI();
        TeacherStudyMaterialCallBackCall = api.TEACHER_STUDY_MATERIAL_RESPONSE_CALL(teacher_id);


        TeacherStudyMaterialCallBackCall.enqueue(new Callback<TeacherStudyMaterialResponse>() {
            @Override
            public void onResponse(Call<TeacherStudyMaterialResponse> call, Response<TeacherStudyMaterialResponse> response) {

                if (response.body().getSuccess()) {
//

                    studyMaterialList = response.body().getData();
                    setStudyList(response.body().getData());


                } else {
                    Toast.makeText(studymaterial.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<TeacherStudyMaterialResponse> call, Throwable t) {

                Toast.makeText(studymaterial.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });
        return studyMaterialList;

    }

    private void setStudyList(List<TeacherStudyMaterialModel> materialList)
    {

        TStudyMaterialAdapter = new TeacherStudyMaterialAdapter(materialList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(TStudyMaterialAdapter);
    }

    public void LoadStudentStudyMaterial(){
        mnAdapter = new materialAdapter(StudyMaterialsById(student_id),this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);
    }
}
