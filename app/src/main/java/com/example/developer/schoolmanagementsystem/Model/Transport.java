package com.example.developer.schoolmanagementsystem.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Developer on 3/2/2018.
 */

public class Transport {

    @SerializedName("transport_id")
    @Expose
    private String transportId;
    @SerializedName("route_name")
    @Expose
    private String routeName;
    @SerializedName("number_of_vehicle")
    @Expose
    private String numberOfVehicle;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("route_fare")
    @Expose
    private String routeFare;

    public String getTransportId() {
        return transportId;
    }

    public void setTransportId(String transportId) {
        this.transportId = transportId;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getNumberOfVehicle() {
        return numberOfVehicle;
    }

    public void setNumberOfVehicle(String numberOfVehicle) {
        this.numberOfVehicle = numberOfVehicle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRouteFare() {
        return routeFare;
    }

    public void setRouteFare(String routeFare) {
        this.routeFare = routeFare;
    }
}
