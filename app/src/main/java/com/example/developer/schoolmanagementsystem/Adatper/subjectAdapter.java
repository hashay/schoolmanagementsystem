package com.example.developer.schoolmanagementsystem.Adatper;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.developer.schoolmanagementsystem.Model.Movie;
import com.example.developer.schoolmanagementsystem.Model.SubjectDetails;
import com.example.developer.schoolmanagementsystem.Model.data;
import com.example.developer.schoolmanagementsystem.Model.sbt;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 2/16/2018.
 */

public class subjectAdapter extends RecyclerView.Adapter<subjectAdapter.MyViewHolder> {



    private List<SubjectDetails> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.subject_name);
            genre = (TextView) view.findViewById(R.id.class_name);
            year = (TextView) view.findViewById(R.id.subject_teacher_name);

        }
    }

    public subjectAdapter(List<SubjectDetails> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public subjectAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.subject_list, parent, false);

        return new subjectAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(subjectAdapter.MyViewHolder holder, int position) {
        SubjectDetails data = moviesList.get(position);
        holder.title.setText(data.getName());
        holder.genre.setText(data.getClassId());
        holder.year.setText(data.getTeacherName());


    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }




}
