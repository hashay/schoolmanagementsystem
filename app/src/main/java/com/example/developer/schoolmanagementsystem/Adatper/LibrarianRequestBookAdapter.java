package com.example.developer.schoolmanagementsystem.Adatper;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.developer.schoolmanagementsystem.Model.LibrarianBookRequest;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 3/22/2018.
 */

public class LibrarianRequestBookAdapter extends RecyclerView.Adapter<LibrarianRequestBookAdapter.MyViewHolder>{

    private List<LibrarianBookRequest> moviesList;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView book, requested , startdate , enddate , status;

        public MyViewHolder(View view) {
            super(view);
            book = (TextView) view.findViewById(R.id.libreq_bookname);
            requested    = (TextView) view.findViewById(R.id.libreq_reqby);
            startdate = (TextView) view.findViewById(R.id.libreq_datestart);
            enddate = (TextView) view.findViewById(R.id.libreq_dateend);
            status = (TextView) view.findViewById(R.id.libreq_requeststatus);

        }
    }

    public LibrarianRequestBookAdapter(List<LibrarianBookRequest> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public LibrarianRequestBookAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.requestbook_list, parent, false);

        return new LibrarianRequestBookAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(LibrarianRequestBookAdapter.MyViewHolder holder, int position) {
        LibrarianBookRequest material = moviesList.get(position);
        holder.book.setText(material.getBookName());
        // holder.genre.setText(data.getGenre());
        // holder.year.setText(data.getYear());
        holder.requested.setText(material.getStudentName());
        holder.startdate.setText(material.getIssueStartDate());
        holder.enddate.setText(material.getIssueEndDate());
        holder.status.setText(material.getStatus());
        if(material.getStatus().equals("Issued")){
            holder.status.setTextColor(Color.parseColor("#ffffff"));
            holder.status.setBackgroundResource(R.drawable.paidbtn);
        }else if(material.getStatus().equals("Rejected")){
            holder.status.setTextColor(Color.parseColor("#ffffff"));
            holder.status.setBackgroundResource(R.drawable.unpaidbtn);
        } else {
            holder.status.setTextColor(Color.parseColor("#ffffff"));
            holder.status.setBackgroundResource(R.drawable.btn_pending);

        }
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
