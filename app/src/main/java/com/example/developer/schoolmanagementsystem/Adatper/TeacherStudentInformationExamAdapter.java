package com.example.developer.schoolmanagementsystem.Adatper;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.developer.schoolmanagementsystem.Model.TeacherStudentProfileExam;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 3/8/2018.
 */

public class TeacherStudentInformationExamAdapter extends RecyclerView.Adapter<TeacherStudentInformationExamAdapter.MyViewHolder> {

    private List<TeacherStudentProfileExam> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, obtained , highest , grade , comment;


        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.exammarks_subject);
            obtained    = (TextView) view.findViewById(R.id.exam_obtained);
            highest = (TextView) view.findViewById(R.id.exam_highest);
            grade = (TextView) view.findViewById(R.id.exam_grade);
            comment = (TextView) view.findViewById(R.id.exam_comment);


        }
    }

    public TeacherStudentInformationExamAdapter(List<TeacherStudentProfileExam> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public TeacherStudentInformationExamAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.exammarks_list, parent, false);

        return new TeacherStudentInformationExamAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(TeacherStudentInformationExamAdapter.MyViewHolder holder, int position) {
        TeacherStudentProfileExam material = moviesList.get(position);
        holder.title.setText(material.getSubjectName());
        // holder.genre.setText(data.getGenre());
        // holder.year.setText(data.getYear());
        holder.obtained.setText(String.valueOf(material.getMarkObtained()));
        holder.highest.setText(String.valueOf(material.getHighestMark()));
        holder.grade.setText(String.valueOf(material.getGradeName()));
        holder.comment.setText(String.valueOf(material.getComment()));



    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}
