package com.example.developer.schoolmanagementsystem.Adatper;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.developer.schoolmanagementsystem.Model.TeacherClassStudentsSection;
import com.example.developer.schoolmanagementsystem.Model.classstudentssection;
import com.example.developer.schoolmanagementsystem.Model.routineclassdays;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 2/20/2018.
 */

public class classstudentssectionAdapter extends RecyclerView.Adapter<classstudentssectionAdapter.MyViewHolder> {
    private List<TeacherClassStudentsSection> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView section;

        public MyViewHolder(View view) {
            super(view);
            section = (TextView) view.findViewById(R.id.classstudents_section);



        }
    }

    public classstudentssectionAdapter(List<TeacherClassStudentsSection> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public classstudentssectionAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.classstudents_section_list, parent, false);

        return new classstudentssectionAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(classstudentssectionAdapter.MyViewHolder holder, int position) {
        TeacherClassStudentsSection data = moviesList.get(position);
       // holder.section.setText(data.getSections());

    }



    @Override
    public int getItemCount() {
        return moviesList.size();
    }




}
