package com.example.developer.schoolmanagementsystem.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.MovieAdapter;
import com.example.developer.schoolmanagementsystem.Model.Movie;
import com.example.developer.schoolmanagementsystem.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarListener;

import static com.example.developer.schoolmanagementsystem.Activities.login.MY_PREFS_NAME;
import static com.example.developer.schoolmanagementsystem.Activities.login.MY_PREFS_NAME2;

public class librarian extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private HorizontalCalendar horizontalCalendar;



    private List<Movie> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MovieAdapter mAdapter;
    public String librarian_id,librarian_name,username;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_librarian);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        librarian_id =  getIntent().getExtras().getString("Librarian","");
        librarian_name =  getIntent().getExtras().getString("librarian_name","");

        // shared preferences

        String MY_PREFS_NAME = "MyPrefsFile";
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);

        username = prefs.getString("username", null);
        //

        calender();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        //intializing number of columns

        int numberOfColumns = 3;

        mAdapter = new MovieAdapter(movieList);
        //opening screen by getting data from card view
        mAdapter.setOnItemClickListener(new MovieAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, Movie obj) {
                //  Toast.makeText(student.this, "Toast"+obj.getTitle(), Toast.LENGTH_SHORT).show();

                if(obj.getTitle() == "Book List") {
                    Intent intent = new Intent(getApplicationContext(), lbbooklist.class);
                    intent.putExtra("librarian_id",librarian_id);
                    startActivity(intent);
                    //finish();)

                } else if(obj.getTitle() == "Book Requests") {
                    Intent intent = new Intent(getApplicationContext(), requestbook.class);
                    intent.putExtra("librarian_id",librarian_id);
                    startActivity(intent);
                    //finish();)

                }

                else{
                }

            }


        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());



        recyclerView.setLayoutManager(new GridLayoutManager(this, numberOfColumns));

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        prepareMovieData();



        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){


        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_librarian);
        View Hview = navigationView.getHeaderView(0);
        TextView nav_useremail = (TextView)Hview.findViewById(R.id.lib_email);
        TextView nav_username = (TextView)Hview.findViewById(R.id.lib_name);
        nav_username.setText(librarian_name);
        nav_useremail.setText(username);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void prepareMovieData() {
//        Movie movie = new Movie(R.drawable.addbook, "Add Book");
//        movieList.add(movie);

        Movie movie = new Movie(R.drawable.bookrequest, "Book Requests");
        movieList.add(movie);

        movie = new Movie(R.drawable.booklist, "Book List");
        movieList.add(movie);




        mAdapter.notifyDataSetChanged();
    }


    public void calender(){

        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);

        horizontalCalendar = new HorizontalCalendar.Builder(librarian.this    , R.id.calendarView)
                .startDate(startDate.getTime())
                .endDate(endDate.getTime())
                .datesNumberOnScreen(5)
                .dayNameFormat("EEE")
                .dayNumberFormat("dd")
                .monthFormat("MMM")
                .textSize(14f, 24f, 14f)
                .showDayName(true)
                .showMonthName(true)

                .build();

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Date date, int position) {
//                Toast.makeText(getContext(), DateFormat.getDateInstance().format(dateEditText) + " is selected!", Toast.LENGTH_SHORT).show();
            }

        });
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.librarian, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.lib_logout) {
            logout();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.lb_book_list) {
            Intent intent = new Intent(getApplicationContext(), lbbooklist.class);
            intent.putExtra("librarian_id",librarian_id);
            startActivity(intent);
            // Handle the camera action
        } else if (id == R.id.lb_book_request) {
            Intent intent = new Intent(getApplicationContext(), requestbook.class);
            intent.putExtra("librarian_id",librarian_id);
            startActivity(intent);
        }else if (id == R.id.lb_logout) {
            logout();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void logout(){
        getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit().clear().commit();
        getSharedPreferences(MY_PREFS_NAME2, MODE_PRIVATE).edit().clear().commit();
        Intent intent = new Intent(getApplicationContext(), login.class);
        startActivity(intent);
        finish();
    }
}
