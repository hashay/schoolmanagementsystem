package com.example.developer.schoolmanagementsystem.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Developer on 3/6/2018.
 */

public class RequestBookForm {

    @SerializedName("book_id")
    @Expose
    private String bookId;
    @SerializedName("student_id")
    @Expose
    private String studentId;
    @SerializedName("issue_start_date")
    @Expose
    private Integer issueStartDate;
    @SerializedName("issue_end_date")
    @Expose
    private Integer issueEndDate;

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public Integer getIssueStartDate() {
        return issueStartDate;
    }

    public void setIssueStartDate(Integer issueStartDate) {
        this.issueStartDate = issueStartDate;
    }

    public Integer getIssueEndDate() {
        return issueEndDate;
    }

    public void setIssueEndDate(Integer issueEndDate) {
        this.issueEndDate = issueEndDate;
    }

}
