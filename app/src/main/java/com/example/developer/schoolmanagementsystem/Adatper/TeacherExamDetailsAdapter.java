package com.example.developer.schoolmanagementsystem.Adatper;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.developer.schoolmanagementsystem.Model.TeacherExamDetailsModel;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 3/19/2018.
 */

public class TeacherExamDetailsAdapter extends RecyclerView.Adapter<TeacherExamDetailsAdapter.MyViewHolder>{

    private List<TeacherExamDetailsModel> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, obtained , total;


        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.exammarks_sname);
            obtained    = (TextView) view.findViewById(R.id.exam_obtained);
            total = (TextView) view.findViewById(R.id.exam_highest);



        }
    }

    public TeacherExamDetailsAdapter(List<TeacherExamDetailsModel> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public TeacherExamDetailsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.teacher_exam_list, parent, false);

        return new TeacherExamDetailsAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(TeacherExamDetailsAdapter.MyViewHolder holder, int position) {
        TeacherExamDetailsModel material = moviesList.get(position);
        holder.name.setText(material.getStudentName());
        // holder.genre.setText(data.getGenre());
        // holder.year.setText(data.getYear());
        holder.obtained.setText(String.valueOf(material.getMarkObtained()));
        holder.total.setText(String.valueOf(material.getMarkTotal()));




    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }


}
