package com.example.developer.schoolmanagementsystem.Adatper;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.developer.schoolmanagementsystem.Model.PAttendanceModel;
import com.example.developer.schoolmanagementsystem.Model.pattend;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 2/22/2018.
 */

public class pattendAdapter extends RecyclerView.Adapter<pattendAdapter.MyViewHolder> {

    private List<PAttendanceModel> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView date, status;

        public MyViewHolder(View view) {
            super(view);
            date = (TextView) view.findViewById(R.id.attendday);
            status = (TextView) view.findViewById(R.id.attend);


        }




    }


    public pattendAdapter(List<PAttendanceModel> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public pattendAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pattendance_list, parent, false);

        return new pattendAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(pattendAdapter.MyViewHolder holder, int position) {
        PAttendanceModel material = moviesList.get(position);
        holder.date.setText(material.getDate());
        // holder.genre.setText(data.getGenre());
        // holder.year.setText(data.getYear());


        holder.status.setText(String.valueOf(material.getStatus()));
        if(String.valueOf(material.getStatus()).equals("1")){
            holder.status.setText("Present");
            holder.status.setTextColor(Color.parseColor("#ffffff"));
            holder.status.setBackgroundResource(R.drawable.paidbtn);
        }else if(String.valueOf(material.getStatus()).equals("2")){
            holder.status.setText("Absent");
            holder.status.setTextColor(Color.parseColor("#ffffff"));
            holder.status.setBackgroundResource(R.drawable.unpaidbtn);
        }else{
            holder.status.setText("Not Entered");
            holder.status.setTextColor(Color.parseColor("#D3D3D3"));
        }





    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }


}

