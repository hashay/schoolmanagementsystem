package com.example.developer.schoolmanagementsystem.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Model.ParentChild;
import com.example.developer.schoolmanagementsystem.Model.ParentChildResponse;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class pclassroutinestudent extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    public Spinner ShowChilds;
    List<ParentChild> ChildList;
    Call<ParentChildResponse> ChildCallBackCall;
    public String parent_id,student_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pclassroutinestudent);
        parent_id =  getIntent().getExtras().getString("parent_id","");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_subject);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ShowChilds = (Spinner) findViewById(R.id.sp_academic);
        ShowParentChild(parent_id);
        ParentChildSpinnerOnSelect();

    }

    private void ShowParentChild(String parent_id) {
        API api = RestAdapter.createAPI();
        ChildCallBackCall = api.PARENT_CHILD_RESPONSE_CALL(parent_id);

        ChildCallBackCall.enqueue(new Callback<ParentChildResponse>() {
            @Override
            public void onResponse(Call<ParentChildResponse> call, Response<ParentChildResponse> response) {

                if (response.body().getSuccess()) {
                    ChildList = response.body().getData();
                    List<String> listSpinner = new ArrayList<String>();
                    listSpinner.clear();

                    for (int i = 0; i < ChildList.size(); i++){
                        listSpinner.add(ChildList.get(i).getName());
                        ArrayAdapter<String> csAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.custom_spinner_box, listSpinner);
                        csAdapter.setDropDownViewResource(R.layout.custom_dropdown_spinner_box);
                        ShowChilds.setAdapter(csAdapter);


                    }
                    student_id = response.body().getData().get(0).getStudentId();

                    // Toast.makeText(teacherclassroutine.this,,Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(pclassroutinestudent.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ParentChildResponse> call, Throwable t) {

                Toast.makeText(pclassroutinestudent.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void ParentChildSpinnerOnSelect() {
        ShowChilds.setOnItemSelectedListener(this);
        ShowChilds.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> obj, View view, int position, long id) {

                student_id = ChildList.get(position).getStudentId();



            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void btnclassroutine(View v)
    {
        Intent intent = new Intent(getApplicationContext(), classroutine.class);
        intent.putExtra("student_id",student_id);
        startActivity(intent);
        //finish();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
