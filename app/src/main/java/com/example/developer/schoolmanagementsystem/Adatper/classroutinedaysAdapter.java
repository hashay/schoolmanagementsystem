package com.example.developer.schoolmanagementsystem.Adatper;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.developer.schoolmanagementsystem.Model.routineclass;
import com.example.developer.schoolmanagementsystem.Model.routineclassdays;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 2/19/2018.
 */

public class classroutinedaysAdapter  extends RecyclerView.Adapter<classroutinedaysAdapter.MyViewHolder> {
    private List<routineclassdays> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView day;

        public MyViewHolder(View view) {
            super(view);
            day = (TextView) view.findViewById(R.id.day_name);



        }
    }

    public classroutinedaysAdapter(List<routineclassdays> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public classroutinedaysAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.classroutine_day_list, parent, false);

        return new classroutinedaysAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(classroutinedaysAdapter.MyViewHolder holder, int position) {
        routineclassdays data = moviesList.get(position);
        holder.day.setText(data.getDays());

    }



    @Override
    public int getItemCount() {
        return moviesList.size();
    }




}

