package com.example.developer.schoolmanagementsystem.Model;

/**
 * Created by Developer on 2/19/2018.
 */

public class marks {

    private String subject, obtained,highest, grade, comment ;

    public marks(String subject,String obtained, String highest, String grade, String comment){
        this.subject = subject;
        this.obtained = obtained;
        this.highest = highest;
        this.grade = grade;
        this.comment = comment;



    }

    public String getSubject(){
        return  subject;

    }

    public void setSubject(String name){
        this.subject = name;

    }

    public String getObtained(){
        return obtained;
    }

    public void  setObtained(String name){
        this.subject = name;
    }

    public String getHighest(){
        return highest;
    }

    public void setHighest(String name){
        this.highest = name;
    }

    public String getGrade(){
        return grade;
    }

    public void setGrade(String name){
        this.grade = name;
    }

    public String getComment(){
        return comment;
    }

    public void setComment(String name){

        this.comment = name;
    }

}
