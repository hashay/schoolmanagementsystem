package com.example.developer.schoolmanagementsystem.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.TeacherStudentClassForSubjectAdapter;
import com.example.developer.schoolmanagementsystem.Adatper.TeacherStudentClassForSyllabusAdapter;
import com.example.developer.schoolmanagementsystem.Adatper.studentclassnameAdapter;
import com.example.developer.schoolmanagementsystem.Model.TeacherClassesResponse;
import com.example.developer.schoolmanagementsystem.Model.TeachersClasses;
import com.example.developer.schoolmanagementsystem.Model.studentclass;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class selectclassforsyllabus extends AppCompatActivity {


    private List<TeachersClasses> classButtons = new ArrayList<>();
    private RecyclerView recyclerView;
    private TeacherStudentClassForSyllabusAdapter mnAdapter;
    private Call<TeacherClassesResponse> callbackCall;
    public String teacher_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selectclassforsyllabus);
        teacher_id =  getIntent().getExtras().getString("teacher_id","");
        Toolbar toolbar = (Toolbar) findViewById(R.id.studymaterial_toolbar); // check it
        setSupportActionBar(toolbar);

        // adding back functionality

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        recyclerView = (RecyclerView) findViewById(R.id.studentinformation_recycler_view);
        LoadClassList();




    }

    private void LoadClassList() {
        mnAdapter = new TeacherStudentClassForSyllabusAdapter(ClassListByTeacherId(teacher_id),this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);
    }

    private List<TeachersClasses> ClassListByTeacherId(String teacher_id) {
        API api = RestAdapter.createAPI();
        callbackCall = api.TeacherClassesResponse(teacher_id);


        callbackCall.enqueue(new Callback<TeacherClassesResponse>() {
            @Override
            public void onResponse(Call<TeacherClassesResponse> call, Response<TeacherClassesResponse> response) {

                if (response.body().getSuccess()) {
//

                    classButtons = response.body().getData();
                    setTeacherList(response.body().getData());

                } else {
                    Toast.makeText(selectclassforsyllabus.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<TeacherClassesResponse> call, Throwable t) {
                Log.e("Error","--"+t.getMessage());

                Toast.makeText(selectclassforsyllabus.this, "incorrect"+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return classButtons;

    }

    private void setTeacherList(List<TeachersClasses> classbuttons)
    {

        mnAdapter = new TeacherStudentClassForSyllabusAdapter(classButtons,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);
    }




    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
