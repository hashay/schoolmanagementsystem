package com.example.developer.schoolmanagementsystem.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.TeacherExamDetailsAdapter;
import com.example.developer.schoolmanagementsystem.Model.ExamClassModel;
import com.example.developer.schoolmanagementsystem.Model.ExamClassModelResponse;
import com.example.developer.schoolmanagementsystem.Model.ExamList;
import com.example.developer.schoolmanagementsystem.Model.ExamListResponse;
import com.example.developer.schoolmanagementsystem.Model.ExamSection;
import com.example.developer.schoolmanagementsystem.Model.ExamSectionResponse;
import com.example.developer.schoolmanagementsystem.Model.ExamSubjectsModel;
import com.example.developer.schoolmanagementsystem.Model.ExamSubjectsResponse;
import com.example.developer.schoolmanagementsystem.Model.TeacherExamDetailsModel;
import com.example.developer.schoolmanagementsystem.Model.TeacherExamDetailsResponse;
import com.example.developer.schoolmanagementsystem.Model.marks;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class managemarks extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private retrofit2.Call<ExamClassModelResponse> ExamCallBackCall;
    private retrofit2.Call<ExamListResponse> ExamListCallBackCall;
    private Call<ExamSectionResponse> ExamSectionCallBackCall;
    private Call<ExamSubjectsResponse> ExamSubjectCallBackCall;
    private Call<TeacherExamDetailsResponse> ExamDetailCallBackCall;
    public String teacher_id,class_id,exam_id,subject_id;
    private List<TeacherExamDetailsModel> ExamDetailsList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TeacherExamDetailsAdapter ExamDetailsAdapter;
    public Spinner ShowExam,ShowClass,ShowSection,ShowSubject;
    List<ExamSubjectsModel> ExamSubjectList;
    List<ExamSection> ExamSectionList;
    List<ExamClassModel> ExamClassList;
    List<ExamList> ExamList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_managemarks);
        teacher_id =  getIntent().getExtras().getString("teacher_id","");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_subject);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        initializations();
        loadExamData(teacher_id);
        loadClassData(teacher_id);
        classSpinnerOnClick();
        ExamSpinnerOnClick();
        ExamSubjectSpinnerOnClick();

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String item = adapterView.getItemAtPosition(i).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void initializations(){
        ShowExam = (Spinner) findViewById(R.id.tsp_exam);
        ShowClass = (Spinner) findViewById(R.id.tsp_class);
        ShowSection = (Spinner) findViewById(R.id.tsp_section);
        ShowSubject = (Spinner) findViewById(R.id.tsp_subject);
        recyclerView = (RecyclerView) findViewById(R.id.exammarks_recycler_view);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void loadClassData(String teacher_id) {
        API api = RestAdapter.createAPI();
        ExamCallBackCall = api.EXAM_CLASS_MODEL_RESPONSE_CALL(teacher_id);

        ExamCallBackCall.enqueue(new Callback<ExamClassModelResponse>() {
            @Override
            public void onResponse(retrofit2.Call<ExamClassModelResponse> call, Response<ExamClassModelResponse> response) {

                if (response.body().getHTTPResponseCode().equals(200)) {
                    ExamClassList = response.body().getData();
                    List<String> listSpinner = new ArrayList<String>();
                    listSpinner.clear();

                    for (int i = 0; i < ExamClassList.size(); i++){
                        listSpinner.add(ExamClassList.get(i).getName());
                        ArrayAdapter<String> csAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.custom_spinner_box, listSpinner);
                        csAdapter.setDropDownViewResource(R.layout.custom_dropdown_spinner_box);
                        ShowClass.setAdapter(csAdapter);


                    }
                   class_id = response.body().getData().get(0).getClassId();

                } else {
                    Toast.makeText(managemarks.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(retrofit2.Call<ExamClassModelResponse> call, Throwable t) {

                Toast.makeText(managemarks.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void loadExamData(String teacher_id) {
        API api = RestAdapter.createAPI();
        ExamListCallBackCall = api.EXAM_LIST_RESPONSE_CALL(teacher_id);

        ExamListCallBackCall.enqueue(new Callback<ExamListResponse>() {
            @Override
            public void onResponse(retrofit2.Call<ExamListResponse> call, Response<ExamListResponse> response) {

                if (response.body().getHTTPResponseCode().equals(200)) {
                    ExamList = response.body().getData();
                    List<String> listSpinner = new ArrayList<String>();
                    listSpinner.clear();

                    for (int i = 0; i < ExamList.size(); i++){
                        listSpinner.add(ExamList.get(i).getName());
                        ArrayAdapter<String> csAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.custom_spinner_box, listSpinner);
                        csAdapter.setDropDownViewResource(R.layout.custom_dropdown_spinner_box);
                        ShowExam.setAdapter(csAdapter);


                    }
                    exam_id = response.body().getData().get(0).getExamId();

                } else {
                    Toast.makeText(managemarks.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(retrofit2.Call<ExamListResponse> call, Throwable t) {

                Toast.makeText(managemarks.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void loadExamSection(String class_id) {
        API api = RestAdapter.createAPI();
        ExamSectionCallBackCall = api.EXAM_SECTION_RESPONSE_CALL(class_id);

        ExamSectionCallBackCall.enqueue(new Callback<ExamSectionResponse>() {
            @Override
            public void onResponse(retrofit2.Call<ExamSectionResponse> call, Response<ExamSectionResponse> response) {

                if (response.body().getHTTPResponseCode().equals(200)) {
                    ExamSectionList = response.body().getData();
                    List<String> listSpinner = new ArrayList<String>();
                    listSpinner.clear();

                    for (int i = 0; i < ExamSectionList.size(); i++){
                        listSpinner.add(ExamSectionList.get(i).getName());
                        ArrayAdapter<String> csAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.custom_spinner_box, listSpinner);
                        csAdapter.setDropDownViewResource(R.layout.custom_dropdown_spinner_box);
                        ShowSection.setAdapter(csAdapter);


                    }

                } else {
                    Toast.makeText(managemarks.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(retrofit2.Call<ExamSectionResponse> call, Throwable t) {

                Toast.makeText(managemarks.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void loadExamSubject(String class_id) {
        API api = RestAdapter.createAPI();
        ExamSubjectCallBackCall = api.EXAM_SUBJECTS_RESPONSE_CALL(class_id);

        ExamSubjectCallBackCall.enqueue(new Callback<ExamSubjectsResponse>() {
            @Override
            public void onResponse(retrofit2.Call<ExamSubjectsResponse> call, Response<ExamSubjectsResponse> response) {

                if (response.body().getSuccess()) {
                    ExamSubjectList = response.body().getData();
                    List<String> listSpinner = new ArrayList<String>();
                    listSpinner.clear();

                    for (int i = 0; i < ExamSubjectList.size(); i++){
                        listSpinner.add(ExamSubjectList.get(i).getName());
                        ArrayAdapter<String> csAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.custom_spinner_box, listSpinner);
                        csAdapter.setDropDownViewResource(R.layout.custom_dropdown_spinner_box);
                        ShowSubject.setAdapter(csAdapter);


                    }

                } else {
                    Toast.makeText(managemarks.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(retrofit2.Call<ExamSubjectsResponse> call, Throwable t) {

                Toast.makeText(managemarks.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void classSpinnerOnClick(){
        ShowClass.setOnItemSelectedListener(this);
        ShowClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> obj, View view, int position, long id) {

                class_id = ExamClassList.get(position).getClassId();
                loadExamSection(class_id);
                loadExamSubject(class_id);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void ExamSpinnerOnClick(){
        ShowExam.setOnItemSelectedListener(this);
        ShowExam.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> obj, View view, int position, long id) {

                exam_id = ExamList.get(position).getExamId();



            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void ExamSubjectSpinnerOnClick(){
        ShowSubject.setOnItemSelectedListener(this);
        ShowSubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> obj, View view, int position, long id) {

                subject_id = ExamSubjectList.get(position).getSubjectId();
                LoadExamDetails(exam_id,subject_id);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private List<TeacherExamDetailsModel> LoadExamDetailsBySujectAndExam(String exam_id, String subject_id) {
        API api = RestAdapter.createAPI();
        ExamDetailCallBackCall = api.TEACHER_EXAM_DETAILS_RESPONSE_CALL(exam_id,subject_id);


        ExamDetailCallBackCall.enqueue(new Callback<TeacherExamDetailsResponse>() {
            @Override
            public void onResponse(Call<TeacherExamDetailsResponse> call, Response<TeacherExamDetailsResponse> response) {

                if (response.body().getSuccess()) {
//
                    ExamDetailsList = response.body().getData();
                    setData(response.body().getData());

                } else {
                    Toast.makeText(managemarks.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TeacherExamDetailsResponse> call, Throwable t) {

                Toast.makeText(managemarks.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });
        return ExamDetailsList;

    }

    private void setData(List<TeacherExamDetailsModel> ExamDetailsList)
    {

        ExamDetailsAdapter = new TeacherExamDetailsAdapter(ExamDetailsList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(ExamDetailsAdapter);
    }

    public void LoadExamDetails(String exam_id, String subject_id){
        ExamDetailsAdapter = new TeacherExamDetailsAdapter(LoadExamDetailsBySujectAndExam(this.exam_id, this.subject_id));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(ExamDetailsAdapter);

    }
}
