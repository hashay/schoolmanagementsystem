package com.example.developer.schoolmanagementsystem.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.developer.schoolmanagementsystem.R;

public class texammenu extends AppCompatActivity {
    public  String teacher_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_texammenu);
        teacher_id =  getIntent().getExtras().getString("teacher_id","");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_library); // check it
        setSupportActionBar(toolbar);

        // adding back functionality

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void btnmm(View v)
    {
        Intent intent = new Intent(getApplicationContext(), managemarks.class);
        intent.putExtra("teacher_id",teacher_id);
        startActivity(intent);
        //finish();
    }

    public void btnqp(View v)
    {
        Intent intent = new Intent(getApplicationContext(), questionpaper.class);
        intent.putExtra("teacher_id",teacher_id);
        startActivity(intent);
        //finish();
    }

}
