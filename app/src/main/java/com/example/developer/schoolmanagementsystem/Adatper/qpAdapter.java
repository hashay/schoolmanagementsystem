package com.example.developer.schoolmanagementsystem.Adatper;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.developer.schoolmanagementsystem.Model.qp;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 2/21/2018.
 */

public class qpAdapter extends RecyclerView.Adapter<qpAdapter.MyViewHolder> {

    private List<qp> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, classname, exam, teacher;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.qp_title);
            classname = (TextView) view.findViewById(R.id.qp_class);
            exam = (TextView) view.findViewById(R.id.qp_exam);
            teacher = (TextView) view.findViewById(R.id.qp_teacher);


        }
    }

    public qpAdapter(List<qp> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public qpAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.qp_list, parent, false);

        return new qpAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(qpAdapter.MyViewHolder holder, int position) {
        qp material = moviesList.get(position);
        holder.title.setText(material.getTitle());
        // holder.genre.setText(data.getGenre());
        // holder.year.setText(data.getYear());
        holder.classname.setText(material.getClassName());
        holder.exam.setText(material.getExamName());

        holder.teacher.setText(material.getTeacherName());


    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}
