package com.example.developer.schoolmanagementsystem.Adatper;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.developer.schoolmanagementsystem.Model.TeacherClassRoutineSubjects;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 3/12/2018.
 */

public class TeacherClassRoutineSubjectsAdapter extends RecyclerView.Adapter<TeacherClassRoutineSubjectsAdapter.MyViewHolder> {



    private List<TeacherClassRoutineSubjects> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, time;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.subject_name);
            time = (TextView) view.findViewById(R.id.class_time);

        }
    }

    public TeacherClassRoutineSubjectsAdapter(List<TeacherClassRoutineSubjects> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public TeacherClassRoutineSubjectsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.classroutine_list, parent, false);

        return new TeacherClassRoutineSubjectsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TeacherClassRoutineSubjectsAdapter.MyViewHolder holder, int position) {
        TeacherClassRoutineSubjects data = moviesList.get(position);
        holder.name.setText(data.getSubject());
        holder.time.setText(data.getCtime());


    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}
