package com.example.developer.schoolmanagementsystem.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.pattendAdapter;
import com.example.developer.schoolmanagementsystem.Model.PAttendanceModel;
import com.example.developer.schoolmanagementsystem.Model.PAttendanceResponse;
import com.example.developer.schoolmanagementsystem.Model.pattend;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class pstudentAttendance extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private List<PAttendanceModel> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private pattendAdapter mnAdapter;
    public String student_id;
    private Call<PAttendanceResponse> callbackCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pstudent_attendance);
        student_id =  getIntent().getExtras().getString("student_id","");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_subject); // check it
        setSupportActionBar(toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.pattendace_recycler_view);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        LoadAttendance();

    }
    private List<PAttendanceModel> AttendanceList(String student_id) {
        API api = RestAdapter.createAPI();
        callbackCall = api.P_ATTENDANCE_RESPONSE_CALL(student_id);


        callbackCall.enqueue(new Callback<PAttendanceResponse>() {
            @Override
            public void onResponse(Call<PAttendanceResponse> call, Response<PAttendanceResponse> response) {

                if (response.body().getHTTPResponseCode().equals(200)) {
                    movieList = response.body().getData();
                    setAttendanceList(response.body().getData());


                } else {
                    Toast.makeText(pstudentAttendance.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<PAttendanceResponse> call, Throwable t) {

                Toast.makeText(pstudentAttendance.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });
        return movieList;

    }

    private void setAttendanceList(List<PAttendanceModel> movieList)
    {

        mnAdapter = new pattendAdapter(movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);
    }

    public void LoadAttendance(){
        mnAdapter = new pattendAdapter(AttendanceList(student_id));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);

    }



    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
