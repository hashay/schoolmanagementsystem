package com.example.developer.schoolmanagementsystem.Activities;

import android.content.Intent;
import android.nfc.Tag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.classroutineAdapter;
import com.example.developer.schoolmanagementsystem.Adatper.classroutinedaysAdapter;
import com.example.developer.schoolmanagementsystem.Adatper.subjectAdapter;
import com.example.developer.schoolmanagementsystem.Model.StudentClassRoutine;
import com.example.developer.schoolmanagementsystem.Model.StudentClassRoutineDays;
import com.example.developer.schoolmanagementsystem.Model.StudentClassRoutineDaysResponse;
import com.example.developer.schoolmanagementsystem.Model.StudentClassRoutineResponse;
import com.example.developer.schoolmanagementsystem.Model.routineclass;
import com.example.developer.schoolmanagementsystem.Model.routineclassdays;
import com.example.developer.schoolmanagementsystem.Model.sbt;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class classroutine extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private Call<StudentClassRoutineDaysResponse> callbackCall;
    private Call<StudentClassRoutineResponse> CallBackCall;
    private List<StudentClassRoutineDays> movieList = new ArrayList<>();
    private List<StudentClassRoutine> routineList = new ArrayList<>();
    private RecyclerView recyclerView;
    private classroutineAdapter mnAdapter;
    public String student_id;
    public Spinner showdays;
    public String class_id, section_id,day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classroutine);
        student_id =  getIntent().getExtras().getString("student_id","");

        Login(student_id);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_subject);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.classroutine_recycler_view);
        showdays = (Spinner) findViewById(R.id.sp_crdays);
        showdays.setOnItemSelectedListener(this);
        showdays.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> obj, View view, int position, long id) {
                 day = obj.getItemAtPosition(position).toString();

//                requestDetailDosen(selectedName);
//                Intent intent = new Intent(getApplicationContext(),classroutine.class);
//                intent.putExtra("selectedName",selectedName);
//                startActivity(intent);
          //    String classId=  studentClassRoutineDays.getClassId();
                mnAdapter = new classroutineAdapter(ClassRoutineById(student_id,day));
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(mLayoutManager);

                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(mnAdapter);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private List<StudentClassRoutine> ClassRoutineById(String student_id,String day) {
        API api = RestAdapter.createAPI();
        CallBackCall = api.StudentClassRoutine(student_id,day);


        CallBackCall.enqueue(new Callback<StudentClassRoutineResponse>() {
            @Override
            public void onResponse(Call<StudentClassRoutineResponse> call, Response<StudentClassRoutineResponse> response) {

                if (response.body().getSuccess()) {
//
                    Toast.makeText(classroutine.this,"success",Toast.LENGTH_SHORT).show();
                    routineList = response.body().getData();
                    setRoutine(response.body().getData());
                    //Toast.makeText(classroutine.this, "Date 1"+routineList.get(0).getFileName(), Toast.LENGTH_SHORT).show();

                } else {
                }
            }

            @Override
            public void onFailure(Call<StudentClassRoutineResponse> call, Throwable t) {

                Toast.makeText(classroutine.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });
        return routineList;

    }

    private void setRoutine(List<StudentClassRoutine> routineList)
    {

        mnAdapter = new classroutineAdapter(routineList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);
    }

    //SPINNER

    private void Login(String student_id) {
        API api = RestAdapter.createAPI();
        callbackCall = api.StudentClassRoutineDays(student_id);

        callbackCall.enqueue(new Callback<StudentClassRoutineDaysResponse>() {
            @Override
            public void onResponse(Call<StudentClassRoutineDaysResponse> call, Response<StudentClassRoutineDaysResponse> response) {

                if (response.body().getSuccess()) {
                    List<StudentClassRoutineDays> semuadosenItems = response.body().getData();
                    List<String> listSpinner = new ArrayList<String>();
                    for (int i = 0; i < semuadosenItems.size(); i++){
                        listSpinner.add(semuadosenItems.get(i).getDayName());

                        ArrayAdapter<String> csAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.custom_spinner_box, listSpinner);

                        csAdapter.setDropDownViewResource(R.layout.custom_dropdown_spinner_box);

                        showdays.setAdapter(csAdapter);
                    }
                    class_id = response.body().getData().get(0).getClassId();
                    section_id = response.body().getData().get(0).getSectionId();

                } else {
                    Toast.makeText(classroutine.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<StudentClassRoutineDaysResponse> call, Throwable t) {

                Toast.makeText(classroutine.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

//
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
