package com.example.developer.schoolmanagementsystem.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Developer on 2/21/2018.
 */

public class qp {

    @SerializedName("question_paper_id")
    @Expose
    private String questionPaperId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("question_paper")
    @Expose
    private String questionPaper;
    @SerializedName("class_id")
    @Expose
    private String classId;
    @SerializedName("exam_id")
    @Expose
    private String examId;
    @SerializedName("teacher_id")
    @Expose
    private String teacherId;
    @SerializedName("class_name")
    @Expose
    private String className;
    @SerializedName("teacher_name")
    @Expose
    private String teacherName;
    @SerializedName("exam_name")
    @Expose
    private String examName;

    public String getQuestionPaperId() {
        return questionPaperId;
    }

    public void setQuestionPaperId(String questionPaperId) {
        this.questionPaperId = questionPaperId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getQuestionPaper() {
        return questionPaper;
    }

    public void setQuestionPaper(String questionPaper) {
        this.questionPaper = questionPaper;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

}
