package com.example.developer.schoolmanagementsystem.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.TeacherStudentSubjectAdapter;
import com.example.developer.schoolmanagementsystem.Adatper.dataAdapter;
import com.example.developer.schoolmanagementsystem.Adatper.subjectAdapter;
import com.example.developer.schoolmanagementsystem.Model.Movie;
import com.example.developer.schoolmanagementsystem.Model.SubjectDetails;
import com.example.developer.schoolmanagementsystem.Model.SubjectDetailsResponse;
import com.example.developer.schoolmanagementsystem.Model.TeacherStudentSubject;
import com.example.developer.schoolmanagementsystem.Model.TeacherStudentSubjectResponse;
import com.example.developer.schoolmanagementsystem.Model.data;
import com.example.developer.schoolmanagementsystem.Model.sbt;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class subject extends AppCompatActivity {
//  private List<SubjectDetails> movieList = new ArrayList<>();
    private List<TeacherStudentSubject> TsubjectList = new ArrayList<>();
    List<SubjectDetails> SubjectDetails = new ArrayList<>();
    private RecyclerView recyclerView;
    private subjectAdapter mnAdapter;
    private TeacherStudentSubjectAdapter TsubjectAdapter;
    private Call<TeacherStudentSubjectResponse> TeacherSubjectCallBack;
    private Call<SubjectDetailsResponse> callbackCall;

    public String student_id,class_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject);
        Intent intent = getIntent();

        // Get the extras (if there are any)
        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.containsKey("student_id")) {
                student_id =  getIntent().getExtras().getString("student_id","");
                recyclerView = (RecyclerView) findViewById(R.id.subject_recycler_view);
                LoadStudentSubject();


            } else if(extras.containsKey("class_id")){
                class_id =  getIntent().getExtras().getString("class_id","");
                recyclerView = (RecyclerView) findViewById(R.id.subject_recycler_view);
               // Toast.makeText(this, "Hello , its working now", Toast.LENGTH_SHORT).show();
                LoadTeacherSubjectsByClassId();

            }
        }


//        if(getIntent().getExtras().getBoolean("student_id",true)){
//            student_id =  getIntent().getExtras().getString("student_id","");
//            recyclerView = (RecyclerView) findViewById(R.id.subject_recycler_view);
//            LoadStudentSubject();
//        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_subject);
        setSupportActionBar(toolbar);

        // adding back functionality

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);





    }

    private void LoadStudentSubject() {
        mnAdapter = new subjectAdapter(SubjectDetailsBySId(student_id));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);

    }

    private  void  LoadTeacherSubjectsByClassId(){
        TsubjectAdapter = new TeacherStudentSubjectAdapter(TeacherSubjectList(class_id));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(TsubjectAdapter);

    }


    private List<SubjectDetails> SubjectDetailsBySId(String student_id) {
        API api = RestAdapter.createAPI();
        callbackCall = api.SubjectDetails(student_id);


        callbackCall.enqueue(new Callback<SubjectDetailsResponse>() {
            @Override
            public void onResponse(Call<SubjectDetailsResponse> call, Response<SubjectDetailsResponse> response) {

                if (response.body().getSuccess()) {
//

                    SubjectDetails = response.body().getData();
                    setSubjectListbySId(response.body().getData());


                } else {
                    Toast.makeText(subject.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<SubjectDetailsResponse> call, Throwable t) {

                Toast.makeText(subject.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });
        return SubjectDetails;

    }

    private void setSubjectListbySId(List<SubjectDetails> SubjectList)
    {

        mnAdapter = new subjectAdapter(SubjectList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);
    }

    private List<TeacherStudentSubject> TeacherSubjectList(String class_id) {
        API api = RestAdapter.createAPI();
        TeacherSubjectCallBack = api.TEACHER_STUDENT_SUBJECT_RESPONSE_CALL(class_id);


        TeacherSubjectCallBack.enqueue(new Callback<TeacherStudentSubjectResponse>() {
            @Override
            public void onResponse(Call<TeacherStudentSubjectResponse> call, Response<TeacherStudentSubjectResponse> response) {

                if (response.body().getSuccess()) {
//

                    TsubjectList = response.body().getData();
                    setSubjectListByTid(response.body().getData());

                } else {
                    Toast.makeText(subject.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<TeacherStudentSubjectResponse> call, Throwable t) {

                Toast.makeText(subject.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });
        return TsubjectList;

    }
    private void setSubjectListByTid(List<TeacherStudentSubject> TsubjectList)
    {

        TsubjectAdapter = new TeacherStudentSubjectAdapter(TsubjectList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(TsubjectAdapter);
    }




    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


}
