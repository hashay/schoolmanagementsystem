package com.example.developer.schoolmanagementsystem.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Model.TDetails;
import com.example.developer.schoolmanagementsystem.Model.TDetailsResponse;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class details extends AppCompatActivity {
    public String teacher_id;
    private Call<TDetailsResponse> callbackCall;
    List<TDetails> teacherDetails = new ArrayList<>();
    public TextView name, description, fathername, age, dateofbirth, sex,  nationality , school_name ,phone_number, email , address;
    CircleImageView ProfilePicture;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_details);
        setSupportActionBar(toolbar);
        teacher_id =  getIntent().getExtras().getString("teacher_id","");
        // adding back functionality
        name = (TextView) findViewById(R.id.td_name);
        dateofbirth = (TextView) findViewById(R.id.td_dob);
        sex = (TextView) findViewById(R.id.td_gender);
        phone_number = (TextView) findViewById(R.id.td_phone);
        email = (TextView) findViewById(R.id.td_email);
        address = (TextView) findViewById(R.id.td_address);
        ProfilePicture = (CircleImageView) findViewById(R.id.teacher_profile);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Login(teacher_id);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);



    }


    private void Login(String teacher_id) {
        API api = RestAdapter.createAPI();
        callbackCall = api.teacherDetails(teacher_id);

        callbackCall.enqueue(new Callback<TDetailsResponse>() {
            @Override
            public void onResponse(Call<TDetailsResponse> call, Response<TDetailsResponse> response) {

                if (response.body().getSuccess()) {


        //            name.setText(response.body().getData().get(0).getName().toString());
                name.setText(response.body().getData().getName());
                dateofbirth.setText(response.body().getData().getBirthday());
                sex.setText(response.body().getData().getSex());
                phone_number.setText(response.body().getData().getPhone());
                email.setText(response.body().getData().getEmail());
                address.setText(response.body().getData().getAddress());
                    Picasso.get().load(response.body().getData().getImageUrl()).resize(80,80).centerCrop().into(ProfilePicture);

                } else {
                    Toast.makeText(details.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<TDetailsResponse> call, Throwable t) {

                Toast.makeText(details.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }




}
