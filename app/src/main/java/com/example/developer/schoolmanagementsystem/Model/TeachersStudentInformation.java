package com.example.developer.schoolmanagementsystem.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Developer on 3/8/2018.
 */

public class TeachersStudentInformation {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("parent_name")
    @Expose
    private String parentName;
    @SerializedName("parent_email")
    @Expose
    private String parentEmail;
    @SerializedName("parent_address")
    @Expose
    private String parentAddress;
    @SerializedName("profession")
    @Expose
    private String profession;
    @SerializedName("dormitory_namem")
    @Expose
    private String dormitoryNamem;
    @SerializedName("route_name")
    @Expose
    private String routeName;
    @SerializedName("number_of_vehicle")
    @Expose
    private String numberOfVehicle;
    @SerializedName("route_fare")
    @Expose
    private String routeFare;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getParentEmail() {
        return parentEmail;
    }

    public void setParentEmail(String parentEmail) {
        this.parentEmail = parentEmail;
    }

    public String getParentAddress() {
        return parentAddress;
    }

    public void setParentAddress(String parentAddress) {
        this.parentAddress = parentAddress;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getDormitoryNamem() {
        return dormitoryNamem;
    }

    public void setDormitoryNamem(String dormitoryNamem) {
        this.dormitoryNamem = dormitoryNamem;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getNumberOfVehicle() {
        return numberOfVehicle;
    }

    public void setNumberOfVehicle(String numberOfVehicle) {
        this.numberOfVehicle = numberOfVehicle;
    }

    public String getRouteFare() {
        return routeFare;
    }

    public void setRouteFare(String routeFare) {
        this.routeFare = routeFare;
    }


}
