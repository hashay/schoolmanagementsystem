package com.example.developer.schoolmanagementsystem.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Developer on 3/1/2018.
 */

public class StudentMarksheetResponse {

    @SerializedName("data")
    @Expose
    private List<StudentMarksheet> data = null;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("class_id")
    @Expose
    private String classId;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("total_marks")
    @Expose
    private Integer totalMarks;
    @SerializedName("mark_url")
    @Expose
    private String markUrl;
    @SerializedName("avg_grade_point")
    @Expose
    private Integer avgGradePoint;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("HTTP_response_code")
    @Expose
    private Integer hTTPResponseCode;

    public List<StudentMarksheet> getData() {
        return data;
    }

    public void setData(List<StudentMarksheet> data) {
        this.data = data;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(Integer totalMarks) {
        this.totalMarks = totalMarks;
    }

    public String getMarkUrl() {
        return markUrl;
    }

    public void setMarkUrl(String markUrl) {
        this.markUrl = markUrl;
    }

    public Integer getAvgGradePoint() {
        return avgGradePoint;
    }

    public void setAvgGradePoint(Integer avgGradePoint) {
        this.avgGradePoint = avgGradePoint;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getHTTPResponseCode() {
        return hTTPResponseCode;
    }

    public void setHTTPResponseCode(Integer hTTPResponseCode) {
        this.hTTPResponseCode = hTTPResponseCode;
    }
}
