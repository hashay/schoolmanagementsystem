package com.example.developer.schoolmanagementsystem.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Developer on 3/5/2018.
 */

public class BookListResponse {

    @SerializedName("data")
    @Expose
    private List<BookList> data = null;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("HTTP_response_code")
    @Expose
    private Integer hTTPResponseCode;

    public List<BookList> getData() {
        return data;
    }

    public void setData(List<BookList> data) {
        this.data = data;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getHTTPResponseCode() {
        return hTTPResponseCode;
    }

    public void setHTTPResponseCode(Integer hTTPResponseCode) {
        this.hTTPResponseCode = hTTPResponseCode;
    }


}
