package com.example.developer.schoolmanagementsystem.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.classstudentsAdapter;
import com.example.developer.schoolmanagementsystem.Model.TeacherClassStudentRecord;
import com.example.developer.schoolmanagementsystem.Model.TeacherClassStudentRecordResponse;
import com.example.developer.schoolmanagementsystem.Model.TeacherClassStudentsSection;
import com.example.developer.schoolmanagementsystem.Model.TeacherClassStudentsSectionResponse;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class classstudents extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
//spinner
private Call<TeacherClassStudentsSectionResponse> callbackCall;
private Call<TeacherClassStudentRecordResponse> CallBackCall;

    public Spinner showdays;
    List<TeacherClassStudentsSection> semuadosenItems;
//list
    private List<TeacherClassStudentRecord> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private classstudentsAdapter mnAdapter;


    public long day;
    public  String teacher_id,class_id,section_id;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classstudents);
        teacher_id =  getIntent().getExtras().getString("teacher_id","");
        class_id = getIntent().getExtras().getString("class_id","");
        Login(teacher_id,class_id);
        Toolbar toolbar = (Toolbar) findViewById(R.id.studymaterial_toolbar);
        setSupportActionBar(toolbar);



        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        showdays = (Spinner) findViewById(R.id.sp_class_section);

        //showdays.setOnItemSelectedListener(this);

        recyclerView = (RecyclerView) findViewById(R.id.classstudents_recycler_view);

        //spinner

//        recyclerView2 = (RecyclerView) findViewById(R.id.classstudents_section_recycler_view);



        showdays.setOnItemSelectedListener(this);
        showdays.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> obj, View view, int position, long id) {
                day = obj.getItemIdAtPosition(position);
                section_id = semuadosenItems.get(position).getSectionId();
                mnAdapter = new classstudentsAdapter(ClassRoutineById(class_id,section_id),classstudents.this);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });





    }

    private List<TeacherClassStudentRecord> ClassRoutineById(String class_id,String section_id) {
        API api = RestAdapter.createAPI();
        CallBackCall = api.TEACHER_CLASS_STUDENT_RECORD_RESPONSE_CALL(class_id,section_id);


        CallBackCall.enqueue(new Callback<TeacherClassStudentRecordResponse>() {
            @Override
            public void onResponse(Call<TeacherClassStudentRecordResponse> call, Response<TeacherClassStudentRecordResponse> response) {

                if (response.body().getSuccess()) {
//
                    movieList = response.body().getData();
                    setRoutine(response.body().getData());
                    //Toast.makeText(classroutine.this, "Date 1"+routineList.get(0).getFileName(), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(classstudents.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TeacherClassStudentRecordResponse> call, Throwable t) {

                Toast.makeText(classstudents.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });
        return movieList;

    }

    private void setRoutine(List<TeacherClassStudentRecord> movieList)
    {

        mnAdapter = new classstudentsAdapter(movieList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }






    private void Login(String teacher_id,String class_id) {
        API api = RestAdapter.createAPI();
        callbackCall = api.TEACHER_CLASS_STUDENTS_SECTION_RESPONSE_CALL(teacher_id,class_id);

        callbackCall.enqueue(new Callback<TeacherClassStudentsSectionResponse>() {
            @Override
            public void onResponse(Call<TeacherClassStudentsSectionResponse> call, Response<TeacherClassStudentsSectionResponse> response) {

                if (response.body().getSuccess()) {
                    semuadosenItems = response.body().getData();
                    List<String> listSpinner = new ArrayList<String>();

                    for (int i = 0; i < semuadosenItems.size(); i++){
                        listSpinner.add(semuadosenItems.get(i).getClassName() + " " + semuadosenItems.get(i).getName());
                        ArrayAdapter<String> csAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.custom_spinner_box, listSpinner);
                        csAdapter.setDropDownViewResource(R.layout.custom_dropdown_spinner_box);
                        showdays.setAdapter(csAdapter);


                    }
                   section_id = response.body().getData().get(0).getSectionId();
                    Toast.makeText(classstudents.this,section_id,Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(classstudents.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<TeacherClassStudentsSectionResponse> call, Throwable t) {

                Toast.makeText(classstudents.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });

    }



    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }



}
