package com.example.developer.schoolmanagementsystem.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.qpAdapter;
import com.example.developer.schoolmanagementsystem.Model.qp;
import com.example.developer.schoolmanagementsystem.Model.qpResponse;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class questionpaper extends AppCompatActivity {
    String teacher_id;
    private Call<qpResponse> QpCallBackCall;
    private List<qp> QpList = new ArrayList<>();
    private RecyclerView recyclerView;
    private qpAdapter QPAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionpaper);
        teacher_id =  getIntent().getExtras().getString("teacher_id","");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_library); // check it
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        recyclerView = (RecyclerView) findViewById(R.id.qp_recycler_view);
        LoadQpDetails();


//        mnAdapter = new qpAdapter(movieList);
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
//        recyclerView.setLayoutManager(mLayoutManager);
//
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        recyclerView.setAdapter(mnAdapter);




    }

            @Override
            public boolean onSupportNavigateUp() {
                onBackPressed();
                return true;
            }

    private List<qp> QpListByTeacherId(String teacher_id) {
        API api = RestAdapter.createAPI();
        QpCallBackCall = api.QP_RESPONSE_CALL(teacher_id);


        QpCallBackCall.enqueue(new Callback<qpResponse>() {
            @Override
            public void onResponse(Call<qpResponse> call, Response<qpResponse> response) {

                if (response.body().getSuccess()) {
//

                    QpList = response.body().getData();
                    setTeacherList(response.body().getData());

                } else {
                    Toast.makeText(questionpaper.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<qpResponse> call, Throwable t) {
                Log.e("Error","--"+t.getMessage());

                Toast.makeText(questionpaper.this, "incorrect"+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return QpList;

    }

    private void setTeacherList(List<qp> QpList)
    {

        QPAdapter = new qpAdapter(QpList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(QPAdapter);
    }

    public void LoadQpDetails(){
        QPAdapter = new qpAdapter(QpListByTeacherId(teacher_id));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(QPAdapter);

    }

}
