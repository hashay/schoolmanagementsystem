package com.example.developer.schoolmanagementsystem.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.libreqAdapter;
import com.example.developer.schoolmanagementsystem.Adatper.transportAdapter;
import com.example.developer.schoolmanagementsystem.Model.Transport;
import com.example.developer.schoolmanagementsystem.Model.TransportResponse;
import com.example.developer.schoolmanagementsystem.Model.libreq;
import com.example.developer.schoolmanagementsystem.Model.trans;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class transport extends AppCompatActivity {
    private Call<TransportResponse> callbackCall;
    private List<Transport> transportList = new ArrayList<>();
    private RecyclerView recyclerView;
    private transportAdapter mnAdapter;
    public  String student_id;
    public KProgressHUD hud;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transport);
        if(getIntent().getStringExtra("student_id") != null){
            student_id =  getIntent().getExtras().getString("student_id","");

        }else if (getIntent().getStringExtra("teacher_id") != null){
            student_id = getIntent().getExtras().getString("teacher_id","");
        }else if (getIntent().getStringExtra("parent_id") != null) {
            student_id = getIntent().getExtras().getString("parent_id", "");
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.studymaterial_toolbar); // check it
        setSupportActionBar(toolbar);
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setDetailsLabel("Downloading data")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        hud.show();

        // adding back functionality

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.transport_recycler_view);

        mnAdapter = new transportAdapter(transportListByStudentId(student_id));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);





    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    private List<Transport> transportListByStudentId(String student_id) {
        API api = RestAdapter.createAPI();
        callbackCall = api.TransportResponse(student_id);


        callbackCall.enqueue(new Callback<TransportResponse>() {
            @Override
            public void onResponse(Call<TransportResponse> call, Response<TransportResponse> response) {

                if (response.body().getSuccess()) {
                    hud.dismiss();

                    transportList = response.body().getData();
                    SetTransportList(response.body().getData());
                  //  Toast.makeText(transport.this, "Date 1"+movieList.get(0).getTitle(), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(transport.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<TransportResponse> call, Throwable t) {

                Toast.makeText(transport.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });
        return transportList;

    }

    private void SetTransportList(List<Transport> transportList)
    {

        mnAdapter = new transportAdapter(transportList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);
    }



}
