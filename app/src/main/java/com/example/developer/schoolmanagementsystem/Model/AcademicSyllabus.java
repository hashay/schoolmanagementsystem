package com.example.developer.schoolmanagementsystem.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Developer on 2/27/2018.
 */

public class AcademicSyllabus {


    @SerializedName("academic_syllabus_id")
    @Expose
    private String academicSyllabusId;
    @SerializedName("academic_syllabus_code")
    @Expose
    private String academicSyllabusCode;
    @SerializedName("name")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("class_id")
    @Expose
    private String classId;
    @SerializedName("uploader_type")
    @Expose
    private String uploaderType;
    @SerializedName("uploader_id")
    @Expose
    private String uploaderId;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("file_name")
    @Expose
    private String fileName;
    @SerializedName("subject_id")
    @Expose
    private String subjectId;
    @SerializedName("subject_name")
    @Expose
    private String subjectName;
    @SerializedName("file_url")
    @Expose
    private String fileUrl;

    public String getAcademicSyllabusId() {
        return academicSyllabusId;
    }

    public void setAcademicSyllabusId(String academicSyllabusId) {
        this.academicSyllabusId = academicSyllabusId;
    }

    public String getAcademicSyllabusCode() {
        return academicSyllabusCode;
    }

    public void setAcademicSyllabusCode(String academicSyllabusCode) {
        this.academicSyllabusCode = academicSyllabusCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getUploaderType() {
        return uploaderType;
    }

    public void setUploaderType(String uploaderType) {
        this.uploaderType = uploaderType;
    }

    public String getUploaderId() {
        return uploaderId;
    }

    public void setUploaderId(String uploaderId) {
        this.uploaderId = uploaderId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }


}
