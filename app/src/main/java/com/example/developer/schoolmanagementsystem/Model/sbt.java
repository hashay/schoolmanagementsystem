package com.example.developer.schoolmanagementsystem.Model;

/**
 * Created by Developer on 2/16/2018.
 */

public class sbt {

    private String title, genre, year;




    public sbt(String title, String genre, String year) {
        this.title = title;
        this.genre = genre;
        this.year = year;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String name) {
        this.genre = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String name) {
        this.year = name;
    }




}
