package com.example.developer.schoolmanagementsystem.Adatper;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Activities.TeacherSyllabus;
import com.example.developer.schoolmanagementsystem.Model.TeacherAcademicSyllabus;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 3/12/2018.
 */

public class TeacherAcademicSyllabusAdapter extends RecyclerView.Adapter<TeacherAcademicSyllabusAdapter.MyViewHolder> {

    private List<TeacherAcademicSyllabus> moviesList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, date, description, subjectname, uploader;
        public Button download;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.syllabus_title);
            date = (TextView) view.findViewById(R.id.syllabus_date);
            description = (TextView) view.findViewById(R.id.syllabus_description);
            uploader = (TextView) view.findViewById(R.id.syllabus_uploader);
            subjectname = (TextView) view.findViewById(R.id.syllabus_subject);
            download = (Button) view.findViewById(R.id.syllabus_download);

        }
    }

    public TeacherAcademicSyllabusAdapter(List<TeacherAcademicSyllabus> moviesList, Context context) {
        this.moviesList = moviesList;
        this.context = context;
    }

    @Override
    public TeacherAcademicSyllabusAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.syllabus_list, parent, false);

        return new TeacherAcademicSyllabusAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(TeacherAcademicSyllabusAdapter.MyViewHolder holder, int position) {
        final TeacherAcademicSyllabus material = moviesList.get(position);
        holder.title.setText(material.getTitle());
        // holder.genre.setText(data.getGenre());
        // holder.year.setText(data.getYear());
        holder.date.setText(material.getYear());
        holder.description.setText(material.getDescription());

        holder.subjectname.setText(material.getSubjectName());
        holder.uploader.setText(material.getUploaderType());
        holder.download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,material.getFileUrl().toString(), Toast.LENGTH_SHORT).show();

                //    String url = material.getFileUrl().toString();
                String url = material.getFileUrl().toString();
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                request.setDescription(material.getDescription().toString());
                request.setTitle(material.getFileName().toString());
// in order for this if to run, you must use the android 3.2 to compile your app
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    request.allowScanningByMediaScanner();
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                }
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, material.getFileName().toString());

// get download service and enqueue file
                DownloadManager manager = (DownloadManager)context.getSystemService(Context.DOWNLOAD_SERVICE);
                manager.enqueue(request);

                //  startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("www.education.gov.yk.ca/pdf/pdf-test.pdf")));

            }
        });

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
