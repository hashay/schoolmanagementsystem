package com.example.developer.schoolmanagementsystem.Model;

/**
 * Created by Developer on 2/19/2018.
 */

public class syllabus {

    private String title, date, description, subject, uploader;




    public syllabus(String title, String date, String description, String subject, String uploader) {
        this.title = title;
        this.date = date;
        this.description = description;
        this.subject = subject;
        this.uploader = uploader;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String name) {
        this.date = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String name) {
        this.description = name;
    }



    public String getSubject() {
        return subject;
    }

    public void setSubject(String name) {
        this.subject = name;
    }


    public String getUploader() {
        return uploader;
    }

    public void setUploader(String name) {
        this.uploader = name;
    }

}
