package com.example.developer.schoolmanagementsystem.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Developer on 3/19/2018.
 */

public class TeacherExamDetailsModel {

    @SerializedName("exam_id")
    @Expose
    private String examId;
    @SerializedName("student_id")
    @Expose
    private String studentId;
    @SerializedName("student_name")
    @Expose
    private String studentName;
    @SerializedName("mark_obtained")
    @Expose
    private String markObtained;
    @SerializedName("mark_total")
    @Expose
    private Object markTotal;

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getMarkObtained() {
        return markObtained;
    }

    public void setMarkObtained(String markObtained) {
        this.markObtained = markObtained;
    }

    public Object getMarkTotal() {
        return markTotal;
    }

    public void setMarkTotal(Object markTotal) {
        this.markTotal = markTotal;
    }


}
