package com.example.developer.schoolmanagementsystem.Adatper;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.developer.schoolmanagementsystem.Model.Transport;
import com.example.developer.schoolmanagementsystem.Model.libreq;
import com.example.developer.schoolmanagementsystem.Model.trans;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 2/19/2018.
 */

public class transportAdapter extends RecyclerView.Adapter<transportAdapter.MyViewHolder>{

    private List<Transport> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView route, vehiclenumber , description , fare;

        public MyViewHolder(View view) {
            super(view);
            route = (TextView) view.findViewById(R.id.transport_route);
            vehiclenumber    = (TextView) view.findViewById(R.id.transport_vnumber);
            description = (TextView) view.findViewById(R.id.transport_description);
            fare = (TextView) view.findViewById(R.id.transport_routefare);


        }
    }

    public transportAdapter(List<Transport> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public transportAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transport_list, parent, false);

        return new transportAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(transportAdapter.MyViewHolder holder, int position) {
        Transport material = moviesList.get(position);
        holder.route.setText(material.getRouteName());
        // holder.genre.setText(data.getGenre());
        // holder.year.setText(data.getYear());
        holder.vehiclenumber.setText(material.getNumberOfVehicle());
        holder.description.setText(material.getDescription());
        holder.fare.setText(material.getRouteFare());




    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}
