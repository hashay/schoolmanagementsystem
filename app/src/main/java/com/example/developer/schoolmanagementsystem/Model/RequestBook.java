package com.example.developer.schoolmanagementsystem.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Developer on 3/5/2018.
 */

public class RequestBook {

    @SerializedName("book_request_id")
    @Expose
    private String bookRequestId;
    @SerializedName("student_id")
    @Expose
    private String studentId;
    @SerializedName("book_name")
    @Expose
    private String bookName;
    @SerializedName("book_id")
    @Expose
    private String bookId;
    @SerializedName("student_name")
    @Expose
    private String studentName;
    @SerializedName("issue_start_date")
    @Expose
    private String issueStartDate;
    @SerializedName("issue_end_date")
    @Expose
    private String issueEndDate;
    @SerializedName("status")
    @Expose
    private String status;

    public String getBookRequestId() {
        return bookRequestId;
    }

    public void setBookRequestId(String bookRequestId) {
        this.bookRequestId = bookRequestId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getIssueStartDate() {
        return issueStartDate;
    }

    public void setIssueStartDate(String issueStartDate) {
        this.issueStartDate = issueStartDate;
    }

    public String getIssueEndDate() {
        return issueEndDate;
    }

    public void setIssueEndDate(String issueEndDate) {
        this.issueEndDate = issueEndDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }



}
