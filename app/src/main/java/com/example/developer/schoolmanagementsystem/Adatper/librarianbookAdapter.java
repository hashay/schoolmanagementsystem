package com.example.developer.schoolmanagementsystem.Adatper;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.developer.schoolmanagementsystem.Model.librarianbook;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 2/21/2018.
 */

public class librarianbookAdapter extends RecyclerView.Adapter<librarianbookAdapter.MyViewHolder>{

    private List<librarianbook> moviesList;




    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView book, author , description , price , classname, issued , total;


        public MyViewHolder(View view) {
            super(view);
            book = (TextView) view.findViewById(R.id.booklist_title);
            author    = (TextView) view.findViewById(R.id.booklist_author);
            description = (TextView) view.findViewById(R.id.booklist_description);
            price = (TextView) view.findViewById(R.id.booklidt_price);
            classname = (TextView) view.findViewById(R.id.booklist_class);
            issued = (TextView) view.findViewById(R.id.booklist_ic);
            total = (TextView) view.findViewById(R.id.booklist_tc);




        }
    }

    public librarianbookAdapter(List<librarianbook> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public librarianbookAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lb_book_list, parent, false);

        return new librarianbookAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(librarianbookAdapter.MyViewHolder holder, int position) {
        final librarianbook material = moviesList.get(position);
        holder.book.setText(material.getName());
        // holder.genre.setText(data.getGenre());
        // holder.year.setText(data.getYear());
        holder.author.setText(material.getAuthor());
        holder.description.setText(material.getDescription());
        holder.price.setText(material.getPrice());
        holder.classname.setText(material.getClassName());
        try {
            holder.issued.setText((Integer) material.getIssuedCopies());
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            holder.total.setText((Integer) material.getTotalCopies());
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}
