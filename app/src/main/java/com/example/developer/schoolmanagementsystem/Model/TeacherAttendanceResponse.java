package com.example.developer.schoolmanagementsystem.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Developer on 3/13/2018.
 */

public class TeacherAttendanceResponse {

    @SerializedName("data")
    @Expose
    private List<TeacherAttendance> data = null;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("HTTP_response_code")
    @Expose
    private Integer hTTPResponseCode;

    public List<TeacherAttendance> getData() {
        return data;
    }

    public void setData(List<TeacherAttendance> data) {
        this.data = data;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getHTTPResponseCode() {
        return hTTPResponseCode;
    }

    public void setHTTPResponseCode(Integer hTTPResponseCode) {
        this.hTTPResponseCode = hTTPResponseCode;
    }
}
