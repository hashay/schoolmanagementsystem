package com.example.developer.schoolmanagementsystem.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.TeacherAcademicSyllabusAdapter;
import com.example.developer.schoolmanagementsystem.Model.TeacherAcademicSyllabus;
import com.example.developer.schoolmanagementsystem.Model.TeacherAcademicSyllabusResponse;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeacherSyllabus extends AppCompatActivity {

    public String class_id;
    private List<TeacherAcademicSyllabus> syllabusList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TeacherAcademicSyllabusAdapter syllabusAdapter;
    private Call<TeacherAcademicSyllabusResponse> TeacherSyllabusCallBackCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_academicsyllabus);
        class_id =  getIntent().getExtras().getString("class_id","");

        Toolbar toolbar = (Toolbar) findViewById(R.id.studymaterial_toolbar); // check it


        recyclerView = (RecyclerView) findViewById(R.id.syllabus_recycler_view);

        LoadSyllabus();



    }
    public void LoadSyllabus(){
        syllabusAdapter = new TeacherAcademicSyllabusAdapter(TeacherSyllabusByClassId(class_id), this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(syllabusAdapter);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private List<TeacherAcademicSyllabus> TeacherSyllabusByClassId(String class_id) {
        API api = RestAdapter.createAPI();
        TeacherSyllabusCallBackCall = api.TEACHER_ACADEMIC_SYLLABUS_RESPONSE_CALL(class_id);


        TeacherSyllabusCallBackCall.enqueue(new Callback<TeacherAcademicSyllabusResponse>() {
            @Override
            public void onResponse(Call<TeacherAcademicSyllabusResponse> call, Response<TeacherAcademicSyllabusResponse> response) {

                if (response.body().getSuccess()) {
//

                    syllabusList = response.body().getData();
                    setSyllabusList(response.body().getData());
//                    Toast.makeText(TeacherSyllabus.this, "Date 1"+syllabusList.get(0).getFileName(), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(TeacherSyllabus.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TeacherAcademicSyllabusResponse> call, Throwable t) {

                Toast.makeText(TeacherSyllabus.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });
        return syllabusList;

    }

    private void setSyllabusList(List<TeacherAcademicSyllabus> teacherList)
    {

        syllabusAdapter = new TeacherAcademicSyllabusAdapter(syllabusList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(syllabusAdapter);
    }



}
