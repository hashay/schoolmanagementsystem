package com.example.developer.schoolmanagementsystem.Adatper;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.developer.schoolmanagementsystem.Model.Invoice;
import com.example.developer.schoolmanagementsystem.Model.libreq;
import com.example.developer.schoolmanagementsystem.Model.pay;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 2/19/2018.
 */

public class paymentAdapter extends RecyclerView.Adapter<paymentAdapter.MyViewHolder>{

    private List<Invoice> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, description , amount , date , status;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.payment_title);
            description    = (TextView) view.findViewById(R.id.payment_description);
            amount = (TextView) view.findViewById(R.id.payment_amount);
            date = (TextView) view.findViewById(R.id.payment_date);
            status = (TextView) view.findViewById(R.id.payment_status);

        }
    }

    public paymentAdapter(List<Invoice> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public paymentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.payment_list, parent, false);

        return new paymentAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(paymentAdapter.MyViewHolder holder, int position) {
        Invoice material = moviesList.get(position);
        holder.title.setText(material.getTitle());
        // holder.genre.setText(data.getGenre());
        // holder.year.setText(data.getYear());
        holder.description.setText(material.getDescription());
        holder.amount.setText(material.getAmountPaid());
        holder.date.setText(String.valueOf(material.getPaymentTimestamp()));

        holder.status.setText(material.getStatus());
       if(material.getStatus().equals("paid")){
           holder.status.setTextColor(Color.parseColor("#ffffff"));
           holder.status.setBackgroundResource(R.drawable.paidbtn);
       }else{
           holder.status.setTextColor(Color.parseColor("#ffffff"));
           holder.status.setBackgroundResource(R.drawable.unpaidbtn);
       }





    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}
