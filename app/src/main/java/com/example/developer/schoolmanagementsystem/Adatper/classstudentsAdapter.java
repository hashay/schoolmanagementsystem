package com.example.developer.schoolmanagementsystem.Adatper;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.developer.schoolmanagementsystem.Activities.TeacherStudentProfileExams;
import com.example.developer.schoolmanagementsystem.Activities.exammarks;
import com.example.developer.schoolmanagementsystem.Activities.studentprofile;
import com.example.developer.schoolmanagementsystem.Model.TeacherClassStudentRecord;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Developer on 2/20/2018.
 */

public class classstudentsAdapter extends RecyclerView.Adapter<classstudentsAdapter.MyViewHolder> {
    private List<TeacherClassStudentRecord> moviesList;
    Context context;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView sname,rollnumber, email, address;
        public CircleImageView icon;
        private Button btnprofile, btnmarksheet;

        public MyViewHolder(View view) {
            super(view);
            rollnumber = (TextView) view.findViewById(R.id.classstudents_rollnumber);
            email = (TextView) view.findViewById(R.id.classstudents_email);
            address = (TextView) view.findViewById(R.id.classstudents_address);
            sname = (TextView) view.findViewById(R.id.classstudents_name);
            icon = (CircleImageView) view.findViewById(R.id.classstudents_image);
            itemView.setTag(itemView);
            btnprofile = (Button) view.findViewById(R.id.btnprofile);
            btnmarksheet = (Button) view.findViewById(R.id.btnmarksheet);



        }
    }

    public classstudentsAdapter(List<TeacherClassStudentRecord> moviesList, Context context) {
        this.moviesList = moviesList;
        this.context = context;

    }

    @Override
    public classstudentsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.classstudents_list, parent, false);

        return new classstudentsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(classstudentsAdapter.MyViewHolder holder, int position) {
        final TeacherClassStudentRecord data = moviesList.get(position);
        holder.sname.setText(data.getStudentName());
        holder.rollnumber.setText(data.getRoll());
        holder.email.setText(data.getEmail());
       holder.address.setText(data.getAddress());
       holder.btnprofile.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               String student_id = data.getStudentId();
               context.startActivity(new Intent(context,studentprofile.class).putExtra("student_id",student_id));

           }
       });
        holder.btnmarksheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String student_id = data.getStudentId();
                context.startActivity(new Intent(context,TeacherStudentProfileExams.class).putExtra("student_id",student_id));
            }
        });
      // holder.icon.setImageResource(Integer.parseInt(data.getImg()));

      //Glide.with(context).load(moviesList.get(position).getImg()).crossFade().diskCacheStrategy(DiskCacheStrategy.NONE).into(holder.icon);
    }






    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}
