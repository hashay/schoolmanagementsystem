package com.example.developer.schoolmanagementsystem.Activities;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.TeacherStudentInformationExamAdapter;
import com.example.developer.schoolmanagementsystem.Adatper.TeacherStudentInformationPaymentAdapter;
import com.example.developer.schoolmanagementsystem.Adatper.exammarksAdapter;
import com.example.developer.schoolmanagementsystem.Adatper.paymentAdapter;
import com.example.developer.schoolmanagementsystem.Model.TeacherStudentInformationResponse;
import com.example.developer.schoolmanagementsystem.Model.TeacherStudentProfileExam;
import com.example.developer.schoolmanagementsystem.Model.TeacherStudentProfilePayment;
import com.example.developer.schoolmanagementsystem.Model.marks;
import com.example.developer.schoolmanagementsystem.Model.pay;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class studentprofile extends AppCompatActivity {
    //exam initialization
    private Call<TeacherStudentInformationResponse> CallBackCall;
    private List<TeacherStudentProfileExam> marksList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TeacherStudentInformationExamAdapter ExamAdapter;
    public TextView totalMarks, AverageGradePoints;
    public Button marksheet;
    //student profile initialization
    private Call<TeacherStudentInformationResponse> callbackCall;
    public String student_id;


    public TextView student_name, parent_name,student_class,student_section,student_email,student_phone,student_address,
            student_gender, student_birthday,student_dorm,student_transport;
    public TextView parent_email,parent_phone,parent_address,parent_profession;

    //student payment initialization]

    private Call<TeacherStudentInformationResponse> CallBackCallPayment;
    private List<TeacherStudentProfilePayment> paymentList = new ArrayList<>();
    public RecyclerView PaymentRv;
    private TeacherStudentInformationPaymentAdapter PaymentAdapter;
    public KProgressHUD hud;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studentprofile);
        student_id =  getIntent().getExtras().getString("student_id","");
        Toolbar toolbar = (Toolbar) findViewById(R.id.studymaterial_toolbar); // check it
        setSupportActionBar(toolbar);

        // adding back functionality

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        recyclerView = (RecyclerView) findViewById(R.id.exammarks_recycler_view);
        PaymentRv = (RecyclerView) findViewById(R.id.payment_recycler_view);


        initTvs();
        DisplayStudentData(student_id);
        LoadStudentExamMarksRecyclerView();
        LoadStudentInvoicesRecyclerView();
        getTotal(student_id);




    }

    private void LoadStudentExamMarksRecyclerView() {

        ExamAdapter = new TeacherStudentInformationExamAdapter(LoadStudentMarks(student_id));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(ExamAdapter);

    }

    private void LoadStudentInvoicesRecyclerView() {

        PaymentAdapter = new TeacherStudentInformationPaymentAdapter(StudentPayment(student_id));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        PaymentRv.setLayoutManager(mLayoutManager);

        PaymentRv.setItemAnimator(new DefaultItemAnimator());
        PaymentRv.setAdapter(PaymentAdapter);

    }



    private void initTvs() {
        student_name = (TextView) findViewById(R.id.csbasic_studentname);
        parent_name = (TextView) findViewById(R.id.csbasic_parent);
        student_class = (TextView) findViewById(R.id.csbasic_class);
        student_section = (TextView) findViewById(R.id.csbasic_section);
        student_email = (TextView) findViewById(R.id.csbasic_email);
        student_phone = (TextView) findViewById(R.id.csbasic_phone);
        student_address = (TextView) findViewById(R.id.csbasic_address);
        student_birthday = (TextView) findViewById(R.id.csbasic_bday);
        student_dorm = (TextView) findViewById(R.id.csbasic_dorm);
        student_transport = (TextView) findViewById(R.id.csbasic_transport);

        parent_name = (TextView) findViewById(R.id.csparent_name);
        parent_address = (TextView) findViewById(R.id.csparent_address);
        parent_email = (TextView) findViewById(R.id.csparent_email);
        parent_profession = (TextView) findViewById(R.id.csparent_profession);

        totalMarks = (TextView) findViewById(R.id.exam_total);
        AverageGradePoints = (TextView) findViewById(R.id.exam_grade);


    }

    private void DisplayStudentData(String student_id) {
        API api = RestAdapter.createAPI();
        callbackCall = api.TEACHER_STUDENT_INFORMATION_RESPONSE_CALL(student_id);

        callbackCall.enqueue(new Callback<TeacherStudentInformationResponse>() {
            @Override
            public void onResponse(Call<TeacherStudentInformationResponse> call, Response<TeacherStudentInformationResponse> response) {

                if (response.body().getSuccess()) {




                    student_name.setText(response.body().getData().get(0).getName());
                    parent_name.setText(response.body().getData().get(0).getParentName());

                    student_email.setText(response.body().getData().get(0).getEmail());
                    student_phone.setText(response.body().getData().get(0).getPhone());
                    student_address.setText(response.body().getData().get(0).getAddress());
                    student_birthday.setText(response.body().getData().get(0).getBirthday());
                    student_dorm.setText(response.body().getData().get(0).getDormitoryNamem());
                    student_transport.setText(response.body().getData().get(0).getRouteName());


                    parent_address.setText(response.body().getData().get(0).getParentAddress());
                    parent_email.setText(response.body().getData().get(0).getParentAddress());
                    parent_profession.setText(response.body().getData().get(0).getProfession());


                } else {
                    Toast.makeText(studentprofile.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<TeacherStudentInformationResponse> call, Throwable t) {

                Toast.makeText(studentprofile.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private List<TeacherStudentProfileExam> LoadStudentMarks(String student_id) {
        API api = RestAdapter.createAPI();
        CallBackCall = api.TEACHER_STUDENT_INFORMATION_RESPONSE_CALL(student_id);


        CallBackCall.enqueue(new Callback<TeacherStudentInformationResponse>() {
            @Override
            public void onResponse(Call<TeacherStudentInformationResponse> call, Response<TeacherStudentInformationResponse> response) {

                if (response.body().getSuccess()) {
//

                    marksList = response.body().getMarksSheet();
                    setExamMarks(response.body().getMarksSheet());


                } else {
                    Toast.makeText(studentprofile.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<TeacherStudentInformationResponse> call, Throwable t) {

                Toast.makeText(studentprofile.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });
        return marksList;

    }

    private void setExamMarks(List<TeacherStudentProfileExam> marksList)
    {

        ExamAdapter = new TeacherStudentInformationExamAdapter(marksList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(ExamAdapter);
    }

    private void getTotal(String student_id) {
        API api = RestAdapter.createAPI();
        CallBackCall = api.TEACHER_STUDENT_INFORMATION_RESPONSE_CALL(student_id);

        CallBackCall.enqueue(new Callback<TeacherStudentInformationResponse>() {
            @Override
            public void onResponse(Call<TeacherStudentInformationResponse> call, final Response<TeacherStudentInformationResponse> response) {

                if (response.body().getSuccess()) {


                    //            name.setText(response.body().getData().get(0).getName().toString());

                    totalMarks.setText(response.body().getTotalMarks().toString());
                    AverageGradePoints.setText(response.body().getAvgGradePoint().toString());
//                    marksheet.setOnClickListener(new View.OnClickListener(){
//                        @Override
//                        public void onClick(View view) {
//                            String URL = response.body().getTotalMarks().get;
//                            String FileName = "Marksheet for year "+response.body().getYear();
//                            if (URL.equals("")) {
//
//                                Toast.makeText(studentprofile.this, "No File present", Toast.LENGTH_SHORT).show();
//                            }
//                            else{
//                                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(URL));
//                                request.setDescription("Marksheet");
//                                request.setTitle(FileName);
//// in order for this if to run, you must use the android 3.2 to compile your app
//                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                                    request.allowScanningByMediaScanner();
//                                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//                                }
//                                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, FileName);
//
//// get download service and enqueue file
//                                DownloadManager manager = (DownloadManager) getApplicationContext().getSystemService(Context.DOWNLOAD_SERVICE);
//                                manager.enqueue(request);
//
//                            }
//                        }
//                    });


                } else {
                    Toast.makeText(studentprofile.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<TeacherStudentInformationResponse> call, Throwable t) {

                Toast.makeText(studentprofile.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private List<TeacherStudentProfilePayment> StudentPayment(String student_id) {
        API api = RestAdapter.createAPI();
        CallBackCallPayment = api.TEACHER_STUDENT_INFORMATION_RESPONSE_CALL(student_id);


        CallBackCallPayment.enqueue(new Callback<TeacherStudentInformationResponse>() {
            @Override
            public void onResponse(Call<TeacherStudentInformationResponse> call, Response<TeacherStudentInformationResponse> response) {

                if (response.body().getSuccess()) {


                    paymentList = response.body().getInvoices();
                    setPayment(response.body().getInvoices());


                } else {
                    Toast.makeText(studentprofile.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<TeacherStudentInformationResponse> call, Throwable t) {

                Toast.makeText(studentprofile.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });
        return paymentList;

    }

    private void setPayment(List<TeacherStudentProfilePayment> paymentList)
    {

        PaymentAdapter = new TeacherStudentInformationPaymentAdapter(paymentList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        PaymentRv.setLayoutManager(mLayoutManager);

        PaymentRv.setItemAnimator(new DefaultItemAnimator());
        PaymentRv.setAdapter(PaymentAdapter);
    }



    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void basicbtn(View v){
        LinearLayout basic = (LinearLayout) findViewById(R.id.sp_basic);
        basic.setVisibility(View.VISIBLE);
        LinearLayout parent = (LinearLayout) findViewById(R.id.sp_parent);
        parent.setVisibility(View.INVISIBLE);
        LinearLayout exam = (LinearLayout) findViewById(R.id.sp_exam);
        exam.setVisibility(View.INVISIBLE);
        LinearLayout pay = (LinearLayout) findViewById(R.id.sp_payment);
        pay.setVisibility(View.INVISIBLE);
//        Button btn = (Button) findViewById(R.id.btnbasic);
//        btn.setBackground(this.getResources().getDrawable(R.drawable.btnprofilefocused));

    }

    public void parentbtn(View v){
        LinearLayout basic = (LinearLayout) findViewById(R.id.sp_basic);
        basic.setVisibility(View.INVISIBLE);
        LinearLayout parent = (LinearLayout) findViewById(R.id.sp_parent);
        parent.setVisibility(View.VISIBLE);
        LinearLayout exam = (LinearLayout) findViewById(R.id.sp_exam);
        exam.setVisibility(View.INVISIBLE);
        LinearLayout pay = (LinearLayout) findViewById(R.id.sp_payment);
        pay.setVisibility(View.INVISIBLE);
//        Button btn = (Button) findViewById(R.id.btnbasic);
//        btn.setBackground(this.getResources().getDrawable(R.drawable.btnprofile));
//        Button btn2 = (Button) findViewById(R.id.btnprofile);
//        btn2.setBackground(this.getResources().getDrawable(R.drawable.btnprofilefocused));

    }

    public void exambtn(View v){
        LinearLayout basic = (LinearLayout) findViewById(R.id.sp_basic);
        basic.setVisibility(View.INVISIBLE);
        LinearLayout parent = (LinearLayout) findViewById(R.id.sp_parent);
        parent.setVisibility(View.INVISIBLE);
        LinearLayout exam = (LinearLayout) findViewById(R.id.sp_exam);
        exam.setVisibility(View.VISIBLE);
        LinearLayout pay = (LinearLayout) findViewById(R.id.sp_payment);
        pay.setVisibility(View.INVISIBLE);
//        Button btn = (Button) findViewById(R.id.btnbasic);
//        btn.setBackground(this.getResources().getDrawable(R.drawable.btnprofile));
//        Button btn2 = (Button) findViewById(R.id.btnprofile);
//        btn2.setBackground(this.getResources().getDrawable(R.drawable.btnprofilefocused));

    }

    public void paymentbtn(View v){
        LinearLayout basic = (LinearLayout) findViewById(R.id.sp_basic);
        basic.setVisibility(View.INVISIBLE);
        LinearLayout parent = (LinearLayout) findViewById(R.id.sp_parent);
        parent.setVisibility(View.INVISIBLE);
        LinearLayout exam = (LinearLayout) findViewById(R.id.sp_exam);
        exam.setVisibility(View.INVISIBLE);
        LinearLayout pay = (LinearLayout) findViewById(R.id.sp_payment);
        pay.setVisibility(View.VISIBLE);

//        Button btn = (Button) findViewById(R.id.btnbasic);
//        btn.setBackground(this.getResources().getDrawable(R.drawable.btnprofile));
//        Button btn2 = (Button) findViewById(R.id.btnprofile);
//        btn2.setBackground(this.getResources().getDrawable(R.drawable.btnprofilefocused));

    }








}
