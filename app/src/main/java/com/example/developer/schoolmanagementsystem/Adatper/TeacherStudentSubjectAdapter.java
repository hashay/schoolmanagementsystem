package com.example.developer.schoolmanagementsystem.Adatper;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.developer.schoolmanagementsystem.Model.TeacherStudentSubject;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 3/8/2018.
 */

public class TeacherStudentSubjectAdapter extends RecyclerView.Adapter<TeacherStudentSubjectAdapter.MyViewHolder> {

    private List<TeacherStudentSubject> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.subject_name);
            genre = (TextView) view.findViewById(R.id.class_name);
            year = (TextView) view.findViewById(R.id.subject_teacher_name);

        }
    }
    public TeacherStudentSubjectAdapter(List<TeacherStudentSubject> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public TeacherStudentSubjectAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.subject_list, parent, false);

        return new TeacherStudentSubjectAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TeacherStudentSubjectAdapter.MyViewHolder holder, int position) {
        TeacherStudentSubject data = moviesList.get(position);
        holder.title.setText(data.getName());
        holder.genre.setText(data.getClassName());
        holder.year.setText(data.getTeacherName());
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
