package com.example.developer.schoolmanagementsystem.Model;

/**
 * Created by Developer on 2/20/2018.
 */

public class classstudentsm {

    private String sname, rollnumber, address, email;
    private int imageid;




    public classstudentsm(int imageid, String rollnumber, String address, String email, String sname) {
        this.sname = sname;
        this.rollnumber = rollnumber;
        this.address = address;
        this.email = email;
        this.imageid = imageid;

    }

    public String getSname(){ return sname;}

    public void setSname(String name){ this.sname = name; }

    public String getRollnumber() {
        return rollnumber;
    }

    public void setRollnumber(String name) {
        this.rollnumber = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String name) {
        this.address = name;
    }

    public String getEmail(){ return email; }

    public void setEmail(String name){ this.email = name; }

    public int getImageid() {
        return imageid;
    }

    public void setImageid(int imageid) {
        this.imageid = imageid;
    }

}
