package com.example.developer.schoolmanagementsystem.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Developer on 3/12/2018.
 */

public class TeacherClassRoutineSubjects {

    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("ctime")
    @Expose
    private String ctime;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getCtime() {
        return ctime;
    }

    public void setCtime(String ctime) {
        this.ctime = ctime;
    }


}
