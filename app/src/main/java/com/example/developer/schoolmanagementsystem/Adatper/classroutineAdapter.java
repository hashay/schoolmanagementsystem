package com.example.developer.schoolmanagementsystem.Adatper;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.developer.schoolmanagementsystem.Model.StudentClassRoutine;
import com.example.developer.schoolmanagementsystem.Model.routineclass;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 2/19/2018.
 */

public class classroutineAdapter extends RecyclerView.Adapter<classroutineAdapter.MyViewHolder> {
    private List<StudentClassRoutine> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, time;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.subject_name);
            time = (TextView) view.findViewById(R.id.class_time);


        }
    }

    public classroutineAdapter(List<StudentClassRoutine> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public classroutineAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.classroutine_list, parent, false);

        return new classroutineAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(classroutineAdapter.MyViewHolder holder, int position) {
        StudentClassRoutine data = moviesList.get(position);
        holder.name.setText(data.getSubject());
        holder.time.setText(data.getCtime());



    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }




}
