package com.example.developer.schoolmanagementsystem.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Developer on 3/8/2018.
 */

public class TeacherStudentInformationSubject {

    @SerializedName("subject_id")
    @Expose
    private String subjectId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("class_id")
    @Expose
    private String classId;
    @SerializedName("teacher_id")
    @Expose
    private String teacherId;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("teacher_name")
    @Expose
    private String teacherName;
    @SerializedName("class_name")
    @Expose
    private String className;

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

}
