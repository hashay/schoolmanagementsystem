package com.example.developer.schoolmanagementsystem.Model;

/**
 * Created by Developer on 2/14/2018.
 */

public class Movie {

    private String title;
    private int imageid;



    public Movie(int imageid, String title) {
        this.title = title;
        this.imageid = imageid;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public int getImageid() {
        return imageid;
    }

    public void setImageid(int imageid) {
        this.imageid = imageid;
    }




}
