package com.example.developer.schoolmanagementsystem.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Developer on 3/6/2018.
 */

public class TeachersClasses {

    @SerializedName("class_id")
    @Expose
    private String classId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("name_numeric")
    @Expose
    private String nameNumeric;
    @SerializedName("teacher_id")
    @Expose
    private String teacherId;

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameNumeric() {
        return nameNumeric;
    }

    public void setNameNumeric(String nameNumeric) {
        this.nameNumeric = nameNumeric;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

}
