package com.example.developer.schoolmanagementsystem.Activities;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.attendAdapter;
import com.example.developer.schoolmanagementsystem.Model.TeacherAttendance;
import com.example.developer.schoolmanagementsystem.Model.TeacherAttendanceResponse;
import com.example.developer.schoolmanagementsystem.Model.TeacherClassStudentsSection;
import com.example.developer.schoolmanagementsystem.Model.TeacherClassStudentsSectionResponse;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class attendance extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private List<TeacherAttendance> AttendanceList = new ArrayList<>();
    private RecyclerView recyclerView;
    private attendAdapter AttendanceAdpater;
    List<TeacherClassStudentsSection> sectionList;
    private Call<TeacherAttendanceResponse> TeacherAttendanceCallBackCall;
    private Call<TeacherClassStudentsSectionResponse> TeacherSectionCallBackCall;
    public String class_id,section_id, date,teacher_id;
    public Spinner ShowSections;
    public DatePickerDialog attendanceDatePicker;
    EditText dateEditText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        class_id =  getIntent().getExtras().getString("class_id","");
        teacher_id = getIntent().getExtras().getString("teacher_id","");
        Toolbar toolbar = (Toolbar) findViewById(R.id.studymaterial_toolbar); // check it
        setSupportActionBar(toolbar);

        // adding back functionality

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        initializations();
        ShowSection(teacher_id,class_id);
        classSpinnerOnClick();
        ShowDate();






    }

    private  void  initializations(){
        ShowSections = (Spinner) findViewById(R.id.spinner);
        dateEditText = (EditText) findViewById(R.id.attendanceDatePicker);
        recyclerView = (RecyclerView) findViewById(R.id.attendance_recycler_view);
        SimpleDateFormat sdf = new SimpleDateFormat( "dd-MM-yyyy" );
        dateEditText.setText( sdf.format(new Date()) );
        date=sdf.format(new Date());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void ShowSection(String teacher_id,String class_id) {
        API api = RestAdapter.createAPI();

        TeacherSectionCallBackCall = api.TEACHER_CLASS_STUDENTS_SECTION_RESPONSE_CALL(teacher_id,class_id);

        TeacherSectionCallBackCall.enqueue(new Callback<TeacherClassStudentsSectionResponse>() {
            @Override
            public void onResponse(Call<TeacherClassStudentsSectionResponse> call, Response<TeacherClassStudentsSectionResponse> response) {


                if (response.body().getSuccess()) {
                    sectionList = response.body().getData();
                    List<String> ListSection = new ArrayList<String>();
                    ListSection.clear();
                    for (int i = 0; i < sectionList.size(); i++){
                        ListSection.add(sectionList.get(i).getName());
                        ArrayAdapter<String> csAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.custom_spinner_box, ListSection);
                        csAdapter.setDropDownViewResource(R.layout.custom_dropdown_spinner_box);
                        ShowSections.setAdapter(csAdapter);


                    }
                    section_id = response.body().getData().get(0).getSectionId();

                } else {
                    Toast.makeText(attendance.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<TeacherClassStudentsSectionResponse> call, Throwable t) {

                Toast.makeText(attendance.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void ShowDate(){
        dateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current dateEditText , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current date
                // dateEditText picker dialog
                attendanceDatePicker = new DatePickerDialog(attendance.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set date of month , month and year value in the edit text
                                dateEditText.setText(dayOfMonth + "-"
                                        + (monthOfYear + 1) + "-" + year);
                                date = dateEditText.getText().toString();
                                LoadAttendance();

                            }
                        }, mYear, mMonth, mDay);
                attendanceDatePicker.show();

            }
        });

    }

    public void classSpinnerOnClick(){
        ShowSections.setOnItemSelectedListener(this);
        ShowSections.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> obj, View view, int position, long id) {

                section_id = sectionList.get(position).getSectionId();

                LoadAttendance();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private List<TeacherAttendance> AttendanceForTeacher(String class_id, String section_id, String date) {
        API api = RestAdapter.createAPI();
        TeacherAttendanceCallBackCall = api.TEACHER_ATTENDANCE_RESPONSE_CALL(class_id,section_id,date);


        TeacherAttendanceCallBackCall.enqueue(new Callback<TeacherAttendanceResponse>() {
            @Override
            public void onResponse(Call<TeacherAttendanceResponse> call, Response<TeacherAttendanceResponse> response) {

                if (response.body().getHTTPResponseCode().equals(200)) {
//
                    Toast.makeText(attendance.this,"success",Toast.LENGTH_SHORT).show();
                    AttendanceList = response.body().getData();
                    setRoutine(response.body().getData());

                } else {
                    Toast.makeText(attendance.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TeacherAttendanceResponse> call, Throwable t) {
                Log.e("Error","--"+t.getMessage());
                Toast.makeText(attendance.this, "incorrect"+t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
        return AttendanceList;

    }

    private void setRoutine(List<TeacherAttendance> AttendanceList)
    {

        AttendanceAdpater = new attendAdapter(AttendanceList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(AttendanceAdpater);
    }


    public void LoadAttendance(){
        AttendanceAdpater = new attendAdapter(AttendanceForTeacher(class_id,section_id, date));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(AttendanceAdpater);

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
