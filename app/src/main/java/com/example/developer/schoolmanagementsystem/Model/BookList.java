package com.example.developer.schoolmanagementsystem.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Developer on 3/5/2018.
 */

public class BookList {

    @SerializedName("book_id")
    @Expose
    private String bookId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("author")
    @Expose
    private String author;
    @SerializedName("class_id")
    @Expose
    private String classId;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("total_copies")
    @Expose
    private Object totalCopies;
    @SerializedName("issued_copies")
    @Expose
    private Object issuedCopies;
    @SerializedName("status")
    @Expose
    private Object status;
    @SerializedName("class_name")
    @Expose
    private String className;

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Object getTotalCopies() {
        return totalCopies;
    }

    public void setTotalCopies(Object totalCopies) {
        this.totalCopies = totalCopies;
    }

    public Object getIssuedCopies() {
        return issuedCopies;
    }

    public void setIssuedCopies(Object issuedCopies) {
        this.issuedCopies = issuedCopies;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

}
