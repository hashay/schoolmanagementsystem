package com.example.developer.schoolmanagementsystem.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.TeacherStudentInformationExamAdapter;
import com.example.developer.schoolmanagementsystem.Model.TeacherStudentInformationResponse;
import com.example.developer.schoolmanagementsystem.Model.TeacherStudentProfileExam;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeacherStudentProfileExams extends AppCompatActivity {
    public String student_id;
    private Call<TeacherStudentInformationResponse> CallBackCall;
    private List<TeacherStudentProfileExam> marksList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TeacherStudentInformationExamAdapter ExamAdapter;
    public TextView totalMarks, AverageGradePoints;
    public Button marksheet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_student_profile_exams);
        student_id =  getIntent().getExtras().getString("student_id","");

        recyclerView = (RecyclerView) findViewById(R.id.exammarks_recycler_view);
        totalMarks = (TextView) findViewById(R.id.exam_total);
        AverageGradePoints = (TextView) findViewById(R.id.exam_grade);

        LoadStudentExamMarksRecyclerView();
        getTotal(student_id);

    }

    private List<TeacherStudentProfileExam> LoadStudentMarks(String student_id) {
        API api = RestAdapter.createAPI();
        CallBackCall = api.TEACHER_STUDENT_INFORMATION_RESPONSE_CALL(student_id);


        CallBackCall.enqueue(new Callback<TeacherStudentInformationResponse>() {
            @Override
            public void onResponse(Call<TeacherStudentInformationResponse> call, Response<TeacherStudentInformationResponse> response) {

                if (response.body().getSuccess()) {
//

                    marksList = response.body().getMarksSheet();
                    setExamMarks(response.body().getMarksSheet());

                } else {
                    Toast.makeText(TeacherStudentProfileExams.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<TeacherStudentInformationResponse> call, Throwable t) {

                Toast.makeText(TeacherStudentProfileExams.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });
        return marksList;

    }

    private void setExamMarks(List<TeacherStudentProfileExam> marksList)
    {

        ExamAdapter = new TeacherStudentInformationExamAdapter(marksList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(ExamAdapter);
    }


    private void getTotal(String student_id) {
        API api = RestAdapter.createAPI();
        CallBackCall = api.TEACHER_STUDENT_INFORMATION_RESPONSE_CALL(student_id);

        CallBackCall.enqueue(new Callback<TeacherStudentInformationResponse>() {
            @Override
            public void onResponse(Call<TeacherStudentInformationResponse> call, final Response<TeacherStudentInformationResponse> response) {

                if (response.body().getSuccess()) {



                    totalMarks.setText(response.body().getTotalMarks().toString());
                    AverageGradePoints.setText(response.body().getAvgGradePoint().toString());
//                    marksheet.setOnClickListener(new View.OnClickListener(){
//                        @Override
//                        public void onClick(View view) {
//                            String URL = response.body().getTotalMarks().get;
//                            String FileName = "Marksheet for year "+response.body().getYear();
//                            if (URL.equals("")) {
//
//                                Toast.makeText(studentprofile.this, "No File present", Toast.LENGTH_SHORT).show();
//                            }
//                            else{
//                                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(URL));
//                                request.setDescription("Marksheet");
//                                request.setTitle(FileName);
//// in order for this if to run, you must use the android 3.2 to compile your app
//                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                                    request.allowScanningByMediaScanner();
//                                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//                                }
//                                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, FileName);
//
//// get download service and enqueue file
//                                DownloadManager manager = (DownloadManager) getApplicationContext().getSystemService(Context.DOWNLOAD_SERVICE);
//                                manager.enqueue(request);
//
//                            }
//                        }
//                    });


                } else {
                    Toast.makeText(TeacherStudentProfileExams.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<TeacherStudentInformationResponse> call, Throwable t) {

                Toast.makeText(TeacherStudentProfileExams.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void LoadStudentExamMarksRecyclerView() {

        ExamAdapter = new TeacherStudentInformationExamAdapter(LoadStudentMarks(student_id));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(ExamAdapter);

    }

}
