package com.example.developer.schoolmanagementsystem.Adatper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Activities.details;
import com.example.developer.schoolmanagementsystem.Activities.requestbookform;
import com.example.developer.schoolmanagementsystem.Model.BookList;
import com.example.developer.schoolmanagementsystem.Model.Movie;
import com.example.developer.schoolmanagementsystem.Model.book;
import com.example.developer.schoolmanagementsystem.Model.libreq;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 2/19/2018.
 */

public class bookAdapter extends RecyclerView.Adapter<bookAdapter.MyViewHolder>{

    private List<BookList> moviesList;
    Context context;
    boolean isTeacherOrStudent;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView book, author , description , price , classname;
        private Button main;

        public MyViewHolder(View view) {
            super(view);
            book = (TextView) view.findViewById(R.id.booklist_title);
            author    = (TextView) view.findViewById(R.id.booklist_author);
            description = (TextView) view.findViewById(R.id.booklist_description);
            price = (TextView) view.findViewById(R.id.booklidt_price);
            classname = (TextView) view.findViewById(R.id.booklist_class);

            main = (Button) view.findViewById(R.id.booklist_request);


        }
    }

    public bookAdapter(List<BookList> moviesList, Context context,boolean isTeacherOrStudent) {
        this.moviesList = moviesList;
        this.context = context;
        this.isTeacherOrStudent = isTeacherOrStudent;
    }

    @Override
    public bookAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.book_list, parent, false);

        return new bookAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(bookAdapter.MyViewHolder holder, int position) {
        final BookList material = moviesList.get(position);
        holder.book.setText(material.getName());
        // holder.genre.setText(data.getGenre());
        // holder.year.setText(data.getYear());
        holder.author.setText(material.getAuthor());
        holder.description.setText(material.getDescription());
        holder.price.setText(material.getPrice());
        holder.classname.setText(material.getClassName());
        if (isTeacherOrStudent){
            holder.main.setVisibility(View.GONE);
        }else {
            holder.main.setVisibility(View.VISIBLE);
            holder.main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String bookname = material.getName();
                    String bookid = material.getBookId();
                    SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
                    String student_id =  pref.getString("student_id", "");
                    context.startActivity(new Intent(context,requestbookform.class).putExtra("book_name",bookname).putExtra("book_id",bookid).putExtra("student_id",student_id));


                }
            });
        }



    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}
