package com.example.developer.schoolmanagementsystem.Adatper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.developer.schoolmanagementsystem.Activities.classstudents;
import com.example.developer.schoolmanagementsystem.Model.TeachersClasses;
import com.example.developer.schoolmanagementsystem.Model.libreq;
import com.example.developer.schoolmanagementsystem.Model.studentclass;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 2/19/2018.
 */

public class studentclassnameAdapter extends RecyclerView.Adapter<studentclassnameAdapter.MyViewHolder>{

    private List<TeachersClasses> moviesList;

    Context context;




    public class MyViewHolder extends RecyclerView.ViewHolder {
        public Button classbutton;

        public MyViewHolder(View view) {
            super(view);
            classbutton = (Button) view.findViewById(R.id.studentclassbtn);


        }
    }

    public studentclassnameAdapter(List<TeachersClasses> moviesList, Context context) {
        this.moviesList = moviesList;
        this.context = context;
    }

    @Override
    public studentclassnameAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.studentclass_list, parent, false);

        return new studentclassnameAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(studentclassnameAdapter.MyViewHolder holder, int position) {
        final TeachersClasses material = moviesList.get(position);
        holder.classbutton.setText(material.getName());
        holder.classbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String classid = material.getClassId();
                String teacher_id = material.getTeacherId();
                context.startActivity(new Intent(context,classstudents.class).putExtra("class_id",classid).putExtra("teacher_id",teacher_id));

            }
        });








    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}
