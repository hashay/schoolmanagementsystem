package com.example.developer.schoolmanagementsystem.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Developer on 3/8/2018.
 */

public class TeacherStudentProfileExam {

    @SerializedName("mark_obtained")
    @Expose
    private Object markObtained;
    @SerializedName("grade_name")
    @Expose
    private Object gradeName;
    @SerializedName("highest_mark")
    @Expose
    private String highestMark;
    @SerializedName("mark_total")
    @Expose
    private Object markTotal;
    @SerializedName("comment")
    @Expose
    private Object comment;
    @SerializedName("subject_name")
    @Expose
    private String subjectName;

    public Object getMarkObtained() {
        return markObtained;
    }

    public void setMarkObtained(Object markObtained) {
        this.markObtained = markObtained;
    }

    public Object getGradeName() {
        return gradeName;
    }

    public void setGradeName(Object gradeName) {
        this.gradeName = gradeName;
    }

    public String getHighestMark() {
        return highestMark;
    }

    public void setHighestMark(String highestMark) {
        this.highestMark = highestMark;
    }

    public Object getMarkTotal() {
        return markTotal;
    }

    public void setMarkTotal(Object markTotal) {
        this.markTotal = markTotal;
    }

    public Object getComment() {
        return comment;
    }

    public void setComment(Object comment) {
        this.comment = comment;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

}
