package com.example.developer.schoolmanagementsystem.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.example.developer.schoolmanagementsystem.Adatper.tbookadapter;
import com.example.developer.schoolmanagementsystem.Model.tbook;
import com.example.developer.schoolmanagementsystem.R;

import java.util.ArrayList;
import java.util.List;

public class teacherlibrary extends AppCompatActivity {

    private List<tbook> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private tbookadapter mnAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacherlibrary);

        Toolbar toolbar = (Toolbar) findViewById(R.id.studymaterial_toolbar); // check it
        setSupportActionBar(toolbar);

        // adding back functionality

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.tbooklist_recycler_view);

        mnAdapter = new tbookadapter(movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);


        prepareTeacherData();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    private void prepareTeacherData() {
        tbook material = new tbook("The Hungry Fox","stephen","this is a very good book to learn", "10","1");
        movieList.add(material);

        material = new tbook("The Hungry dog","stephen","this is a very good book to learn", "10","1");
        movieList.add(material);

        material = new tbook("The Hungry cat","stephen","this is a very good book to learn", "10","1");
        movieList.add(material);

        material = new tbook("The Hungry fish","stephen","this is a very good book to learn", "10","1");
        movieList.add(material);

        material = new tbook("The Hungry rat","stephen","this is a very good book to learn", "10","1");
        movieList.add(material);

        material = new tbook("The Hungry human","stephen","this is a very good book to learn", "10","1");
        movieList.add(material);

        mnAdapter.notifyDataSetChanged();
    }

}
