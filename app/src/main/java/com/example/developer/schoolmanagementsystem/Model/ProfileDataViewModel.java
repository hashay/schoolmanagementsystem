package com.example.developer.schoolmanagementsystem.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Developer on 3/27/2018.
 */

public class ProfileDataViewModel {

    @SerializedName("librarian_id")
    @Expose
    private String librarianId;
    @SerializedName("teacher_id")
    @Expose
    private String teacherId;
    @SerializedName("student_id")
    @Expose
    private String studentId;
    @SerializedName("student_code")
    @Expose
    private Object studentCode;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("religion")
    @Expose
    private Object religion;
    @SerializedName("blood_group")
    @Expose
    private Object bloodGroup;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("parent_id")
    @Expose
    private String parentId;
    @SerializedName("dormitory_id")
    @Expose
    private String dormitoryId;
    @SerializedName("transport_id")
    @Expose
    private String transportId;
    @SerializedName("dormitory_room_number")
    @Expose
    private Object dormitoryRoomNumber;
    @SerializedName("authentication_key")
    @Expose
    private Object authenticationKey;
    @SerializedName("photo")
    @Expose
    private String photo;

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getLibrarianId() {
        return librarianId;
    }

    public void setLibrarianId(String librarianId) {
        this.librarianId = librarianId;
    }


    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public Object getStudentCode() {
        return studentCode;
    }

    public void setStudentCode(Object studentCode) {
        this.studentCode = studentCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Object getReligion() {
        return religion;
    }

    public void setReligion(Object religion) {
        this.religion = religion;
    }

    public Object getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(Object bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getDormitoryId() {
        return dormitoryId;
    }

    public void setDormitoryId(String dormitoryId) {
        this.dormitoryId = dormitoryId;
    }

    public String getTransportId() {
        return transportId;
    }

    public void setTransportId(String transportId) {
        this.transportId = transportId;
    }

    public Object getDormitoryRoomNumber() {
        return dormitoryRoomNumber;
    }

    public void setDormitoryRoomNumber(Object dormitoryRoomNumber) {
        this.dormitoryRoomNumber = dormitoryRoomNumber;
    }

    public Object getAuthenticationKey() {
        return authenticationKey;
    }

    public void setAuthenticationKey(Object authenticationKey) {
        this.authenticationKey = authenticationKey;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

}
