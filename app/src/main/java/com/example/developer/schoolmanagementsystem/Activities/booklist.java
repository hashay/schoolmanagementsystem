package com.example.developer.schoolmanagementsystem.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.MovieAdapter;
import com.example.developer.schoolmanagementsystem.Adatper.bookAdapter;
import com.example.developer.schoolmanagementsystem.Adatper.libreqAdapter;
import com.example.developer.schoolmanagementsystem.Model.BookList;
import com.example.developer.schoolmanagementsystem.Model.BookListResponse;
import com.example.developer.schoolmanagementsystem.Model.Movie;
import com.example.developer.schoolmanagementsystem.Model.book;
import com.example.developer.schoolmanagementsystem.Model.libreq;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class booklist extends AppCompatActivity {
    private Call<BookListResponse> callbackCall;
    private List<BookList> booklists = new ArrayList<>();
    private RecyclerView recyclerView;
    private bookAdapter mnAdapter;
    public String student_id,teacher_id;
    boolean isTechStd;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booklist);


        Toolbar toolbar = (Toolbar) findViewById(R.id.studymaterial_toolbar); // check it
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.booklist_recycler_view);
        if(getIntent().getStringExtra("student_id") != null){
            student_id =  getIntent().getExtras().getString("student_id","");
            isTechStd=false;
            LoadDataByStudent(isTechStd);
        }else if (getIntent().getStringExtra("teacher_id") != null){
            student_id = getIntent().getExtras().getString("teacher_id","");
            isTechStd=true;
            LoadDataByStudent(isTechStd);

        }else if (getIntent().getStringExtra("parent_id") != null) {
            student_id = getIntent().getExtras().getString("parent_id", "");
            isTechStd=true;
            LoadDataByStudent(isTechStd);
        }
//        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
//        SharedPreferences.Editor editor = pref.edit();
//        editor.putString("student_id", student_id); // Storing string
//        editor.commit();
        // adding back functionality






        //prepareTeacherData();
    }

    public void LoadDataByStudent(boolean isTechStd){
        mnAdapter = new bookAdapter(teacherDetailsByStId(student_id),this,true);



        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private List<BookList> teacherDetailsByStId(String student_id) {
        API api = RestAdapter.createAPI();
        callbackCall = api.BookListResponse(student_id);


        callbackCall.enqueue(new Callback<BookListResponse>() {
            @Override
            public void onResponse(Call<BookListResponse> call, Response<BookListResponse> response) {

                if (response.body().getSuccess()) {
//
                    Toast.makeText(booklist.this,"success",Toast.LENGTH_SHORT).show();
                    booklists = response.body().getData();
                    setTeacherList(response.body().getData(),isTechStd);

                } else {
                    Toast.makeText(booklist.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<BookListResponse> call, Throwable t) {
                Log.e("Error","--"+t.getMessage());

                Toast.makeText(booklist.this, "incorrect"+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return booklists;

    }

    private void setTeacherList(List<BookList> booklists,boolean isTechStd)
    {

        mnAdapter = new bookAdapter(booklists,this,isTechStd);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);
    }




}
