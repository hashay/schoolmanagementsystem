package com.example.developer.schoolmanagementsystem.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.LibrarianRequestBookAdapter;
import com.example.developer.schoolmanagementsystem.Adatper.exammarksAdapter;
import com.example.developer.schoolmanagementsystem.Adatper.libreqAdapter;
import com.example.developer.schoolmanagementsystem.Model.LibrarianBookRequest;
import com.example.developer.schoolmanagementsystem.Model.LibrarianBookRequestResponse;
import com.example.developer.schoolmanagementsystem.Model.RequestBook;
import com.example.developer.schoolmanagementsystem.Model.RequestBookResponse;
import com.example.developer.schoolmanagementsystem.Model.libreq;
import com.example.developer.schoolmanagementsystem.Model.marks;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class requestbook extends AppCompatActivity {
    private Call<RequestBookResponse> callbackCall;
    private Call<LibrarianBookRequestResponse> LibBookRequestCallBackCall;
    private List<RequestBook> requestBooks = new ArrayList<>();
    private List<LibrarianBookRequest> LibBookRequestList = new ArrayList<>();
    private RecyclerView recyclerView;
    private libreqAdapter mnAdapter;
    private LibrarianRequestBookAdapter LibrarianBookAdapter;
    public String student_id,librarian_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requestbook);
        if(getIntent().getStringExtra("student_id") != null){
            student_id =  getIntent().getExtras().getString("student_id","");
            recyclerView = (RecyclerView) findViewById(R.id.requestbook_recycler_view);
            LoadData();
        } else if (getIntent().getStringExtra("librarian_id") != null){
            librarian_id = getIntent().getExtras().getString("parent_id","");
            recyclerView = (RecyclerView) findViewById(R.id.requestbook_recycler_view);
            LoadLibrarianData();
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.studymaterial_toolbar); // check it
        setSupportActionBar(toolbar);

        // adding back functionality

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);









    }

    private void LoadData() {

        mnAdapter = new libreqAdapter(getRequestBooks(student_id));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);
    }

    private void LoadLibrarianData() {

        LibrarianBookAdapter = new LibrarianRequestBookAdapter(getLibrarianRequests(librarian_id));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(LibrarianBookAdapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private List<RequestBook> getRequestBooks(String student_id) {
        API api = RestAdapter.createAPI();
        callbackCall = api.RequestBookResponse(student_id);


        callbackCall.enqueue(new Callback<RequestBookResponse>() {
            @Override
            public void onResponse(Call<RequestBookResponse> call, Response<RequestBookResponse> response) {

                if (response.body().getSuccess()) {
//

                    requestBooks = response.body().getData();
                    setTeacherList(response.body().getData());
                    //Toast.makeText(requestbook.this, "Date 1"+booklists.get(0).getName(), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(requestbook.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<RequestBookResponse> call, Throwable t) {
                Log.e("Error","--"+t.getMessage());
                Toast.makeText(requestbook.this, "incorrect"+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return requestBooks;

    }

    private void setTeacherList(List<RequestBook> requestBooks)
    {

        mnAdapter = new libreqAdapter(requestBooks);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mnAdapter);
    }

    private List<LibrarianBookRequest> getLibrarianRequests(String librarian_id) {
        API api = RestAdapter.createAPI();
        LibBookRequestCallBackCall = api.LIBRARIAN_BOOK_REQUEST_RESPONSE_CALL(librarian_id);


        LibBookRequestCallBackCall.enqueue(new Callback<LibrarianBookRequestResponse>() {
            @Override
            public void onResponse(Call<LibrarianBookRequestResponse> call, Response<LibrarianBookRequestResponse> response) {

                if (response.body().getSuccess()) {
//

                    LibBookRequestList = response.body().getData();
                    setLibrarianBookList(response.body().getData());
                    //Toast.makeText(requestbook.this, "Date 1"+booklists.get(0).getName(), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(requestbook.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<LibrarianBookRequestResponse> call, Throwable t) {
                Log.e("Error","--"+t.getMessage());
                Toast.makeText(requestbook.this, "incorrect"+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return LibBookRequestList;

    }

    private void setLibrarianBookList(List<LibrarianBookRequest> LibBookRequestList)
    {

        LibrarianBookAdapter = new LibrarianRequestBookAdapter(LibBookRequestList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(LibrarianBookAdapter);
    }



}
