package com.example.developer.schoolmanagementsystem.Adatper;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.developer.schoolmanagementsystem.Model.TeacherAttendance;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 2/21/2018.
 */

public class attendAdapter extends RecyclerView.Adapter<attendAdapter.MyViewHolder>{

    private List<TeacherAttendance> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, rollnumber , status;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.attendance_name);
            rollnumber    = (TextView) view.findViewById(R.id.attendance_rollnumber);
            status = (TextView) view.findViewById(R.id.attendance_status);

        }
    }

    public attendAdapter(List<TeacherAttendance> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public attendAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.attendance_list, parent, false);

        return new attendAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(attendAdapter.MyViewHolder holder, int position) {
        TeacherAttendance material = moviesList.get(position);
        holder.name.setText(material.getStudentName());
        // holder.genre.setText(data.getGenre());
        // holder.year.setText(data.getYear());
        holder.rollnumber.setText(material.getAttendanceId());
        holder.status.setText(material.getStatus());
        try {
            if(material.getStatus().equals("1")){
                holder.status.setTextColor(Color.parseColor("#FF0000"));
                holder.status.setText("Present");
            }else if(material.getStatus().equals("2")){
                holder.status.setTextColor(Color.parseColor("#00FF00"));
                holder.status.setText("Absent");
            }else{
                holder.status.setTextColor(Color.parseColor("#000000"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}
