package com.example.developer.schoolmanagementsystem.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.developer.schoolmanagementsystem.R;

public class library extends AppCompatActivity {
    private String student_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);
        student_id =  getIntent().getExtras().getString("student_id","");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_library); // check it
        setSupportActionBar(toolbar);

        // adding back functionality

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    public void requestbook(View v)
    {
        Intent intent = new Intent(getApplicationContext(), requestbook.class);
        intent.putExtra("student_id",student_id);
        startActivity(intent);
        //finish();
    }

    public void booklist(View v)
    {
        Intent intent = new Intent(getApplicationContext(), booklist.class);
        intent.putExtra("student_id",student_id);
        startActivity(intent);
        //finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
