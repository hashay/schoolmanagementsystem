package com.example.developer.schoolmanagementsystem.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Model.LoginResponse;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.developer.schoolmanagementsystem.Activities.login.MY_PREFS_NAME;

public class MainActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 5000;
    private Call<LoginResponse> callbackCall;
    public String email, pswd,usertype,userId,Username;
    Intent i;
    SharedPreferences preferences;
    SharedPreferences prefs2 ;
    public Boolean saveLogin = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();
        prefs2 = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                if(prefs2.getBoolean("CheckBox",saveLogin) == true){
                    email = prefs2.getString("username",null);
                    pswd = prefs2.getString("password",null);
                    usertype = prefs2.getString("usertype",null);
                    switch (usertype){
                        case "student":
                            Login(email,pswd,usertype);
                            break;
                        case "teacher":
                            Login(email,pswd,usertype);
                            break;
                        case "librarian":
                            Login(email,pswd,usertype);
                            break;
                        case "parent":
                            Login(email,pswd,usertype);
                            break;
                    }

                }else {
                    i= new Intent(MainActivity.this , login.class);
                    startActivity(i);
                }

                finish();
            }
        }, SPLASH_TIME_OUT);
        //splash screen timer end
    }

    private void Login(String email,String password,String type) {
        API api = RestAdapter.createAPI();
        callbackCall = api.login(email, password,type);

        callbackCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if (response.body().getSuccess()) {
//
                    if (response.body().getData().getUserType().equals("student")) {
                        Intent intent = new Intent(getApplicationContext(), student.class);
                        userId = response.body().getData().getStudentId();
                        Username = response.body().getData().getName();
                        intent.putExtra("studentid", userId);
                        intent.putExtra("student_name",Username);
                        startActivity(intent);
                    } else if (response.body().getData().getUserType().equals("teacher")) {

                        Intent intent = new Intent(MainActivity.this, teahcershome.class);
                        intent.putExtra("teacherid", response.body().getData().getTeacherId());
                        intent.putExtra("teacher_name", response.body().getData().getName());

                        startActivity(intent);
                    }else if (response.body().getData().getUserType().equals("librarian")) {

                        Intent intent = new Intent(MainActivity.this, librarian.class);
                        intent.putExtra("Librarian", response.body().getData().getLibrarianId());
                        intent.putExtra("librarian_name", response.body().getData().getName());
                        startActivity(intent);
                    }else if (response.body().getData().getUserType().equals("parent")) {

                        Intent intent = new Intent(MainActivity.this, parentHome.class);
                        intent.putExtra("parent_id", response.body().getData().getParentId());
                        intent.putExtra("parent_name", response.body().getData().getName());
                        startActivity(intent);
                    }

                } else {
                    Toast.makeText(MainActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

                Toast.makeText(MainActivity.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });



    }

}




