package com.example.developer.schoolmanagementsystem.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.librarianbookAdapter;
import com.example.developer.schoolmanagementsystem.Model.LibrarianBookResponse;
import com.example.developer.schoolmanagementsystem.Model.librarianbook;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class lbbooklist extends AppCompatActivity {

    private List<librarianbook> LibrarianBookList = new ArrayList<>();
    private RecyclerView LibrarianRecyclerView;
    private librarianbookAdapter LibrarianBookAdapter;
    public Call<LibrarianBookResponse> LibrarianBookListCallBackCall;
    public String librarian_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lbbooklist);
        librarian_id =  getIntent().getExtras().getString("librarian_id","");

        Toolbar toolbar = (Toolbar) findViewById(R.id.studymaterial_toolbar); // check it
        setSupportActionBar(toolbar);

        // adding back functionality

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        LibrarianRecyclerView = (RecyclerView) findViewById(R.id.lbbooklist_recycler_view);
        loadLibBookList();
//





    }

    private void loadLibBookList() {
        LibrarianBookAdapter = new librarianbookAdapter(LibBook(librarian_id));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        LibrarianRecyclerView.setLayoutManager(mLayoutManager);

        LibrarianRecyclerView.setItemAnimator(new DefaultItemAnimator());
        LibrarianRecyclerView.setAdapter(LibrarianBookAdapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    private List<librarianbook> LibBook(String librarian_id) {
        API api = RestAdapter.createAPI();
        LibrarianBookListCallBackCall = api.LIBRARIAN_BOOK_RESPONSE_CALL(librarian_id);


        LibrarianBookListCallBackCall.enqueue(new Callback<LibrarianBookResponse>() {
            @Override
            public void onResponse(Call<LibrarianBookResponse> call, Response<LibrarianBookResponse> response) {

                if (response.body().getSuccess()) {
                    LibrarianBookList = response.body().getData();
                    setMaterialList(response.body().getData());
                    // Toast.makeText(exammarks.this, "Date 1"+movieList.get(0).get(), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(lbbooklist.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<LibrarianBookResponse> call, Throwable t) {

                Toast.makeText(lbbooklist.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });
        return LibrarianBookList;

    }

    private void setMaterialList(List<librarianbook> paymentList)
    {

        LibrarianBookAdapter = new librarianbookAdapter(paymentList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        LibrarianRecyclerView.setLayoutManager(mLayoutManager);

        LibrarianRecyclerView.setItemAnimator(new DefaultItemAnimator());
        LibrarianRecyclerView.setAdapter(LibrarianBookAdapter);
    }




}
