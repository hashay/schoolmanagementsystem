package com.example.developer.schoolmanagementsystem.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.MovieAdapter;
import com.example.developer.schoolmanagementsystem.Model.Movie;
import com.example.developer.schoolmanagementsystem.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarListener;

import static com.example.developer.schoolmanagementsystem.Activities.login.MY_PREFS_NAME;
import static com.example.developer.schoolmanagementsystem.Activities.login.MY_PREFS_NAME2;

public class teahcershome extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private HorizontalCalendar horizontalCalendar;

    //recycler view

    private List<Movie> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MovieAdapter mAdapter;
    public String teacher_id,Teacher_Name,username;
    int numberOfColumns = 3;
    SharedPreferences prefs;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teahcershome);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        teacher_id =  getIntent().getExtras().getString("teacherid","");
        Teacher_Name =  getIntent().getExtras().getString("teacher_name","");
        //getting shared preferences

        prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        username = prefs.getString("username", null);
        //

        calender();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new MovieAdapter(movieList);
        //opening screen by getting data from card view
        mAdapter.setOnItemClickListener(new MovieAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, Movie obj) {
                //  Toast.makeText(student.this, "Toast"+obj.getTitle(), Toast.LENGTH_SHORT).show();

                if(obj.getTitle() == "Teacher") {
                    OpenTeacher();

                } else if(obj.getTitle() == "Student") {
                    OpenStudent();

                }else if(obj.getTitle() == "Subject") {
                    OpenSubject();

                }else if(obj.getTitle() == "Study Material") {
                    OpenStudyMaterial();

                }else if(obj.getTitle() == "Syllabus") {
                    OpenSyllabus();

                }else if(obj.getTitle() == "Student Attendance") {
                    OpenAttendance();

                }else if(obj.getTitle() == "Library") {
                    OpenLibrary();

                }else if(obj.getTitle() == "Class Routine") {
                    OpenClassRoutine();

                }else if(obj.getTitle() == "Exam Mark") {
                    OpenExamMarks();

                }else if(obj.getTitle() == "Transport") {
                    OpenTransport();

                }
                else{
                    Toast.makeText(teahcershome.this, obj.getTitle() + " Screen not ready yet", Toast.LENGTH_SHORT).show();
                }

            }


        });

        LoadHomepage();
        prepareMovieData();
        LoadNav();


    }

    public void OpenStudent(){
        Intent intent = new Intent(getApplicationContext(), studentinformation.class);
        intent.putExtra("teacher_id",teacher_id);
        startActivity(intent);
    }
    public void OpenTeacher(){
        Intent intent = new Intent(getApplicationContext(), teachers.class);
        intent.putExtra("teacher_id",teacher_id);
        startActivity(intent);
    }
    public void OpenSubject(){
        Intent intent = new Intent(getApplicationContext(), selectclasssforsubject.class);
        intent.putExtra("teacher_id",teacher_id);
        startActivity(intent);
    }
    public void OpenClassRoutine(){
        Intent intent = new Intent(getApplicationContext(), teacherclassroutine.class);
        intent.putExtra("teacher_id",teacher_id);
        startActivity(intent);
    }
    public void OpenStudyMaterial(){
        Intent intent = new Intent(getApplicationContext(), studymaterial.class);
        intent.putExtra("teacher_id",teacher_id);
        startActivity(intent);
    }
    public void OpenSyllabus(){
        Intent intent = new Intent(getApplicationContext(), selectclassforsyllabus.class);
        intent.putExtra("teacher_id",teacher_id);
        startActivity(intent);
    }
    public void OpenAttendance(){
        Intent intent = new Intent(getApplicationContext(), selectclassforattendance.class);
        intent.putExtra("teacher_id",teacher_id);
        startActivity(intent);
    }
    public void OpenExamMarks(){
        Intent intent = new Intent(getApplicationContext(), texammenu.class);
        intent.putExtra("teacher_id",teacher_id);
        startActivity(intent);
    }
    public void OpenLibrary(){
        Intent intent = new Intent(getApplicationContext(), booklist.class);
        intent.putExtra("teacher_id",teacher_id);
        startActivity(intent);
    }
    public void OpenTransport(){
        Intent intent = new Intent(getApplicationContext(), transport.class);
        intent.putExtra("teacher_id",teacher_id);
        startActivity(intent);
    }


    private void prepareMovieData() {
        Movie movie = new Movie(R.drawable.student, "Student");
        movieList.add(movie);


        movie = new Movie(R.drawable.teacher, "Teacher");
        movieList.add(movie);

        movie = new Movie(R.drawable.subject, "Subject");
        movieList.add(movie);

        movie = new Movie(R.drawable.classroutine, "Class Routine");
        movieList.add(movie);

        movie = new Movie(R.drawable.studymaterial, "Study Material");
        movieList.add(movie);

        movie = new Movie(R.drawable.syllabus, "Syllabus");
        movieList.add(movie);

        movie = new Movie(R.drawable.dailyattendance, "Student Attendance");
        movieList.add(movie);

        movie = new Movie(R.drawable.exam, "Exam Mark");
        movieList.add(movie);



        movie = new Movie(R.drawable.lib, "Library");
        movieList.add(movie);

        movie = new Movie(R.drawable.transport, "Transport");
        movieList.add(movie);






        mAdapter.notifyDataSetChanged();
    }


    public void calender(){

        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);

        horizontalCalendar = new HorizontalCalendar.Builder(teahcershome.this    , R.id.calendarView)
                .startDate(startDate.getTime())
                .endDate(endDate.getTime())
                .datesNumberOnScreen(5)
                .dayNameFormat("EEE")
                .dayNumberFormat("dd")
                .monthFormat("MMM")
                .textSize(14f, 24f, 14f)
                .showDayName(true)
                .showMonthName(true)

                .build();

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Date date, int position) {
//                Toast.makeText(getContext(), DateFormat.getDateInstance().format(dateEditText) + " is selected!", Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.teahcershome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {

            logout();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.teacher_student) {
            OpenStudent();
        } else if (id == R.id.teacher_teacher) {
            OpenTeacher();
        } else if (id == R.id.teacher_subject) {
            OpenSubject();
        } else if (id == R.id.teacher_classroutine) {
            OpenClassRoutine();
        } else if (id == R.id.teacher_studymaterial) {
            OpenStudyMaterial();
        } else if (id == R.id.teacher_syllabus) {
            OpenSyllabus();
        }else if (id == R.id.teacher_attendance) {
            OpenAttendance();
        }else if (id == R.id.teacher_exammarks) {
            OpenExamMarks();
        }else if (id == R.id.teacher_library) {
            OpenLibrary();
        }else if (id == R.id.teacher_transport) {
            OpenTransport();
        }else if (id == R.id.teacher_logout) {
            logout();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void logout(){
        getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit().clear().commit();
        getSharedPreferences(MY_PREFS_NAME2, MODE_PRIVATE).edit().clear().commit();
        Intent intent = new Intent(getApplicationContext(), login.class);
        startActivity(intent);
        finish();
    }

    public void LoadHomepage(){
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }

    public void LoadNav(){
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_teacher);
        View Hview = navigationView.getHeaderView(0);
        TextView nav_useremail = (TextView)Hview.findViewById(R.id.teacher_email);
        TextView nav_username = (TextView)Hview.findViewById(R.id.teacher_name);
        nav_username.setText(Teacher_Name);
        nav_useremail.setText(username);
        navigationView.setNavigationItemSelectedListener(this);
    }
}
