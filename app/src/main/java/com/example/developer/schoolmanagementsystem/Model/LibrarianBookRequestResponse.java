package com.example.developer.schoolmanagementsystem.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Developer on 3/22/2018.
 */

public class LibrarianBookRequestResponse {

    @SerializedName("data")
    @Expose
    private List<LibrarianBookRequest> data = null;
    @SerializedName("status")
    @Expose
    private LibrarianBookRequestStatus status;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("HTTP_response_code")
    @Expose
    private Integer hTTPResponseCode;

    public List<LibrarianBookRequest> getData() {
        return data;
    }

    public void setData(List<LibrarianBookRequest> data) {
        this.data = data;
    }

    public LibrarianBookRequestStatus getStatus() {
        return status;
    }

    public void setStatus(LibrarianBookRequestStatus status) {
        this.status = status;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getHTTPResponseCode() {
        return hTTPResponseCode;
    }

    public void setHTTPResponseCode(Integer hTTPResponseCode) {
        this.hTTPResponseCode = hTTPResponseCode;
    }
}
