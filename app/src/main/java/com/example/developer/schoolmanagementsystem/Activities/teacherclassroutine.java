package com.example.developer.schoolmanagementsystem.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.TeacherClassRoutineSubjectsAdapter;
import com.example.developer.schoolmanagementsystem.Model.TeacherClassRoutineDays;
import com.example.developer.schoolmanagementsystem.Model.TeacherClassRoutineDaysResponse;
import com.example.developer.schoolmanagementsystem.Model.TeacherClassRoutineSubjects;
import com.example.developer.schoolmanagementsystem.Model.TeacherClassRoutineSubjectsResponse;
import com.example.developer.schoolmanagementsystem.Model.TeacherClassStudentsSection;
import com.example.developer.schoolmanagementsystem.Model.TeacherClassStudentsSectionResponse;
import com.example.developer.schoolmanagementsystem.Model.TeacherClassesResponse;
import com.example.developer.schoolmanagementsystem.Model.TeachersClasses;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class teacherclassroutine extends AppCompatActivity implements AdapterView.OnItemSelectedListener {


//    private List<routineclass> movieList = new ArrayList<>();
//    private RecyclerView recyclerView;
//    private classroutineAdapter mnAdapter;
//
    private List<TeacherClassRoutineSubjects> subjectList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TeacherClassRoutineSubjectsAdapter mAdapter;

    public List<String> ListDays = new ArrayList<String>();
    public String teacher_id,class_id,section_id,day;
    public Spinner showClass,ShowSections,showDays;
    List<TeachersClasses> classList;
    List<TeacherClassStudentsSection> sectionList;
    List<TeacherClassRoutineDays> daysList;
    private Call<TeacherClassRoutineDaysResponse> TeacherDaysCallBackCall;
    private Call<TeacherClassesResponse> TeacherClassesCallBackCall;
    private Call<TeacherClassStudentsSectionResponse> TeacherSectionCallBackCall;
    private Call<TeacherClassRoutineSubjectsResponse> TeacherRoutineCallBackCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacherclassroutine);
        teacher_id =  getIntent().getExtras().getString("teacher_id","");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_subject);
        setSupportActionBar(toolbar);
        ShowClass(teacher_id);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        // Spinner element
        showClass = (Spinner) findViewById(R.id.spinner);
        ShowSections = (Spinner) findViewById(R.id.spinner2);
        showDays = (Spinner) findViewById(R.id.spinner3);
        recyclerView = (RecyclerView) findViewById(R.id.tcr_recycler_view);




        classSpinnerOnClick();
        sectionSpinnerOnClick();
        DaysSpinnerOnClick();










    }

    public void classSpinnerOnClick(){
        showClass.setOnItemSelectedListener(this);
        showClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> obj, View view, int position, long id) {

                class_id = classList.get(position).getClassId();

                ShowSection(teacher_id,class_id);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void sectionSpinnerOnClick() {
        ShowSections.setOnItemSelectedListener(this);
        ShowSections.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> obj, View view, int position, long id) {

                section_id = sectionList.get(position).getSectionId();
                ShowDaysForSpinner(class_id,section_id);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void  DaysSpinnerOnClick(){
        showDays.setOnItemSelectedListener(this);
        showDays.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> obj, View view, int position, long id) {
                if(daysList.get(position).getDayName().equals(null)) {


                }else {
                    day = daysList.get(position).getDayName();


                    LoadRoutine();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void LoadRoutine(){
        mAdapter = new TeacherClassRoutineSubjectsAdapter(ClassRoutineOfTeacher(class_id,section_id,day));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String item = adapterView.getItemAtPosition(i).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void ShowClass(String teacher_id) {
        API api = RestAdapter.createAPI();
        TeacherClassesCallBackCall = api.TeacherClassesResponse(teacher_id);

        TeacherClassesCallBackCall.enqueue(new Callback<TeacherClassesResponse>() {
            @Override
            public void onResponse(Call<TeacherClassesResponse> call, Response<TeacherClassesResponse> response) {

                if (response.body().getSuccess()) {
                    classList = response.body().getData();
                    List<String> listSpinner = new ArrayList<String>();
                    listSpinner.clear();

                    for (int i = 0; i < classList.size(); i++){
                        listSpinner.add(classList.get(i).getNameNumeric());
                        ArrayAdapter<String> csAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.custom_spinner_box, listSpinner);
                        csAdapter.setDropDownViewResource(R.layout.custom_dropdown_spinner_box);
                        showClass.setAdapter(csAdapter);


                    }
                    class_id = response.body().getData().get(0).getClassId();

                } else {
                    Toast.makeText(teacherclassroutine.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<TeacherClassesResponse> call, Throwable t) {

                Toast.makeText(teacherclassroutine.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void ShowSection(String teacher_id,String class_id) {
        API api = RestAdapter.createAPI();

        TeacherSectionCallBackCall = api.TEACHER_CLASS_STUDENTS_SECTION_RESPONSE_CALL(teacher_id,class_id);

        TeacherSectionCallBackCall.enqueue(new Callback<TeacherClassStudentsSectionResponse>() {
            @Override
            public void onResponse(Call<TeacherClassStudentsSectionResponse> call, Response<TeacherClassStudentsSectionResponse> response) {


                if (response.body().getSuccess()) {
                    sectionList = response.body().getData();
                    List<String> ListSection = new ArrayList<String>();
                    ListSection.clear();
                    for (int i = 0; i < sectionList.size(); i++){
                        ListSection.add(sectionList.get(i).getName());
                        ArrayAdapter<String> csAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.custom_spinner_box, ListSection);
                        csAdapter.setDropDownViewResource(R.layout.custom_dropdown_spinner_box);
                        ShowSections.setAdapter(csAdapter);


                    }
                    section_id = response.body().getData().get(0).getSectionId();

                } else {
                    Toast.makeText(teacherclassroutine.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<TeacherClassStudentsSectionResponse> call, Throwable t) {

                Toast.makeText(teacherclassroutine.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void ShowDaysForSpinner(String class_id,String section_id) {
        API api = RestAdapter.createAPI();

        TeacherDaysCallBackCall = api.TEACHER_CLASS_ROUTINE_DAYS_RESPONSE_CALL(class_id,section_id);

        TeacherDaysCallBackCall.enqueue(new Callback<TeacherClassRoutineDaysResponse>() {
            @Override
            public void onResponse(Call<TeacherClassRoutineDaysResponse> call, Response<TeacherClassRoutineDaysResponse> response) {


                if (response.body().getSuccess()) {
                    daysList = response.body().getData();

                    ListDays.clear();
//                    ListDays.equals("");
                    if(daysList.size() > 0) {
                        for (int i = 0; i < daysList.size(); i++) {
                            ListDays.add(daysList.get(i).getDayName());


                            ArrayAdapter<String> csAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.custom_spinner_box, ListDays);
                            csAdapter.setDropDownViewResource(R.layout.custom_dropdown_spinner_box);
                            showDays.setAdapter(csAdapter);


                        }

                    }else{
                        ListDays.clear();
                        showDays.setAdapter(null);
                    }


                    Toast.makeText(teacherclassroutine.this,"your class is " + day , Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(teacherclassroutine.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<TeacherClassRoutineDaysResponse> call, Throwable t) {

                Toast.makeText(teacherclassroutine.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });

    }


    private List<TeacherClassRoutineSubjects> ClassRoutineOfTeacher(String class_id, String section_id, String day) {
        API api = RestAdapter.createAPI();
        TeacherRoutineCallBackCall = api.TEACHER_CLASS_ROUTINE_SUBJECTS_RESPONSE_CALL(class_id,section_id,day);


        TeacherRoutineCallBackCall.enqueue(new Callback<TeacherClassRoutineSubjectsResponse>() {
            @Override
            public void onResponse(Call<TeacherClassRoutineSubjectsResponse> call, Response<TeacherClassRoutineSubjectsResponse> response) {

                if (response.body().getSuccess()) {
//

                    subjectList = response.body().getData();
                    setRoutine(response.body().getData());
                    //Toast.makeText(classroutine.this, "Date 1"+routineList.get(0).getFileName(), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(teacherclassroutine.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TeacherClassRoutineSubjectsResponse> call, Throwable t) {

                Toast.makeText(teacherclassroutine.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });
        return subjectList;

    }

    private void setRoutine(List<TeacherClassRoutineSubjects> subjectList)
    {

        mAdapter = new TeacherClassRoutineSubjectsAdapter(subjectList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }

}
