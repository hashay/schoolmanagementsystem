package com.example.developer.schoolmanagementsystem.Model;

/**
 * Created by Developer on 2/19/2018.
 */

public class book {

    private String book, author,description, price, classname ;

    public book(String book,String author, String description, String price, String classname){
        this.book = book;
        this.author = author;
        this.description = description;
        this.price = price;
        this.classname = classname;



    }


    public String getBook(){
        return  book;

    }

    public void setBook(String name){
        this.book = name;

    }

    public String getAuthor(){
        return author;
    }

    public void  setAuthor(String name){
        this.author = name;
    }

    public String getDescription(){
        return description;
    }

    public void setDescription(String name){
        this.description = name;
    }

    public String getPrice(){
        return price;
    }

    public void setPrice(String name){
        this.price = name;
    }

    public String getClassname(){
        return classname;
    }

    public void setClassname(String name){

        this.classname = name;
    }



}
