package com.example.developer.schoolmanagementsystem.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Model.LoginResponse;
import com.example.developer.schoolmanagementsystem.Model.UserDetails;
import com.example.developer.schoolmanagementsystem.R;
import com.example.developer.schoolmanagementsystem.Tools.API;
import com.example.developer.schoolmanagementsystem.Tools.RestAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class login extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    EditText username, password;
    public String email, pswd,usertype,userId,Username;
    private Call<LoginResponse> callbackCall;
    public Spinner type;
    public CheckBox Remember;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    public static final String MY_PREFS_NAME2 = "MyPrefsFile2";
    public Boolean saveLogin = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //getActionBar().hide();
        getSupportActionBar().hide();
        username=(EditText)findViewById(R.id.email);
        password=(EditText)findViewById(R.id.password);
        Remember = (CheckBox)findViewById(R.id.rememberPassword);
        SharedPreferences prefs2 = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);

//        String userid = prefs2.getString("id", null);
        if(prefs2.getBoolean("CheckBox",saveLogin) == true){
            username.setText(String.valueOf(prefs2.getString("username",null)));
            password.setText(String.valueOf(prefs2.getString("password",null)));
            usertype = prefs2.getString("usertype", null);
        }


         type = (Spinner) findViewById(R.id.spinner);

        type.setOnItemSelectedListener(this);

        List<String> user = new ArrayList<String>();
        user.add("student");
        user.add("parent");
        user.add("teacher");
        user.add("librarian");
        ArrayAdapter<String> csAdapter = new ArrayAdapter<String>(this,R.layout.login_spinner_box, user);

        csAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        type.setAdapter(csAdapter);

    }

    public void buttonClickFunction(View v)
    {

        email = username.getText().toString();
        pswd = password.getText().toString();
        usertype = type.getSelectedItem().toString();
        Remember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Remember.isChecked()){
                    saveLogin = true;
                }else{
                    saveLogin = false;
                }
            }
        });
        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString("username", email);
        editor.putString("password",pswd);
        editor.putString("usertype",usertype);
       editor.putBoolean("Checked",saveLogin);
        editor.apply();





        Login(email,pswd,usertype);



    }

    private void Login(String email,String password,String type) {
        API api = RestAdapter.createAPI();
        callbackCall = api.login(email, password,type);

        callbackCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if (response.body().getSuccess()) {
//
                    if (response.body().getData().getUserType().equals("student")) {
                        Intent intent = new Intent(getApplicationContext(), student.class);
                        userId = response.body().getData().getStudentId();
                        Username = response.body().getData().getName();
                        intent.putExtra("user_type",usertype);
                        intent.putExtra("studentid", userId);
                        intent.putExtra("student_name",Username);
                        startActivity(intent);
                    } else if (response.body().getData().getUserType().equals("teacher")) {

                        Intent intent = new Intent(login.this, teahcershome.class);
                        intent.putExtra("teacherid", response.body().getData().getTeacherId());
                        intent.putExtra("teacher_name", response.body().getData().getName());

                        startActivity(intent);
                    }else if (response.body().getData().getUserType().equals("librarian")) {

                        Intent intent = new Intent(login.this, librarian.class);
                        intent.putExtra("Librarian", response.body().getData().getLibrarianId());
                        intent.putExtra("librarian_name", response.body().getData().getName());
                        startActivity(intent);
                    }else if (response.body().getData().getUserType().equals("parent")) {

                        Intent intent = new Intent(login.this, parentHome.class);
                        intent.putExtra("parent_id", response.body().getData().getParentId());
                        intent.putExtra("parent_name", response.body().getData().getName());
                        startActivity(intent);
                    }

                } else {

                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

                Toast.makeText(login.this, "incorrect", Toast.LENGTH_SHORT).show();
            }
        });

        if(Remember.isChecked()){
            SharedPreferences.Editor Reditor = getSharedPreferences(MY_PREFS_NAME2, MODE_PRIVATE).edit();
            Reditor.putString("username", email);
            Reditor.putString("password",pswd);
            Reditor.putString("usertype",usertype);
            Reditor.putString("userid",userId);
            Reditor.putString("username",Username);
            Reditor.putBoolean("CheckBox",saveLogin);
            Reditor.apply();
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
