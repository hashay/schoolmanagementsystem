package com.example.developer.schoolmanagementsystem.Tools;



import com.example.developer.schoolmanagementsystem.Model.AcademicSyllabusResponse;
import com.example.developer.schoolmanagementsystem.Model.BookListResponse;
import com.example.developer.schoolmanagementsystem.Model.ExamClassModelResponse;
import com.example.developer.schoolmanagementsystem.Model.ExamListResponse;
import com.example.developer.schoolmanagementsystem.Model.ExamSectionResponse;
import com.example.developer.schoolmanagementsystem.Model.ExamSubjectsResponse;
import com.example.developer.schoolmanagementsystem.Model.InvoiceResponse;
import com.example.developer.schoolmanagementsystem.Model.LibrarianBookRequestResponse;
import com.example.developer.schoolmanagementsystem.Model.LibrarianBookResponse;
import com.example.developer.schoolmanagementsystem.Model.LoginResponse;
import com.example.developer.schoolmanagementsystem.Model.PAttendanceResponse;
import com.example.developer.schoolmanagementsystem.Model.ParentChildResponse;
import com.example.developer.schoolmanagementsystem.Model.ProfileDataUpdateResponse;
import com.example.developer.schoolmanagementsystem.Model.ProfileDataViewResponse;
import com.example.developer.schoolmanagementsystem.Model.RequestBookFormResponse;
import com.example.developer.schoolmanagementsystem.Model.RequestBookResponse;
import com.example.developer.schoolmanagementsystem.Model.StudentClassRoutineDaysResponse;
import com.example.developer.schoolmanagementsystem.Model.StudentClassRoutineResponse;
import com.example.developer.schoolmanagementsystem.Model.StudentMarksheetResponse;
import com.example.developer.schoolmanagementsystem.Model.StudentStudyMaterialResponse;
import com.example.developer.schoolmanagementsystem.Model.SubjectDetailsResponse;
import com.example.developer.schoolmanagementsystem.Model.TDetailsResponse;
import com.example.developer.schoolmanagementsystem.Model.TeacherAcademicSyllabusResponse;
import com.example.developer.schoolmanagementsystem.Model.TeacherAttendanceResponse;
import com.example.developer.schoolmanagementsystem.Model.TeacherClassRoutineDaysResponse;
import com.example.developer.schoolmanagementsystem.Model.TeacherClassRoutineSubjectsResponse;
import com.example.developer.schoolmanagementsystem.Model.TeacherClassStudentRecordResponse;
import com.example.developer.schoolmanagementsystem.Model.TeacherClassStudentsSectionResponse;
import com.example.developer.schoolmanagementsystem.Model.TeacherClassesResponse;
import com.example.developer.schoolmanagementsystem.Model.TeacherExamDetailsResponse;
import com.example.developer.schoolmanagementsystem.Model.TeacherStudentInformationResponse;
import com.example.developer.schoolmanagementsystem.Model.TeacherStudentSubjectResponse;
import com.example.developer.schoolmanagementsystem.Model.TeacherStudyMaterialResponse;
import com.example.developer.schoolmanagementsystem.Model.TransportResponse;
import com.example.developer.schoolmanagementsystem.Model.qpResponse;
import com.example.developer.schoolmanagementsystem.Model.TeacherDetailsResponse;

import java.io.File;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface API {

    String CACHE = "Cache-Control: max-age=0";
    String AGENT = "User-Agent: Markeet";

    /* Recipe API transaction ------------------------------- */
//    product_id:1
//    user_id:12
//    score:31
//    comment:12

//    @FormUrlEncoded
//    @POST("index.php?component=json&action=add_rating")
//    Call<ResponseforSubmitRating> submitrating(@Field("product_id") String product_id,
//                                               @Field("user_id") String user_id,
//                                               @Field("score") String score,
//                                               @Field("comment") String comment);


    @FormUrlEncoded
    @POST("api/signin")

    Call<LoginResponse> login(@Field("email") String email ,
                              @Field("password") String password,
                              @Field("user_type") String user_type);

    @FormUrlEncoded
    @POST("api/teacher_list")

    Call<TeacherDetailsResponse> teacher(@Field("student_id") String student_id);

    @FormUrlEncoded
    @POST("api/teacher_details")

    Call<TDetailsResponse> teacherDetails(@Field("teacher_id") String teacher_id);

    @FormUrlEncoded
    @POST("api/subject_list")

    Call<SubjectDetailsResponse> SubjectDetails(@Field("student_id") String student_id);


    @FormUrlEncoded
    @POST("api/study_material")

    Call<StudentStudyMaterialResponse> StudentStudyMaterial(@Field("student_id") String student_id);


    @FormUrlEncoded
    @POST("api/academic_syllabus")

    Call<AcademicSyllabusResponse> AcademicSyllabus(@Field("student_id") String student_id);

    @FormUrlEncoded
    @POST("api/class_routine_days")

    Call<StudentClassRoutineDaysResponse> StudentClassRoutineDays(@Field("student_id") String student_id);

    @FormUrlEncoded
    @POST("api/class_routine")

    Call<StudentClassRoutineResponse> StudentClassRoutine (@Field("student_id") String student_id,
                                                           @Field("day") String day);

    @FormUrlEncoded
    @POST("api/student_marksheet")

    Call<StudentMarksheetResponse> StudentMarkSheetResponse (@Field("student_id") String student_id);

    @FormUrlEncoded
    @POST("api/invoice")

    Call<InvoiceResponse> Invoice (@Field("student_id") String student_id);

    @FormUrlEncoded
    @POST("api/transport")

    Call<TransportResponse> TransportResponse (@Field("student_id") String student_id);

    @FormUrlEncoded
    @POST("api/booklist")

    Call<BookListResponse> BookListResponse (@Field("student_id") String student_id);


    @FormUrlEncoded
    @POST("api/book_request")

    Call<RequestBookResponse> RequestBookResponse (@Field("student_id") String student_id);

    @FormUrlEncoded
    @POST("api/book_request_form")

    Call<RequestBookFormResponse> RequestBookFormResponse (@Field("student_id") String student_id,
                                                           @Field("book_id") String book_id,
                                                           @Field("issue_start_date") String issue_start_date,
                                                           @Field("issue_end_date") String issue_end_date);

    @FormUrlEncoded
    @POST("api/classes")

    Call<TeacherClassesResponse> TeacherClassesResponse (@Field("teacher_id") String teacher_id);

    @FormUrlEncoded
    @POST("api/sections")

    Call<TeacherClassStudentsSectionResponse> TEACHER_CLASS_STUDENTS_SECTION_RESPONSE_CALL (@Field("teacher_id") String teacher_id,
                                                                                            @Field("class_id") String class_id
                                                                                            );


    @FormUrlEncoded
    @POST("api/students")

    Call<TeacherClassStudentRecordResponse> TEACHER_CLASS_STUDENT_RECORD_RESPONSE_CALL (@Field("class_id") String class_id,
                                                                                        @Field("section_id") String section_id
                                                                                        );

    @FormUrlEncoded
    @POST("api/student_profile")

    Call<TeacherStudentInformationResponse> TEACHER_STUDENT_INFORMATION_RESPONSE_CALL (@Field("student_id") String student_id);


    @FormUrlEncoded
    @POST("api/subjects")

    Call<TeacherStudentSubjectResponse> TEACHER_STUDENT_SUBJECT_RESPONSE_CALL (@Field("class_id") String class_id);

    @FormUrlEncoded
    @POST("api/section_routine_days")

    Call<TeacherClassRoutineDaysResponse> TEACHER_CLASS_ROUTINE_DAYS_RESPONSE_CALL (@Field("class_id") String class_id,
                                                                                    @Field("section_id") String section_id
                                                                                    );


    @FormUrlEncoded
    @POST("api/section_routine")

    Call<TeacherClassRoutineSubjectsResponse> TEACHER_CLASS_ROUTINE_SUBJECTS_RESPONSE_CALL (@Field("class_id") String class_id,
                                                                                            @Field("section_id") String section_id,
                                                                                            @Field("date") String day
                                                                                            );

    @FormUrlEncoded
    @POST("api/class_syllabus")

    Call<TeacherAcademicSyllabusResponse> TEACHER_ACADEMIC_SYLLABUS_RESPONSE_CALL (@Field("class_id") String class_id);

    @FormUrlEncoded
    @POST("api/attendance")

    Call<TeacherAttendanceResponse> TEACHER_ATTENDANCE_RESPONSE_CALL                (@Field("class_id") String class_id,
                                                                                    @Field("section_id") String section_id,
                                                                                    @Field("date") String date
                                                                                    );

    @FormUrlEncoded
    @POST("api/exam_classes")

    Call<ExamClassModelResponse> EXAM_CLASS_MODEL_RESPONSE_CALL (@Field("teacher_id") String teacher_id);

    @FormUrlEncoded
    @POST("api/exams")

    Call<ExamListResponse> EXAM_LIST_RESPONSE_CALL (@Field("teacher_id")String teacher_id);

    @FormUrlEncoded
    @POST("api/exam_sections")

    Call<ExamSectionResponse> EXAM_SECTION_RESPONSE_CALL (@Field("class_id") String class_id);

    @FormUrlEncoded
    @POST("api/class_subjects")

    Call<ExamSubjectsResponse> EXAM_SUBJECTS_RESPONSE_CALL (@Field("class_id") String class_id);

    @FormUrlEncoded
    @POST("api/exam_details")

    Call<TeacherExamDetailsResponse> TEACHER_EXAM_DETAILS_RESPONSE_CALL (@Field("exam_id") String exam_id,
                                                                         @Field("subject_id") String subject_id
                                                                         );

    @FormUrlEncoded
    @POST("api/question_papers")

    Call<qpResponse> QP_RESPONSE_CALL (@Field("teacher_id") String teacher_id);

    @FormUrlEncoded
    @POST("api/booklist")

    Call<LibrarianBookResponse> LIBRARIAN_BOOK_RESPONSE_CALL (@Field("librarian_id") String librarian_id);

    @FormUrlEncoded
    @POST("api/parent_childs")

    Call<ParentChildResponse> PARENT_CHILD_RESPONSE_CALL (@Field("parent_id") String parent_id);

    @FormUrlEncoded
    @POST("api/all_book_request")

    Call<LibrarianBookRequestResponse> LIBRARIAN_BOOK_REQUEST_RESPONSE_CALL (@Field("librarian_id") String librarian_id);

    @FormUrlEncoded
    @POST("api/teacher_study_material")

    Call<TeacherStudyMaterialResponse> TEACHER_STUDY_MATERIAL_RESPONSE_CALL (@Field("teacher_id") String teacher_id);

    @FormUrlEncoded
    @POST("api/monthly_attendance")

    Call<PAttendanceResponse> P_ATTENDANCE_RESPONSE_CALL (@Field("student_id") String student_id);

    @FormUrlEncoded
    @POST("api/profile_info")

    Call<ProfileDataViewResponse> PROFILE_DATA_VIEW_RESPONSE_CALL (@Field("user_id") String user_id,
                                                                   @Field("user_type") String user_type
                                                                   );

    @FormUrlEncoded
    @POST("api/update_account")

    Call<ProfileDataUpdateResponse> PROFILE_DATA_UPDATE_RESPONSE_CALL (@Field("user_id") String user_id,
                                                                       @Field("name") String name,
                                                                       @Field("email") String email,
                                                                       @Field("user_type") String user_type,
                                                                       @Field("file")File file
                                                                       );

}