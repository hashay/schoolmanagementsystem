package com.example.developer.schoolmanagementsystem.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.developer.schoolmanagementsystem.Adatper.MovieAdapter;
import com.example.developer.schoolmanagementsystem.Model.Movie;
import com.example.developer.schoolmanagementsystem.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarListener;

import static com.example.developer.schoolmanagementsystem.Activities.login.MY_PREFS_NAME;
import static com.example.developer.schoolmanagementsystem.Activities.login.MY_PREFS_NAME2;

public class parentHome extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private HorizontalCalendar horizontalCalendar;
    public String parent_id,username,parent_name;
    Toolbar toolbar;
    int numberOfColumns = 3;

    //recycler view

    private List<Movie> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MovieAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_home);
        parent_id = getIntent().getExtras().getString("parent_id","");
        parent_name = getIntent().getExtras().getString("parent_name","");
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //shared preferences
        String MY_PREFS_NAME = "MyPrefsFile";
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);

        username = prefs.getString("username", null);

        //

        calender();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new MovieAdapter(movieList);
        //opening screen by getting data from card view
        mAdapter.setOnItemClickListener(new MovieAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, Movie obj) {


                if(obj.getTitle() == "Teacher") {
                    OpenTeacher();
                }else if(obj.getTitle() == "Academic Syllabus") {
                    OpenSyllabus();
                }else if(obj.getTitle() == "Class Routine") {
                    OpenClassRoutine();
                }else if(obj.getTitle() == "Exam Mark") {
                    OpenExam();
                }else if(obj.getTitle() == "Payment") {
                    OpenPayment();
                }else if(obj.getTitle() == "Transport") {
                    OpenTransport();
                }else if(obj.getTitle() == "Library") {
                    OpenLibrary();
                }else if(obj.getTitle() == "Student Attendance") {
                    OpenAttendance();
                }
                else{
                    Toast.makeText(parentHome.this, obj.getTitle() + " Screen not ready yet", Toast.LENGTH_SHORT).show();
                }

            }


        });

        LoadHomepage();
        prepareMovieData();
        LoadNav();


    }

    public void OpenTeacher(){
        Intent intent = new Intent(getApplicationContext(), teachers.class);
        intent.putExtra("parent_id",parent_id);
        startActivity(intent);
    }
    public void OpenSyllabus(){
        Intent intent = new Intent(getApplicationContext(), pacademicstudentselection.class);
        intent.putExtra("parent_id",parent_id);
        startActivity(intent);
    }
    public void OpenClassRoutine(){
        Intent intent = new Intent(getApplicationContext(), pclassroutinestudent.class);
        intent.putExtra("parent_id",parent_id);
        startActivity(intent);
    }
    public void OpenAttendance(){
        Intent intent = new Intent(getApplicationContext(), pattendancestudent.class);
        intent.putExtra("parent_id",parent_id);
        startActivity(intent);
    }
    public void OpenExam(){
        Intent intent = new Intent(getApplicationContext(), pexamstudent.class);
        intent.putExtra("parent_id",parent_id);
        startActivity(intent);
    }
    public void OpenPayment(){
        Intent intent = new Intent(getApplicationContext(), ppaymentstudent.class);
        intent.putExtra("parent_id",parent_id);
        startActivity(intent);
    }
    public void OpenLibrary(){
        Intent intent = new Intent(getApplicationContext(), booklist.class);
        intent.putExtra("parent_id",parent_id);
        startActivity(intent);
    }
    public void OpenTransport(){
        Intent intent = new Intent(getApplicationContext(), transport.class);
        intent.putExtra("parent_id",parent_id);
        startActivity(intent);
    }
    public void logout(){
        getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit().clear().commit();
        getSharedPreferences(MY_PREFS_NAME2, MODE_PRIVATE).edit().clear().commit();
        Intent intent = new Intent(getApplicationContext(), login.class);
        startActivity(intent);
        finish();
    }


    private void prepareMovieData() {
        Movie movie = new Movie(R.drawable.teacher, "Teacher");
        movieList.add(movie);

        movie = new Movie(R.drawable.academicsyllabus, "Academic Syllabus");
        movieList.add(movie);

        movie = new Movie(R.drawable.classroutine, "Class Routine");
        movieList.add(movie);


        movie = new Movie(R.drawable.dailyattendance, "Student Attendance");
        movieList.add(movie);

        movie = new Movie(R.drawable.exam, "Exam Mark");
        movieList.add(movie);

        movie = new Movie(R.drawable.payment, "Payment");
        movieList.add(movie);

        movie = new Movie(R.drawable.lib, "Library");
        movieList.add(movie);

        movie = new Movie(R.drawable.transport, "Transport");
        movieList.add(movie);




        mAdapter.notifyDataSetChanged();
    }


    public void calender(){

        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);

        horizontalCalendar = new HorizontalCalendar.Builder(parentHome.this    , R.id.calendarView)
                .startDate(startDate.getTime())
                .endDate(endDate.getTime())
                .datesNumberOnScreen(5)
                .dayNameFormat("EEE")
                .dayNumberFormat("dd")
                .monthFormat("MMM")
                .textSize(14f, 24f, 14f)
                .showDayName(true)
                .showMonthName(true)

                .build();

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Date date, int position) {
//                Toast.makeText(getContext(), DateFormat.getDateInstance().format(dateEditText) + " is selected!", Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.parent_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.parent_logout) {
            logout();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.parent_teacher) {
            OpenTeacher();
        } else if (id == R.id.parent_syllabus) {
            OpenSyllabus();
        } else if (id == R.id.parent_classroutine) {
            OpenClassRoutine();
        } else if (id == R.id.parent_attendance) {
            OpenAttendance();
        } else if (id == R.id.parent_exammarks) {
            OpenExam();
        } else if (id == R.id.parent_payment) {
            OpenPayment();
        } else if (id == R.id.parent_library) {
            OpenLibrary();
        } else if (id == R.id.parent_transport) {
            OpenTransport();
        }else if (id == R.id.parent_logout) {
            logout();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void LoadHomepage(){
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }

    public void LoadNav(){
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_parent);
        View Hview = navigationView.getHeaderView(0);
        TextView nav_useremail = (TextView)Hview.findViewById(R.id.parent_email);
        TextView nav_username = (TextView)Hview.findViewById(R.id.parent_name);
        nav_username.setText(parent_name);
        nav_useremail.setText(username);
        navigationView.setNavigationItemSelectedListener(this);
    }
}
