package com.example.developer.schoolmanagementsystem.Adatper;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.developer.schoolmanagementsystem.Model.StudentMarksheet;
import com.example.developer.schoolmanagementsystem.Model.marks;
import com.example.developer.schoolmanagementsystem.Model.material;
import com.example.developer.schoolmanagementsystem.R;

import java.util.List;

/**
 * Created by Developer on 2/19/2018.
 */

public class exammarksAdapter extends RecyclerView.Adapter<exammarksAdapter.MyViewHolder>{

    private List<StudentMarksheet> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, obtained , highest , grade , comment;


        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.exammarks_subject);
            obtained    = (TextView) view.findViewById(R.id.exam_obtained);
            highest = (TextView) view.findViewById(R.id.exam_highest);
            grade = (TextView) view.findViewById(R.id.exam_grade);
            comment = (TextView) view.findViewById(R.id.exam_comment);


        }
    }

    public exammarksAdapter(List<StudentMarksheet> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public exammarksAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.exammarks_list, parent, false);

        return new exammarksAdapter.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(exammarksAdapter.MyViewHolder holder, int position) {
        StudentMarksheet material = moviesList.get(position);
        holder.title.setText(material.getSubjectName());
        // holder.genre.setText(data.getGenre());
        // holder.year.setText(data.getYear());
        holder.obtained.setText(String.valueOf(material.getMarkObtained()));
        holder.highest.setText(String.valueOf(material.getHighestMark()));
        holder.grade.setText(String.valueOf(material.getGradeName()));
        holder.comment.setText(String.valueOf(material.getComment()));



    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}
